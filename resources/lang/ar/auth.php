<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'بيانات الاعتماد هذه غير متطابقة مع البيانات المسجلة لدينا.',
    'throttle' => 'عدد كبير جدا من محاولات الدخول. يرجى المحاولة مرة أخرى بعد :seconds ثانية.',

    "sign_up" => "تسجيل الحساب",
    "sign_in" => "تسجيل دخول",
    "login" => "تسجيل دخول",
    "dont_have_account" => "ليس لديك حساب ؟",
    "already_have_account" => "هل لديك حساب ؟",
    "register" => "إنشاء حساب جديد",
    "welcome_back" => "مرحبا بعودتك",
    "remember_me" => "تذكرنى",
    "forget_password" => "هل نسيت كلمة السر ؟",
    "logout" => "خروج",
    "reset_password" => "إعادة تعيين كلمة السر",
    "remembered_password" => "لقد تذكرت كلمة السر",
    "reset_password_hint" => "أدخل البريد الإلكتروني أو رقم الهاتف الذي استخدمته عند انشاء الحساب لإعادة تعيين كلمة السر الخاصة بك",
    "register_hint" => "الرجاء إدخال التفاصيل الخاصة بك للتسجيل وتكون جزءًا من مجتمعنا الرائع"



];
