<?php
return [
    "profile_updated" => "تم تحديث الملف الشخصي بنجاح.",
    "post_approved" => "تمت الموافقة على المنشور بنجاح.",
    "coupon_discount" => "هل لديك كوبون خصم ؟",
"upload_file" => "فشل فى رفع المرفقات.",
"upload_voice_note" => "فشل فى رفع الملاحظة الصوتية.",
    "allowed_extensions" => "الامتدادات المسموح بها: الصور (jpg، jpeg، png، bmp) و pdf فقط | الحجم الأقصى: 8MB . ",
    "invoice" => [
        "pay_half" => "ادفع فقط :half من المبلغ لتأكيد طلبك",
        "state" => [
            "full-paid" => "مدفوع بالكامل",
            "half-paid" => "50٪ مدفوع",
            "unpaid" => "غير مدفوع",
        ],
        "transaction" => [
            "success" => "شكرا لك ، تم الدفع بنجاح.",
            "error" => "خطأ في عملية الدفع ، يرجى الاتصال بنا لإصلاح هذه المشكلة.",
        ]
    ]

];
?>
