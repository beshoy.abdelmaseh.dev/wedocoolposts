<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    "sign_up" => "Sign up",
    "sign_in" => "Sign in",
    "login" => "Login",
    "dont_have_account" => "Don't have an account ?",
    "already_have_account" => "Already have an account?",
    "register" => "Sign up",
    "welcome_back" => "Welcome Back",
    "remember_me" => "Keep me logged in",
    "forget_password" => "Forgot Password ?",
    "logout" => "Logout",
    "reset_password" => "Reset Password",
    "remembered_password" => "I remembered my password",
    "reset_password_hint" => "Enter the email or phone number you used when you joined to reset your password",
    "register_hint" => "Please enter your details to sign up and be part of our great community"
];
