@section('modal_title')
    {{$modal_title ?? __("Poopy") }}
@stop
@section('modal_content')
    <div class="modal-body p-2">
        <form class="ajax-form" data-alert="#update-status-alert"
              data-button="#update-status-button" id="update-status-form" method="POST"
              action="{{route("actions.update_status",$action->id)}}">
            @csrf
            @method("PUT")
            <div class="row">
                <div class="col-12">
                    @component('components.alert')
                        @slot('alert_theme')  background  @endslot
                        @slot('custom_class')  mb-1 d-none  @endslot
                        @slot('alert_id')  update-status-alert  @endslot
                    @endcomponent
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-3" class="control-label">@lang("Reason")</label><br>
                        <small class="text-muted">@lang("Please write the reason").</small>
                        <textarea type="text" class="form-control" name="data" style="resize: none;" maxlength="250"></textarea>
                        <input type="hidden" name="approval" value="declined">
                    </div>
                </div>
            </div>

        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">@lang("Cancel")</button>
        <button type="button" class="btn btn-danger waves-effect waves-light submit-ajax-form"
                id="update-status-button" data-form="#update-status-form">
            <span class="spinner-border spinner-border-sm" style="display: none;" role="status"></span><span
                class="button-text">@lang("Confirm")</span>
        </button>
    </div>
@stop
