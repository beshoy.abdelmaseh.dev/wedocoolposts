@extends('layouts.fullLayoutMaster')
{{-- title --}}
@section('title',__("Tracking").' '.__("Order"))


@section('content')


    @include("panels.header-content")

    <section class="kb-content">
        <div class="row kb-search-content-info mx-1 mx-md-2 mx-lg-5">
            <div class="col-12">
                <div class=" match-height page-content">
                    <div class="row justify-content-center">
                        <div class=" col-sm-8 col-lg-6 col-md-6 text-center mt-4">

                            <div class="divider divider-primary mb-2 text-primary">
                                <div class="divider-text font-large-1" style="background: #f2f4f4 !important;;">@lang("Tracking") @lang("Order")</div>
                            </div>
                            @component('components.alert')
                                @slot('alert_theme')  background  @endslot
                                @slot('alert_type')  {{session('alert_message')["type"]}} @endslot
                                @slot('alert_content')  {!! session('alert_message')["content"] !!}  @endslot
                                @slot('custom_class')  text-left m-0 mb-2 {{session('alert_message') ? "" : "d-none"}} @endslot
                                @slot('alert_id')  tracking-find-alert  @endslot
                            @endcomponent
                                <form class="ajax-form" data-alert="#tracking-find-alert" data-scrolltoalert="true"
                                      data-button="#tracking-find-button" id="tracking-find-form" method="POST"
                                      action="{{route("tracking.find")}}">
                                    @csrf
                            <fieldset>
                                <div class="input-group">
                                    <input type="text" class="form-control form-control-lg tracking-order-input" placeholder="@lang("Enter your order id")"  name="order_id" autocomplete="off">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn mr-1 mb-1 btn-primary btn-lg round px-1" id="tracking-find-button" data-form="#tracking-find-form">
                                            <span class="spinner-border spinner-border-sm" style="display: none;" role="status"></span><span
                                                class="button-text">  <i class="bx bx-target-lock"></i><span class="align-middle"> @lang("Continue")</span></span>
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                       </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include("panels.assistant-info")

@endsection

