@extends('layouts.fullLayoutMaster',["appendFooter" => true])
{{-- title --}}
@section('title',__("Tracking").' '.__("Order"))
{{-- page scripts --}}

@section('content')
    @include("components.remote-modal-content")
    <!-- login page start -->
    <section class="invoice-view-wrapper page-content"   >
        <div class="text-center mx-2 my-2">
            <div class="brand-logo"><a href="{{route("home")}}"><img class="logo"  src="{{asset('assets/images/logo/logo.png')}}"></a></div>
        </div>
        <div class="row mx-1 mx-md-2 mx-lg-5 page-content mt-1">
            <div class="col-12 p-0 px-sm-1">
                <div class="row">
                    <div class="col-12 mb-0">
                        <h2 class="text-muted">@lang("Tracking") @lang("Order"): #{{$invoice->slug}}</h2>
                        <hr>
                    </div>
                    <div class=" col-12 col-xl-3 col-md-6 col-lg-4 py-sm-1">
                        <div class="card shadow-none border">
                            <div class="card-body pb-0"  >

                                <div class="d-flex justify-content-between mb-1">
                                    <div class="text-uppercase font-medium-2">@lang("Order Status")</div>
                                    <div class=""><div class="{{$order->action->status->class}} font-medium-1">{{ucwords($order->action->status->name)}}</div></div>
                                </div>
                                <hr>
                                <div class="d-flex justify-content-between mb-1">
                                    <div class="text-uppercase font-medium-2">@lang("Payment Status")</div>
                                    <div class="">
                                        @include("components.invoice-status",["state" => $invoice->state , "class" => "font-medium-1"])

                                    </div>
                                </div>
                                <hr>
                                <div class="d-flex justify-content-between mb-1">
                                    <div class="text-uppercase font-medium-2">@lang("Order Date")</div>
                                    <div class="">
                                      <div class=" font-medium-1" dir="ltr">{{$order->created_at->format("d M Y, h:i A")}}</div>

                                    </div>
                                </div>
                                <hr>

                                @if($order->user_id == optional(current_user())->id)
                                    <div class="mb-2">
                                        @if($order->action->approval == "approved")
                                        <div class="alert border-success p-1 mb-0">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bxs-check-circle"></i><span>@lang("Post Has Been Approved")</span>
                                            </div>
                                            </div>
                                        @else
                                        @if(optional($order->action->status)->key == null)
                                            <div class="alert alert-dark mb-0 p-50  ">@lang("Waiting the payment to confirm your order"). </div>
                                                <a href="{{route("invoices.show",$order->invoice->slug)}}" class="btn  btn-primary m-0 py-50 px-2 d-block mt-1"> @lang("Invoice Page")</a>
                                        @else
                                       <form class="ajax-form" data-alert="#order-action-alert" data-scrolltoalert="true"
                                              data-button="#order-action-button" id="order-action-form" method="POST"
                                              action="{{route("actions.update_status",$order->action->id)}}">
                                            @method("PUT")
                                            <input type="hidden" name="approval" value="approved">
                                            <input type="hidden" name="order_id" value="{{$order->id}}">
                                        </form>
                                           <div class="d-flex justify-content-center">
                                               <button type="button" class="btn btn-success waves-effect waves-light submit-ajax-form mr-1"
                                                       id="order-action-button" data-form="#order-action-form">
                                                   <span class="spinner-border spinner-border-sm" style="display: none;" role="status"></span><span
                                                       class="button-text"><i class="bx bxs-check-circle mr-50"></i> @lang("Approve")</span>

                                               </button>
                                               @if($order->action->approval != "declined")
                                               <button  class="btn btn-danger toggle-remote-modal"  id="update-status-decline"  data-url="{{route("actions.update_status",$order->action->id)}}" data-target="#update-status-modal">
                                                  <span  class="button-text"><i class="bx bxs-x-circle mr-50"></i> @lang("Poopy")</span>
                                               </button>
                                                @endif
                                           </div>
                                            @endif

                                        @endif
                                    </div>
                                    @endif

                            </div>
                        </div>
                    </div>

            <div class="col-12 col-xl-9 col-md-12 col-lg-8 p-0 py-sm-1 ">
                @if($order->action->approval == "declined")
                <div class="alert alert-danger  mb-1 py-1" role="alert">
                    <div class="d-flex align-items-center">
                        <i class="bx bxs-x-circle"></i>
                        <p class="m-0 p-0">
                        <span class="font-weight-bold">@lang("Poopy")</span><br>
                             <span class="font-small-3">{{$order->action->data->first()}}</span>
                        </p>
                    </div>
                </div>
                @endif

                            @component('components.alert')
                                @slot('alert_theme')  background  @endslot
                                @slot('alert_type')  {{session('alert_message')["type"]}} @endslot
                                @slot('alert_content')  {!! session('alert_message')["content"] !!}  @endslot
                                @slot('custom_class')  mb-1 {{session('alert_message') ? "" : "d-none"}} @endslot
                                @slot('alert_id')  tracking-alert  @endslot
                            @endcomponent

                    <div class="card shadow rounded-lg mb-1">
                        <div class="card-body py-1">
                            <div class="col-12  text-left">
                                <div class="font-medium-3"> <i class="livicon-evo" data-options="name: {{$order->details->firstWhere("answer_type","main_question")->question->getMeta("icon")}}; size: 40px; strokeColorAlt: #FDAC41; strokeColor: #ff6b09; style: lines-alt; eventOn: .kb-hover-1;"></i><span class="ml-1 font-weight-bold">{{$order->details->firstWhere("answer_type","main_question")->question->title}}</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="row ">
                    @php($loadedQuestions = $order->loadQuestions(optional($order->details)->pluck("answer_value")))
                    @foreach($order->details as $details)
                        @if(!($details->question->main) && !in_array($details->answer_type,["file","voice-note"]))
                            @if(optional($details->answer_value)->count() >= 1)
                                 @for($i = 0; $i < $details->answer_value->count(); $i++)
                                        @php($title = $details->question->getMeta("title_in_order"))
                                        <div class="{{$details->question->getMeta("tracking_class","col-sm-4")}} m-0 ">
                                            <div class="card shadow rounded-lg my-25">
                                                <div class="card-content">
                                                    <div class="card-body p-1 ">
                                                        <div class="font-weight-bold">
                                                                {!! $title ?: $details->question->title !!}  @if($i > 0) - {{($i+1)}} @endif
                                                        </div>
                                                        <hr>
                                                            <div class="@if(!in_array($details->answer_type,["texteditor"])) text-message @endif">{!! nl2br(str_contains($details->answer_value[$i] ?: "-","question_id") ? $details->getAnswerValue($loadedQuestions) : $details->answer_value[$i]  ?: "-") !!}
                                                            </div>
                                                        @if($voiceNotes = optional(optional($order->details->where("answer_key",$details->answer_key)->where("answer_type","voice-note")->first())->answer_value)->get($i))
                                                            <div class="divider divider-center">
                                                                <div class="divider-text">@lang("Voice Notes")</div>
                                                            </div>
                                                            <div class="row p-0 px-1 justify-content-md-start voice-preview">
                                                                @foreach($voiceNotes as $voiceNote)
                                                                    <div class="mb-25 col-sm-3 col-md-4">
                                                                        <audio src="{{route("uploads.show",$voiceNote)}}" controls="" style="width:100%"></audio>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                       @if($materials = optional(optional($order->details->where("answer_key",$details->answer_key)->where("answer_type","file")->first())->answer_value)->get($i))
                                                        <div class="divider divider-center">
                                                            <div class="divider-text">@lang("Materials")</div>
                                                        </div>
                                                            <div class="row p-0 px-1 justify-content-md-start material-preview">
                                                                @foreach($materials as $material)
                                                                    @php($file = explode(".",$material))
                                                                    @php($extension = end($file) )
                                                                    <div class="mb-50 main-review">
                                                                          <div class="position-relative ">
                                                                            <a href="{{route("uploads.show",$material)}}" {{($extension == "pdf" ? 'target=_blank' :'class=image-preview' )}} >
                                                                         <img  src="{{$extension == "pdf" ? asset("assets/images/icon/pdf-2.png") : route("uploads.show",$material)}}" class="img img-thumbnail rounded-lg mr-1"   {{($extension == "pdf" ? 'width=60' :  'width=100' )}} />
                                                                               </a>
                                                                                </div>
                                                                              </div>
                                                                @endforeach
                                                            </div>
                                                           @endif
                                                    </div>
                                                    @if($actions = $details->actions)
                                                        <x-show-actions-component :order="$order" :details="$details" :actions="$actions" :offset="$i"  />
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                  @endfor

                                @endif
                        @endif
                    @endforeach
                    </div>


                <div class="card shadow-lg rounded-lg mt-1">
                    <div class="card-content p-2">
                    <div class="card-title border-bottom">
                        <h4 class="mb-0">@lang("Comments")</h4>
                        <small class="text-muted text-lowercase">@lang("Write notes and comments")</small>
                    </div>
                    <div class="card-body p-0">
                        @comments(['model' => $order])
                    </div>
                    </div>
                </div>

            </div>

                </div>
        </div>
        </div>
    </section>

@endsection

@push("scripts")
    <script>
        $( document ).ready(function() {
            $('.image-preview').magnificPopup({
                type: 'image'
                // other options
            });
        });
    </script>
 @endpush
