@extends('layouts.fullLayoutMaster',["appendFooter" => true])
{{-- title --}}
@section('title',__('Invoice'))
{{-- page scripts --}}

@section('content')
    <!-- login page start -->
    <section class="invoice-view-wrapper page-content"   >
        <div class="text-center mx-2 my-2">
            <div class="brand-logo"><a href="{{route("home")}}"><img class="logo"  src="{{asset('assets/images/logo/logo.png')}}"></a></div>
        </div>
        <div class="row mx-1 mx-md-2 mx-lg-5 page-content mt-1">
            <div class="col-12 p-0 px-sm-1">
                <div class="row">
            <!-- invoice view page -->
            <div class="col-12 col-xl-9 col-md-12 col-lg-8 p-0 py-sm-1 ">
                @guest
                <div class="alert alert-dark  mb-1 p-50 p-sm-1" role="alert">
                    <div class="d-flex align-items-center">
                        <i class="bx bx-info-circle"></i>
                        <span class="font-medium-1">
                             @lang("Please") <a href='{{route("register")}}' class='font-weight-bold text-primary'>@lang("Register")</a> @lang("or") <a href='{{route("login")}}' class='font-weight-bold text-primary'>@lang("auth.sign_in")</a> @lang("first, to complete your order and pay").
                </span>
                    </div>
                </div>
                @endguest

                            @component('components.alert')
                                @slot('alert_theme')  background  @endslot
                                @slot('alert_type')  {{session('alert_message')["type"]}} @endslot
                                @slot('alert_content')  {!! session('alert_message')["content"] !!}  @endslot
                                @slot('custom_class')  mb-1 {{session('alert_message') ? "" : "d-none"}} @endslot
                                @slot('alert_id')  checkout-create-alert  @endslot
                            @endcomponent

                <div class="card invoice-print-area">
                    <div class="card-content">
                        <div class="card-body p-50 p-sm-1 mx-25">
                            <!-- header section -->
                            <div class="row">
                                <div class="col-xl-4 col-md-12 font-medium-2">
                                    <span class="invoice-number ">@lang("Order")#</span>
                                    <span class="font-weight-bold">{{$invoice->slug}}</span>
                                </div>
                                <div class="col-xl-8 col-md-12">
                                    <div class=" text-right  ">
                                        <div class="mr-0">
                                            <span class="text-muted">@lang("Date Issue"):</span>
                                            <span class="font-weight-bold " dir="ltr">{{$invoice->created_at->format("d F Y")}}.</span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- logo and title -->
                            <div class="row my-1">
                                <div class="col-6  d-none">
                                    <h4 class="text-primary">Order Details</h4>
                                </div>
                                <div class="col-md-6  text-left">
                                    <div class="mt-1 ml-md-2 font-medium-4"> <i class="livicon-evo" data-options="name: {{$order->details->firstWhere("answer_type","main_question")->question->getMeta("icon")}}; size: 50px; strokeColorAlt: #FDAC41; strokeColor: #ff6b09; style: lines-alt; eventOn: .kb-hover-1;"></i><span class="ml-1 font-weight-bold">{{$order->details->firstWhere("answer_type","main_question")->question->title}}</span></div>
                                </div>
                                <div class="col-md-6 text-right">
                                    @include("components.invoice-status",["state" => $invoice->state, "type" => "glow"])

                                         @if($invoice->state == "pending")
                                             <br><div class="alert alert-dark p-50 m-0 d-inline-block rounded-lg">
                                                 <span class="font-weight-bold ">@lang("locale.invoice.pay_half",["half" => '<span class="text-primary ">50%</span>'])</span>
                                             </div>
                                         @endif
                                         @if($invoice->state == "half-paid")
                                             <br><div class="alert alert-dark p-50 m-0 d-inline-block rounded-lg">
                                                 <span class=" font-weight-bold text-nowrap">@lang("You can pay rest of the amount after approving")</span>
                                             </div>
                                         @endif
                                </div>
                            </div>
                            <h4 class="text-primary">@lang("Order Details")</h4>

                            <hr>
                            <!-- invoice address and contact -->
                            <div class="row invoice-info d-none">
                                <div class="col-6 mt-1">
                                    <h6 class="invoice-from">Bill From</h6>
                                    <div class="mb-1">
                                        <span>Clevision PVT. LTD.</span>
                                    </div>
                                    <div class="mb-1">
                                        <span>9205 Whitemarsh Street New York, NY 10002</span>
                                    </div>
                                    <div class="mb-1">
                                        <span>hello@clevision.net</span>
                                    </div>
                                    <div class="mb-1">
                                        <span>601-678-8022</span>
                                    </div>
                                </div>
                                <div class="col-6 mt-1">
                                    <h6 class="invoice-to">Bill To</h6>
                                    <div class="mb-1">
                                        <span>Pixinvent PVT. LTD.</span>
                                    </div>
                                    <div class="mb-1">
                                        <span>203 Sussex St. Suite B Waukegan, IL 60085</span>
                                    </div>
                                    <div class="mb-1">
                                        <span>pixinvent@gmail.com</span>
                                    </div>
                                    <div class="mb-1">
                                        <span>987-352-5603</span>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <!-- product details table-->
                        <div class="invoice-product-details  mx-md-25">
                            <table class="table table-borderless mb-0">
                                <thead>
                                <tr class="border-0">
                                    <th scope="col" class="col-md-12  text-nowrap w-100"></th>
                                    <th scope="col"></th>
                                    <th scope="col" class="text-right">@lang("Price")</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                            @php($loadedQuestions = $order->loadQuestions(optional($order->details)->pluck("answer_value")))
                            @foreach($order->details as $details)
                                @if(!($details->question->getMeta("hide_in_order",false)))
                                    @php($title = $details->title_in_order)
                                    <div class="row px-1 mb-1">
                                        <div class="col-sm-12 col-md-5">
                                            @if($title)
                                                {!! $title !!}
                                            @else
                                              {{$details->question->title}}
                                            @endif
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            @if(empty($title))
                                                {!! $details->getAnswerValue($loadedQuestions) !!}
                                            @endif
                                        </div>
                                        <div class="col-sm-12 col-md-2 text-right text-muted  text-nowrap">{{$details->price}}</div>
                                        <div class="col-sm-12 d-md-none "><hr></div>
                                    </div>
                                @endif

                            @endforeach

                        </div>
                        <!-- invoice subtotal -->
                        <div class="card-body pt-0 mx-25">
                            <div class="d-none d-sm-block"><hr></div>
                            <div class="row">
                                <div class=" col-sm-12 col-md-6 ">
                                    <p class="d-none d-sm-block">@lang("Thank you for choosing us").</p>
                                </div>
                                <div class="col-12 col-sm-6 d-flex justify-content-end mr-0 pr-0">
                                    <div class="invoice-subtotal">
                                        @if($invoice->discount)
                                        <div class="invoice-calc d-flex justify-content-between mb-50">
                                            <span class="invoice-title ">@lang("Sub Total"):</span>
                                            <span class="invoice-value  text-primary">{{$invoice->getRawOriginal("amount")}} {{currency_locale()}}</span>
                                        </div>
                                        <div class="invoice-calc d-flex justify-content-between">
                                            <span class="invoice-title">@lang("Discount"):</span>
                                            <span class="invoice-value  text-primary">- {{$invoice->discount}} {{currency_locale()}}</span>

                                        </div>
                                        @if($invoice->coupon)
                                        <div class="font-small-2 pt-50 px-50 m-0"><div><span>@lang("A coupon") [<mark class="font-small-2" style="background: rgba(239,239,239,0.82);">{{$invoice->getMeta("coupon_code","NULL")}}</mark>] @lang("has been applied").</span></div>
                                        </div>
                                         @endif
                                        <hr>
                                        @endif
                                        <div class="invoice-calc d-flex justify-content-between">
                                            <span class="invoice-title font-weight-bold text-uppercase">@lang("Amount Due"):</span>
                                            <span class="invoice-value text-primary font-weight-bold font-medium-1">{{$invoice->amount}} {{currency_locale()}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- invoice action  -->
            <div class=" col-12 col-xl-3 col-md-6 col-lg-4 p-0 p-sm-1 ">
                @if($invoice->state == "pending")
                <div class="card invoice-action-wrapper shadow-none border  mb-1">
                    <div class="card-body p-1" >
                        <form class="ajax-form" data-alert="#checkout-create-alert" data-scrolltoalert="true"
                              data-button="#apply-coupon-button" id="apply-coupon-form" method="POST"
                              action="{{route("invoices.apply_coupon",$invoice->slug)}}">
                            @csrf
                            @method("PUT")
                        <label for="apply-coupon" class="text-uppercase text-muted">@lang("Coupon Code")</label><br>
                        <div class="input-group ">
                            <input class="form-control" id="apply-coupon" type="text" name="coupon" autocomplete="off">
                            <div class="input-group-append" id="button-addon2">
                                <button type="button" class="btn btn-primary  submit-ajax-form "
                                id="apply-coupon-button" data-form="#apply-coupon-form">
                                    <span class="spinner-border spinner-border-sm" style="display: none;" role="status"></span><span
                                        class="button-text">{{ ucfirst(__("apply")) }}</span>
                                </button>
                            </div>
                        </div>
                        <small class="text-muted">@lang("locale.coupon_discount")</small>
                        </form>
                        @if($invoice->coupon)
                        <hr>
                        <div class="border-bottom font-small-3 p-0 pb-50 m-0 rounded-lg d-flex justify-content-between"><div><span>@lang("A coupon") [<mark class="font-small-2" style="background: rgba(239,239,239,0.82);">{{$invoice->getMeta("coupon_code","NULL")}}</mark>] @lang("has been applied").</span></div>
                            <a  href="{{route("invoices.remove_coupon",$invoice->slug)}}" data-toggle="tooltip"  data-original-title="@lang("Remove Coupon")" data-placement="top"><i class="bx bx-x-circle font-medium-2 m-0 p-0 text-danger" style="cursor: pointer"></i>
                            </a>
                        </div>
                          @endif
                    </div>
                </div>
                @endif
                <div class="card invoice-action-wrapper shadow-none border">
                    <div class="card-body p-50 p-sm-1" @guest data-toggle="tooltip"  data-original-title="@lang("Register") @lang("or") @lang("auth.sign_in") @lang("first")" data-placement="top" @endguest >
                        @if($invoice->state == "pending" || $invoice->state == "half-paid")
                            <div class="text-center">
                                <img src="{{asset('assets/images/cards/accept_blue.png')}}" class="img-thumbnail bg-transparent border-0 " width="150"/><br>
                                <div class=" p-0 text-left  mb-1 font-weight-bold">
                                    @lang("You will be redirected to weaccept portal to enter your credit card info").
                                </div>
                            </div>
                        <div class="invoice-action-btn ">
                            <hr>
                            <div class="d-flex justify-content-between mb-1">
                                <div class="text-uppercase font-medium-2 font-weight-bold">@lang("Amount To Pay"):</div>
                                <div class="">
                                    <div class="font-medium-1 font-weight-bold text-primary">{{$invoice->amount_to_pay}} {{currency_locale()}}</div>
                                </div>
                            </div>
                            <form class="ajax-form" data-alert="#checkout-create-alert" data-scrolltoalert="true"
                                  data-button="#checkout-create-button" id="checkout-create-form" method="POST"
                                  action="{{route("invoices.checkout",$invoice->slug)}}">
                                <button type="button" class="btn btn-lg btn-primary btn-block  submit-ajax-form  " @guest disabled @endguest
                                        id="checkout-create-button" data-form="#checkout-create-form">
                                    <span class="spinner-border spinner-border-sm" style="display: none;" role="status"></span><span
                                        class="button-text"><i class="bx bxs-dollar-circle font-medium-5"></i> {{strtoupper(__("Pay Now"))}}</span>
                                </button>
                            </form>
                        </div>
                        @endif
                        <div class="invoice-action-btn d-none">
                            <button class="btn btn-light-primary btn-block invoice-print">
                                <span>Print</span>
                            </button>
                        </div>
                            <div class="text-center font-weight-bold mb-1">
                            @if($invoice->state == "full-paid")
                            <div class="text-center">
                                <div class=" p-0 text-center font-weight-bold font-medium-3 ml-1">
                                    @lang("Powered By")
                                </div>
                                <img src="{{asset('assets/images/cards/accept_blue.png')}}" class="img-thumbnail bg-transparent border-0 mb-1" width="150"/><br>
                            </div>
                             @endif
                            <img src="{{asset('assets/images/cards/payment_method_cc.png')}}" class="img-thumbnail bg-transparent border-0 mt-1"/>
                        </div>

                    </div>
                </div>
            </div>
                </div>
        </div>
        </div>
    </section>
    <!-- login page ends -->
@endsection

@push("styles")
    <style>
        .invoice-view-wrapper .invoice-subtotal .invoice-calc .invoice-title {
            width: 171px;
        }
        @media (min-width: 576px) {
            .d-sm-block {
                display: block !important;
            }
        }
    </style>
@endpush
