@extends('layouts.fullLayoutMaster',["appendFooter" => true])
{{-- title --}}
@section('title',null)
{{-- page scripts --}}

@section('content')

    @include("web.pages.orders.main-questions",["selectedMainQuestion" => $question->key])

    <!-- Form wizard with icon tabs section start -->
    <section id="icon-tabs">
        <div class="row kb-search-content-info m-0 mx-sm-1 mx-md-2 mx-lg-5 page-content">
            <div class="col-12 p-0 px-sm-1">
                <div class="card loading-card">
                    <div class="card-header mb-0 text-left text-sm-center">
                        <h4 class="card-title text-lowercase ">@lang("Please fill in fields and answer questions to complete your order").</h4>
                    </div>
                    <div class="card-content mt-1 py-4">
                        <form class="ajax-form d-none" data-alert="#order-create-alert" enctype="multipart/form-data"
                              data-scrolltoalert="true"
                              data-button="#order-create-button" id="order-create-form" method="POST"
                              @if($question->getMeta("form") == "wizard") data-form_wizard="#order-create-form-wizard" @endif
                              action="{{route("orders.store",$question->id)}}">
                            <div class="card-body px-1 m-0 pb-1">
                                <div class="@if($question->getMeta("form") == "wizard") form-wizard @endif @if($question->getMeta("hide_wizard_pagination",false)) hide_wizard_pagination @endif" id="order-create-form-wizard" data-pagination="#order-create-form-wizard-pagination" data-submit_label="{{ucwords(__("Order"))}}" data-next_label="{{__("Next")}}">
                                    <div class="row">
                                        <div class="col-md-12 px-2">
                                            @component('components.alert')
                                                @slot('alert_theme')  background  @endslot
                                                @slot('alert_type')  {{session('alert_message')["type"]}} @endslot
                                                @slot('alert_content')  {!! session('alert_message')["content"] !!}  @endslot
                                                @slot('custom_class')  m-0 {{session('alert_message') ? "" : "d-none"}} @endslot
                                                @slot('alert_id')  order-create-alert  @endslot
                                            @endcomponent
                                        </div>
                                    </div>
                                    @if($question->getMeta("form") == "wizard")
                                    <input type="hidden" readonly value="0" name="form_wizard_step_number">
                                    @endif

                                    @php($steps = $question->countSteps())
                                @for($i = 0; $i<$steps; $i++)
                                    @php($groupQuestions = $question->children->where("group_id",$i)->sortBy("sorting"))
                                    @if($question->getMeta("form") == "wizard")
                                    <!-- Step 1 -->
                                        <h6 class="form-wizard-header-tag">
                                            @if(optional($groupQuestions->first())->label)
                                                <span class="fonticon-wrap text-left" style="top: -15px !important;">
                                                <span style="text-transform: none !important;" class="font-medium-2">{{$groupQuestions->first()->label}}</span>
                                            </span>
                                            @else
                                                <span class="fonticon-wrap text-left" style="top: -30px !important;">
                                                <i class="livicon-evo-step " data-options="name:question-alt.svg; size: 50px; style:lines; strokeColor:#adb5bd;"></i>
                                                    </span>

                                            @endif
                                        </h6>
                                    @endif
                                        <!-- Step 1 end-->
                                        <!-- body content step 1 -->
                                        <div class="form-wizard-content" data-groupid="{{$i}}">
                                            <div class="row">
                                                @foreach($groupQuestions as $groupQuestion)
                                                    <x-question-component :question="$groupQuestion" :groupId="$i"/>
                                                @endforeach
                                            </div>
                                        </div>
                                        <!-- body content step 1 end-->
                                    @endfor
                                </div>
                                @if($question->getMeta("form") == "wizard")
                                <div class="row mx-md-2 mt-2 form-wizard-header form-wizard-pagination"
                                     id="order-create-form-wizard-pagination">
                                    <div class="col-md-12 d-flex justify-content-between">
                                        <button type="button"
                                                class="btn step-previous width-xl btn-dark waves-effect waves-light cursor-default"
                                                disabled="disabled"
                                                data-form_wizard="#order-create-form-wizard"><span
                                                class="button-text d-flex"><i
                                                    class="bx bx-{{class_by_dir("left")}}-arrow-alt"></i> {{__("Previous")}}</span>
                                        </button>
                                        <button type="button"
                                                class="step-next btn btn-primary waves-effect waves-light submit-ajax-form"
                                                id="order-create-button" data-form="#order-create-form" style="width: auto !important;">
                                        <span class="spinner-border spinner-border-sm" style="display: none;"
                                              role="status"></span><span
                                                class="button-text d-flex"> <span class="text text-nowrap">{{__("Next")}}</span> <i class="bx bx-{{class_by_dir("right")}}-arrow-alt"></i></span>
                                        </button>
                                    </div>
                                </div>
                                @else
                                    <hr>
                                    <div class="row mx-md-2 mt-2">
                                        <div class="col-md-12 d-flex justify-content-center">
                                    <button type="button"
                                            class="step-next btn btn-primary waves-effect waves-light submit-ajax-form"
                                            id="order-create-button" data-form="#order-create-form">
                                        <span class="spinner-border spinner-border-sm" style="display: none;"
                                              role="status"></span><span
                                            class="button-text d-flex"> <span class="text">{{ucwords(__("confirm order"))}}</span></span>
                                    </button>
                                    </div>
                                    </div>
                                @endif
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include("panels.assistant-info")
@endsection

@push("scripts")
<script>
    function inFormcallbeforeAjax(formEl){
        if (formEl.data("voice_recorder") === true) {
            var getToolObj = $(formEl.data("button")).closest(".note-voice-record");

            getToolObj.find(".record-state").addClass("d-none");
            getToolObj.find(".save-voice-btn button").tooltip('hide');
            getToolObj.find(".cancel-voice-btn").addClass("d-none");
        }
    }

    function AjaxHandleErrorMessage(formEl,response) {
        if (formEl.data("voice_recorder") === true) {
            var getToolObj = $(formEl.data("button")).closest(".note-voice-record");
            getToolObj.find(".save-voice-btn").addClass("d-none");
            getToolObj.find(".save-voice-btn button").tooltip('hide');
            getToolObj.find(".start-voice-btn").removeClass("d-none");
        }
    }

    function ajaxUnHandeledError(formEl,alertMessage,formData) {
        if (formEl.data("voice_recorder") === true) {
            alert("failed to deliver voice note , check your internet connection please!");
        }
    }
    $( document ).ready(function() {


        setTimeout(function () {
                if($('.loading-card').length > 0) {
                    $('.loading-card').unblock();
                    $('.loading-card').find("form").removeClass("d-none");
                    $('.loading-card').find(".card-content").removeClass("py-4");
                }
            }, 300);

    });
</script>
@endpush
