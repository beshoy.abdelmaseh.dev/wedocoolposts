
@include("panels.header-content")

<section class="kb-content">
    <div class="row kb-search-content-info mx-1 mx-md-2 mx-lg-5">
        <div class="col-12 p-0 px-sm-1">
            <div class=" match-height">
                @foreach($mainQuestions as $mainQuestion)
                    <div class="row justify-content-center {{isset($selectedMainQuestion) ? ($selectedMainQuestion != $mainQuestion->key ? 'd-none' : null) : null}}">
                    <div class=" col-sm-12  col-md-6">
                        <div class="card kb-hover-1  mb-1 ">
                            <div class="card-content rounded {{isset($selectedMainQuestion) ? ($selectedMainQuestion == $mainQuestion->key ? 'border-2 border-primary' : null) : null}}">
                                <div class="card-body text-center p-1">
                                    <a href="{{route('orders.create',$mainQuestion->key)}}" id="{{$mainQuestion->key}}">
                                        <div class="media">
                                            <div class="  mr-1">
                                                <i class="livicon-evo"
                                                   data-options="name: {{$mainQuestion->getMeta("icon")}}; size: 50px; strokeColorAlt: #FDAC41; strokeColor: #ff6b09; style: lines-alt;  eventOn:#{{$mainQuestion->key}};"></i>
                                            </div>
                                            <div class="media-body">
                                                <h5 class="media-heading text-left">{{$mainQuestion->title}}</h5>
                                                <p class=" text-muted mb-0 text-left"> {{$mainQuestion->desc}}</p>
                                            </div>
                                        </div>

                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</section>
