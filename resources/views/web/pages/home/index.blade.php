@extends('layouts.fullLayoutMaster')
{{-- title --}}
@section('title',null)


@section('content')


    @include("panels.header-content")

    <section class="kb-content">
        <div class="row kb-search-content-info mx-1 mx-md-2 mx-lg-5">
            <div class="col-12">
                <div class=" match-height">
                    <div class="row justify-content-center">
                        <div class=" col-sm-12 col-lg-8 col-md-6 text-center mt-4">
                            <a href="{{route("orders.index")}}" class="btn btn-lg mb-1 btn-primary px-5 py-1 rounded-lg">Create Post</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include("panels.assistant-info")

@endsection

