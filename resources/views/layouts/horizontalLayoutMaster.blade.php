<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr" dir="ltr">
<!-- BEGIN: Head-->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title',config("app.name"))</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/ico/favicon.ico')}}">
    <meta property="og:image"   content="{{asset('assets/images/logo/logo-200x.png')}}" />

    {{-- Include core + vendor Styles --}}
    @include('panels.styles')
</head>
<!-- BEGIN: Body-->
<body class="dir-{{config('theme.custom.direction')}} horizontal-layout horizontal-menu @if(isset($configData['navbarType']) && ($configData['navbarType'] !== "navbar-hidden") ){{$configData['navbarType']}} @else {{'navbar-sticky'}}@endif 2-columns
@if($configData['theme'] === 'dark'){{'dark-layout'}} @elseif($configData['theme'] === 'semi-dark'){{'semi-dark-layout'}} @else {{'light-layout'}} @endif
@if($configData['isContentSidebar']=== true) {{'content-left-sidebar'}} @endif
@if(isset($configData['footerType'])) {{$configData['footerType']}} @endif {{$configData['bodyCustomClass']}}
@if($configData['isCardShadow'] === false){{'no-card-shadow'}}@endif"
data-open="hover" data-menu="horizontal-menu" data-col="2-columns">

  <!-- BEGIN: Header-->
  @include('panels.fullLayout-navbar')
  <!-- END: Header-->

  <!-- BEGIN: Main Menu-->
{{--  @include('panels.sidebar')--}}
  <!-- END: Main Menu-->

  <!-- BEGIN: Content-->
  <div class="app-content content">
    {{-- Application page structure --}}
	@if($configData['isContentSidebar'] === true)
		<div class="content-area-wrapper">
			<div class="sidebar-left">
				<div class="sidebar">
					@yield('sidebar-content')
				</div>
			</div>
			<div class="content-right">
        <div class="content-overlay"></div>
				<div class="content-wrapper  p-0 pys-sm-1">
            <div class="content-header row">
            </div>
            <div class="content-body">
                @yield('content')
            </div>
        </div>
			</div>
		</div>
	@else
    {{-- others page structures --}}
    <div class="content-overlay"></div>
		<div class="content-wrapper p-0 p-sm-1">
			<div class="content-header row">
        @if($configData['pageHeader'] === true && isset($breadcrumbs))
          @include('panels.breadcrumbs')
        @endif
			</div>
			<div class="content-body">
				@yield('content')
			</div>
		</div>
	@endif
  </div>
  <!-- END: Content-->
@if($configData['isCustomizer'] === true && isset($configData['isCustomizer']))
  <!-- BEGIN: Customizer-->
  <div class="customizer d-none">
    <a class="customizer-close" href="#"><i class="bx bx-x"></i></a>
    <a class="customizer-toggle" href="#"><i class="bx bx-cog bx bx-spin white"></i></a>

  </div>
  <!-- End: Customizer-->

  <!-- Buynow Button-->

  @endif
  <!-- demo chat-->


  <div class="sidenav-overlay"></div>
  <div class="drag-target"></div>

  <!-- BEGIN: Footer-->
  @include('panels.footer')
  <!-- END: Footer-->

  @include('panels.scripts')
</body>
<!-- END: Body-->
