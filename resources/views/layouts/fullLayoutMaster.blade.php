<!DOCTYPE html>
<html class="loading" lang="{{LaravelLocalization::getCurrentLocale()}}" data-textdirection="{{config('theme.custom.direction')}}" dir="{{config('theme.custom.direction')}}">
  <!-- BEGIN: Head-->
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title',config("app.name"))</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/ico/favicon.ico')}}">
   <meta property="og:image"   content="{{asset('assets/images/logo/logo-200x.png')}}" />

    {{-- Include core + vendor Styles --}}
    @include('panels.styles')
  </head>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="dir-{{config('theme.custom.direction')}} vertical-layout 1-column navbar-sticky {{ config('theme.custom.bodyCustomClass') }} footer-static blank-page
  @if($configData['theme'] === 'dark'){{'dark-layout'}} @elseif($configData['theme'] === 'semi-dark'){{'semi-dark-layout'}} @else {{'light-layout'}} @endif" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- BEGIN: Content-->
    <!-- BEGIN: Header-->
    @include('panels.fullLayout-navbar')
    <!-- END: Header-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
         @yield('content')
        </div>
      </div>
    </div>
    <!-- END: Content-->

    @if(isset($appendFooter))
    <!-- BEGIN: Footer-->
    @include('panels.footer')
    <!-- END: Footer-->
    @endif
    {{-- scripts --}}
    @include('panels.scripts')

  </body>
  <!-- END: Body-->
</html>
