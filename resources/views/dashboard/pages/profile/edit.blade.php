@extends('layouts.horizontalLayoutMaster')
{{-- page title --}}
@section('title',__("Update").' '.__("My Profile"))
{{-- vendor style --}}
@section('vendor-styles')

@endsection
{{-- page style --}}
@section('page-styles')

@endsection

@section('content')
    <!-- invoice list --><!-- account setting page start -->
    <section id="page-account-settings " class="m-0 mx-sm-2">
        <div class="text-center">
            <div class="brand-logo"><a href="{{route("home")}}"><img class="logo"  src="{{asset('assets/images/logo/logo.png')}}"></a></div>
        </div>


        <div class="row m-0 mx-sm-1 mx-md-2 mx-lg-5 mt-1 page-content">
            <div class="col-12 p-0 px-sm-1">
                <div class="card rounded-lg ">
                    <div class="card-body pr-1 pb-0 pl-1">
                    <div class="row ">
                    <!-- left menu section -->
                    <div class="col-md-3 mb-2 mb-md-0 ">
                        <ul class="nav nav-pills flex-column">
                            <li class="nav-item">
                                <a class="nav-link d-flex align-items-center {{$page == "info" ? "active" : ""}}" id="account-pill-general"
                                   href="{{route("dashboard.profile.edit","info")}}" >
                                    <i class="bx bx-cog"></i>
                                    <span>@lang("General")</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link d-flex align-items-center {{$page == "password" ? "active" : ""}}" id="account-pill-password"
                                   href="{{route("dashboard.profile.edit","password")}}">
                                    <i class="bx bx-lock"></i>
                                    <span>@lang("Change Password")</span>
                                </a>
                            </li>
                            <li class="nav-item d-none">
                                <a class="nav-link d-flex align-items-center" id="account-pill-info" data-toggle="pill"
                                   href="#account-vertical-info" aria-expanded="false">
                                    <i class="bx bx-info-circle"></i>
                                    <span>Info</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link d-flex align-items-center {{$page == "links" ? "active" : ""}}" id="account-pill-social"
                                   href="{{route("dashboard.profile.edit","links")}}" >
                                    <i class="bx bxl-twitch"></i>
                                    <span>@lang("Social Links")</span>
                                </a>
                            </li>
                            <li class="nav-item d-none">
                                <a class="nav-link d-flex align-items-center" id="account-pill-connections" data-toggle="pill"
                                   href="#account-vertical-connections" aria-expanded="false">
                                    <i class="bx bx-link"></i>
                                    <span>Connections</span>
                                </a>
                            </li>
                            <li class="nav-item d-none">
                                <a class="nav-link d-flex align-items-center" id="account-pill-notifications" data-toggle="pill"
                                   href="#account-vertical-notifications" aria-expanded="false">
                                    <i class="bx bx-bell"></i>
                                    <span>Notifications</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- right content section -->
                    <div class="col-md-9 shadow-lg ">
                        <div class="card ">
                            <div class="card-content">
                                <div class="card-body px-0 px-sm-2">
                                    <div class="mb-2">
                                        <h4 class="text-muted tab-title">{{$page == "info" ? __("General") : ($page == "password" ? __("Change Password") : __("Social Links"))}}</h4>
                                        <hr>
                                    </div>
                                    <div class="col-md-12">
                                        @component('components.alert')
                                            @slot('alert_theme')  background  @endslot
                                            @slot('alert_type')  {{session('alert_message')["type"]}} @endslot
                                            @slot('alert_content')  {!! session('alert_message')["content"] !!}  @endslot
                                            @slot('custom_class')  mb-1 {{session('alert_message') ? "" : "d-none"}} @endslot
                                            @slot('alert_id')  profile-edit-alert  @endslot
                                        @endcomponent
                                    </div>
                                    <form class="ajax-form" data-alert="#profile-edit-alert" enctype="multipart/form-data"
                                          data-scrolltoalert="true"
                                          data-button="#profile-edit-button" id="profile-edit-form" method="POST"
                                          action="{{route("dashboard.profile.update",$page)}}">
                                        @method("PUT")
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane {{$page == "info" ? 'active' : ''}}" id="account-vertical-general"
                                             aria-labelledby="account-pill-general"  >
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>@lang("Full Name")</label>
                                                                <input type="text" class="form-control" name="full_name" value="{{current_user()->full_name}}" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>@lang("Phone Number")</label>
                                                                <input type="text" class="form-control"  value="{{current_user()->phone}}" readonly disabled >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>@lang("Email Address")</label>
                                                                <input type="text" class="form-control"  value="{{current_user()->email}}" readonly disabled >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 d-none">
                                                        <div class="alert bg-rgba-warning alert-dismissible mb-2"
                                                             role="alert">
                                                            <button type="button" class="close" data-dismiss="alert"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">×</span>
                                                            </button>
                                                            <p class="mb-0">
                                                                Your email is not confirmed. Please check your inbox.
                                                            </p>
                                                            <a href="javascript: void(0);">Resend confirmation</a>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>@lang("Business Type")</label>
                                                            @include("partials.types.dropdown",["type" => "Business-Types" , "type_id" => [current_user()->getMeta("type_id")]])

                                                        </div>
                                                    </div>

                                                </div>

                                        </div>
                                        <div class="tab-pane  {{$page == "password" ? 'active' : ''}}" id="account-vertical-password" role="tabpanel"
                                             aria-labelledby="account-pill-password"  >
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>@lang("Current Password")</label>
                                                                <input type="password" class="form-control" required
                                                                       placeholder="@lang("Current Password")" name="current_password">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>@lang("New Password")</label>
                                                                <input type="password" name="password" class="form-control"
                                                                       placeholder="@lang("New Password")">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>@lang("Retype New Password")</label>
                                                                <input type="password" name="password_confirmation"
                                                                       class="form-control"
                                                                       placeholder="@lang("New Password")">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="tab-pane fade" id="account-vertical-info" role="tabpanel"
                                             aria-labelledby="account-pill-info" aria-expanded="false">
                                            <form novalidate>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>Bio</label>
                                                            <textarea class="form-control" id="accountTextarea" rows="3"
                                                                      placeholder="Your Bio data here..."></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>Birth date</label>
                                                                <input type="text" class="form-control birthdate-picker"
                                                                       required placeholder="Birth date"
                                                                       data-validation-required-message="This birthdate field is required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>Country</label>
                                                            <select class="form-control" id="accountSelect">
                                                                <option>USA</option>
                                                                <option>India</option>
                                                                <option>Canada</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>Languages</label>
                                                            <select class="form-control" id="languageselect2"
                                                                    multiple="multiple">
                                                                <option value="English" selected>English</option>
                                                                <option value="Spanish">Spanish</option>
                                                                <option value="French">French</option>
                                                                <option value="Russian">Russian</option>
                                                                <option value="German">German</option>
                                                                <option value="Arabic" selected>Arabic</option>
                                                                <option value="Sanskrit">Sanskrit</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>Phone</label>
                                                                <input type="text" class="form-control" required
                                                                       placeholder="Phone number" value="(+656) 254 2568"
                                                                       data-validation-required-message="This phone number field is required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>Website</label>
                                                            <input type="text" class="form-control"
                                                                   placeholder="Website address">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>Favourite Music</label>
                                                            <select class="form-control" id="musicselect2"
                                                                    multiple="multiple">
                                                                <option value="Rock">Rock</option>
                                                                <option value="Jazz" selected>Jazz</option>
                                                                <option value="Disco">Disco</option>
                                                                <option value="Pop">Pop</option>
                                                                <option value="Techno">Techno</option>
                                                                <option value="Folk" selected>Folk</option>
                                                                <option value="Hip hop">Hip hop</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>Favourite movies</label>
                                                            <select class="form-control" id="moviesselect2"
                                                                    multiple="multiple">
                                                                <option value="The Dark Knight" selected>The Dark Knight
                                                                </option>
                                                                <option value="Harry Potter" selected>Harry Potter</option>
                                                                <option value="Airplane!">Airplane!</option>
                                                                <option value="Perl Harbour">Perl Harbour</option>
                                                                <option value="Spider Man">Spider Man</option>
                                                                <option value="Iron Man" selected>Iron Man</option>
                                                                <option value="Avatar">Avatar</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                        <button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">Save
                                                            changes</button>
                                                        <button type="reset" class="btn btn-light mb-1">Cancel</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="tab-pane  {{$page == "links" ? 'active' : ''}}" id="account-vertical-social" role="tabpanel"
                                             aria-labelledby="account-pill-social" aria-expanded="{{$page == "links" ? true : false}}" >
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>Facebook Page</label>
                                                            <input type="text" class="form-control"  name="links[facebook]" placeholder="Add link" value="{{current_user()->getMeta("facebook")}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label>Instagram</label>
                                                            <input type="text" class="form-control" name="links[instagram]" placeholder="Add link" value="{{current_user()->getMeta("instagram")}}">
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="tab-pane fade" id="account-vertical-connections" role="tabpanel"
                                             aria-labelledby="account-pill-connections" aria-expanded="false">
                                            <div class="row">
                                                <div class="col-12 my-2">
                                                    <a href="javascript: void(0);" class="btn btn-info">Connect to
                                                        <strong>Twitter</strong></a>
                                                </div>
                                                <hr>
                                                <div class="col-12 my-2">
                                                    <button
                                                        class=" btn btn-sm btn-light-secondary float-right">edit</button>
                                                    <h6>You are connected to facebook.</h6>
                                                    <p>Johndoe@gmail.com</p>
                                                </div>
                                                <hr>
                                                <div class="col-12 my-2">
                                                    <a href="javascript: void(0);" class="btn btn-danger">Connect to
                                                        <strong>Google</strong>
                                                    </a>
                                                </div>
                                                <hr>
                                                <div class="col-12 my-2">
                                                    <button
                                                        class=" btn btn-sm btn-light-secondary float-right">edit</button>
                                                    <h6>You are connected to Instagram.</h6>
                                                    <p>Johndoe@gmail.com</p>
                                                </div>
                                                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                    <button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">Save
                                                        changes</button>
                                                    <button type="reset" class="btn btn-light mb-1">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="account-vertical-notifications" role="tabpanel"
                                             aria-labelledby="account-pill-notifications" aria-expanded="false">
                                            <div class="row">
                                                <h6 class="m-1">Activity</h6>
                                                <div class="col-12 mb-1">
                                                    <div class="custom-control custom-switch custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" checked
                                                               id="accountSwitch1">
                                                        <label class="custom-control-label mr-1"
                                                               for="accountSwitch1"></label>
                                                        <span class="switch-label w-100">Email me when someone comments
                                                        onmy
                                                        article</span>
                                                    </div>
                                                </div>
                                                <div class="col-12 mb-1">
                                                    <div class="custom-control custom-switch custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" checked
                                                               id="accountSwitch2">
                                                        <label class="custom-control-label mr-1"
                                                               for="accountSwitch2"></label>
                                                        <span class="switch-label w-100">Email me when someone answers on
                                                        my
                                                        form</span>
                                                    </div>
                                                </div>
                                                <div class="col-12 mb-1">
                                                    <div class="custom-control custom-switch custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="accountSwitch3">
                                                        <label class="custom-control-label mr-1"
                                                               for="accountSwitch3"></label>
                                                        <span class="switch-label w-100">Email me hen someone follows
                                                        me</span>
                                                    </div>
                                                </div>
                                                <h6 class="m-1">Application</h6>
                                                <div class="col-12 mb-1">
                                                    <div class="custom-control custom-switch custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" checked
                                                               id="accountSwitch4">
                                                        <label class="custom-control-label mr-1"
                                                               for="accountSwitch4"></label>
                                                        <span class="switch-label w-100">News and announcements</span>
                                                    </div>
                                                </div>
                                                <div class="col-12 mb-1">
                                                    <div class="custom-control custom-switch custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="accountSwitch5">
                                                        <label class="custom-control-label mr-1"
                                                               for="accountSwitch5"></label>
                                                        <span class="switch-label w-100">Weekly product updates</span>
                                                    </div>
                                                </div>
                                                <div class="col-12 mb-1">
                                                    <div class="custom-control custom-switch custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" checked
                                                               id="accountSwitch6">
                                                        <label class="custom-control-label mr-1"
                                                               for="accountSwitch6"></label>
                                                        <span class="switch-label w-100">Weekly blog digest</span>
                                                    </div>
                                                </div>
                                                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                    <button type="submit" class="btn btn-primary glow mr-sm-1 mb-1">Save
                                                        changes</button>
                                                    <button type="reset" class="btn btn-light mb-1">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="row">
                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                <button type="submit"
                                                        class="btn  btn-primary waves-effect waves-light  submit-ajax-form "
                                                        id="profile-edit-button" data-form="#profile-edit-form">
                                        <span class="spinner-border spinner-border-sm" style="display: none;"
                                              role="status"></span><span
                                                        class="button-text text-nowrap">@lang("Save Changes")</span>
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- account setting page ends -->
@endsection

