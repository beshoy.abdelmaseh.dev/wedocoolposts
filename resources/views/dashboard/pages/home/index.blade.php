@extends('layouts.horizontalLayoutMaster')
{{-- page title --}}
@section('title',__('My Orders'))
{{-- vendor style --}}
@section('vendor-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/tables/datatable/datatables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/tables/datatable/responsive.bootstrap.min.css')}}">
@endsection
{{-- page style --}}
@section('page-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/app-invoice.css')}}">
@endsection

@section('content')
    <!-- invoice list -->
    <section class="invoice-list-wrapper m-0 mx-sm-2">
        <div class="text-center">
            <div class="brand-logo"><a href="{{route("home")}}"><img class="logo"  src="{{asset('assets/images/logo/logo.png')}}"></a></div>
        </div>
        <!-- create invoice button-->
        <div class="invoice-create-btn mb-1">
            <a href="{{route('orders.index')}}" class="btn btn-primary glow invoice-create" role="button" aria-pressed="true" >@lang("Order New Post")</a >
        </div>

        <div class="table-responsive loading-card ">
            <div class="card-content invisible" style="max-height: 150px !important;">
            <table class="table invoice-data-table dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>

                    <th></th>
                    <th>
                        <span class="align-middle">#@lang("Order")</span>
                    </th>
                    <th>@lang("Type")</th>
                    <th>@lang("Amount Due")</th>
                    <th>@lang("Order Status")</th>
                    <th>@lang("Payment Status")</th>
                    <th class="text-center">@lang("Actions")</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                <tr>

                    <td></td>
                    <td>
                        <a href="{{route('invoices.show',$order->invoice->slug)}}">{{$order->invoice->slug}}</a>
                    </td>
                    <td><span class="invoice-amount font-weight-bold">{{$order->main_question}}</span></td>
                    <td><span class="invoice-amount font-weight-bold">{{$order->invoice->amount}} <span class="dir-bold">{{currency_locale()}}</span></span></td>
                    <td><div class="{{optional($order->action->status)->class}} font-small-2">{{ucwords(optional($order->action->status)->name)}}</div></td>
                    <td>
                        @include("components.invoice-status",["state" => $order->invoice->state])
                    </td>
                    <td class="text-center">
                        <div class="invoice-action">
                            <a href="{{route('invoices.show',$order->invoice->slug)}}" class="btn  btn-info text-white">
                                <i class="bx bx-show-alt"></i>  <span class="align-center ml-25">@lang("Details")</span>
                            </a>
                            <a href="{{route('tracking.show',$order->invoice->slug)}}" class="btn  btn-primary text-white">
                                <i class="bx bx-target-lock"></i> <span class="align-center ml-25">@lang("Tracking")</span>
                            </a>
                        </div>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table></div>
        </div>
    </section>
@endsection
{{-- vendor scripts --}}
@section('vendor-scripts')
    <script src="{{asset('assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/tables/datatable/responsive.bootstrap.min.js')}}"></script>
@endsection
{{-- page scripts --}}
@section('page-scripts')
    <script>

        $(document).ready(function () {

            var getLanguage = {
                search: "",
                searchPlaceholder: '@lang("Search Order")',
            };
            if($("body").hasClass("dir-rtl")){
                 getLanguage = {
                    search: "",
                    searchPlaceholder: '@lang("Search Order")',
                    "sEmptyTable":     "ليست هناك بيانات متاحة في الجدول",
                    "sLoadingRecords": "جارٍ التحميل...",
                    "sProcessing":   "جارٍ التحميل...",
                    "sZeroRecords":  "لم يعثر على أية سجلات",
                    "sInfoPostFix":  "",
                    "oPaginate": {
                        "sFirst":    "الأول",
                        "sPrevious": "السابق",
                        "sNext":     "التالي",
                        "sLast":     "الأخير"
                    },
                };
            }
            setTimeout(function () {
                if($('.loading-card').length > 0) {
                    $('.loading-card').unblock();
                    $('.loading-card').find(".card-content").removeClass("invisible");
                    $('.loading-card').find(".card-content").css("max-height","");
                }
            }, 300);

            if ($(".invoice-data-table").length) {
                var dataListView = $(".invoice-data-table").DataTable({
                    "iDisplayLength": 50,

                    columnDefs: [
                      /*  {
                            targets: 0,
                            className: "control"
                        },
                        {
                            orderable: true,
                            targets: 1,
                            checkboxes: {selectRow: true}
                        },*/
                        {
                            targets: [0],
                            orderable: false
                        },
                    ],
                    //order: [2, 'asc'],
                    dom:
                        '<"top d-flex flex-wrap"<"action-filters flex-grow-1"f><"actions action-btns d-flex align-items-center">><"clear">rt<"bottom"p>',
                    language: getLanguage,
                    responsive: {
                       /* details: {
                            type: "column",
                            target: 0
                        }*/
                    }
                });
            }
        });
    </script>
@endsection
