<div class="question-form-component" id="question-form-{{$question->key}}" data-id="{{$question->id}}" data-input_type="{{$question->getMeta("input_type","none")}}" data-increment="{{$question->getMeta("increment","false")}}">
    @if(!empty($question->getMeta("html")))
        {!! str_replace(["{question_id}","{name}"],[$question->id,$name],$question->getNeta("html")) !!}
    @else
        {!! $question->getMeta("top_html") !!}
        <div class="{{$question->getMeta("parent_class")}}">
            <div class="form-group {{$question->getMeta("form_group_classes")}}">
                @if(!in_array($question->type,["radio","checkbox","switch"]) && !$question->getMeta("hide_label",false))
                    <label for="{{$question->key}}"><span class="{{$question->getMeta("label_class")}}">{!! $questionTitle !!} @if(!$question->getMeta("hide_price",false)) &nbsp;&nbsp;- <small class="text-primary">&nbsp;&nbsp; {{$question->price}}</small> @endif</span></label>
                @endif

                @if($question->type == "textfield")
                    <input type="text" name="{{$name}}" class="form-control {{$question->getMeta("element_class")}}" id="{{$question->key}}"  placeholder="{{$question->getMeta("placeholder")}}" {!! $question->getMeta("element_attr") !!}>
                @elseif($question->type == "textarea")
                    <textarea name="{{$name}}"  data-questionid="{{$question->id}}" class="form-control {{$question->getMeta("element_class")}}" id="{{$question->key}}" rows="4" placeholder="{{$question->getMeta("placeholder")}}" {!! $question->getMeta("element_attr") !!}></textarea>
                @elseif($question->type == "texteditor")
                    <textarea id="{{$question->key}}" data-questionid="{{$question->id}}" class="texteditor-summernote {{$question->getMeta("element_class")}}"  name="{{$name}}"  {!! $question->getMeta("element_attr") !!}></textarea>

                @elseif($question->type == "voice-note")
                    @include("components.voice-note",["text" => ["save" => "Save" , "save_record" => "Save Record"], "id" => $question->key, "class"=> $question->getMeta("element_class"),"store" => route("uploads.voice_note"), "inputname"=> $name , "type" => $question->getMeta("input_type","none")])

                @elseif($question->type == "radio")
                    <div class="radio radio-primary radio-glow">
                        <input name="{{$name}}" value="{{str_replace("(question_id)","question_id_".$question->id,$question->getMeta("default_value",null)) ?: "question_id_".$question->id}}" type="radio" id="{{$question->key}}" data-clone_question="{{$question->getMeta("clone_question","false")}}" data-clone_question_amount="{{$question->getMeta("clone_question_amount","false")}}" class="{{$question->getMeta("element_class")}}" {!! $question->getMeta("element_attr") !!}>
                        <label for="{{$question->key}}"> {{$question->title}} @if(!$question->getMeta("hide_price",false)) &nbsp;&nbsp;- <small class="text-primary">&nbsp;&nbsp; {{$question->price}}</small> @endif </label>
                    </div>
                @elseif($question->type == "checkbox")
                    <div class="checkbox checkbox-primary">
                        <input type="checkbox" class="form-control {{$question->getMeta("element_class")}}"  name="{{$name}}" id="{{$question->key}}"  {!! $question->getMeta("element_attr") !!}>
                        <label  for="{{$question->key}}"> {{$question->title}} @if(!$question->getMeta("hide_price",false)) &nbsp;&nbsp;- <small class="text-primary">&nbsp;&nbsp; {{$question->price}}</small> @endif</label>
                    </div>
                @elseif($question->type == "dropdown")
                    <select class="select2-theme form-control" id="select2-theme">
                        <option value="romboid">Romboid</option>
                        <option value="trapeze">Trapeze</option>
                        <option value="triangle">Triangle</option>
                        <option value="polygon">Polygon</option>
                        <option value="red">Red</option>
                        <option value="green">Green</option>
                        <option value="blue">Blue</option>
                        <option value="purple">Purple</option>
                    </select>
                @elseif($question->type == "switch")
                    <div class="custom-control custom-switch custom-control-inline mb-1">
                        <input type="checkbox" class="custom-control-input" name="{{$name}}" id="{{$question->key}}">
                        <label class="custom-control-label mr-1" for="{{$question->key}}">
                        </label>
                        <span>{{$question->title}} @if(!$question->getMeta("hide_price",false)) &nbsp;&nbsp;- <small class="text-primary">&nbsp;&nbsp; {{$question->price}}</small> @endif</span>
                    </div>
                @elseif($question->type == "file")
                    <div class="custom-file">
                        <input type="file" class="custom-file-input upload-file" data-store="{{route("uploads.store")}}" data-inputname="{{$name}}" id="{{$question->key}}">
                        <label class="custom-file-label" for="{{$question->key}}">@lang("Choose file")</label>
                    </div>
                    <small>@lang("locale.allowed_extensions")</small>
                    <div class="mt-2 ml-1">
                        <div class="progress progress-bar-primary mb-2 progress-input d-none">
                            <div class="progress-bar progress-label" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100" style="width:1%"></div>
                        </div>
                    </div>
                    <div >
                        <div class="row p-0 px-1 justify-content-md-start material-preview">
                        </div>
                    </div>
                @endif
                    <small>{{$question->desc}}</small>
                <span class="d-none {{$isChild ? 'ml-2' : ''}}"></span>
            </div>
        </div>
        {!! $question->getMeta("bottom_html") !!}
    @endif
</div>
