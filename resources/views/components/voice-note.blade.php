<div class="{{$class ?? null}}">
    <div class="note-tool note-voice-record note-btn-group" data-type="voice-record" >
        <div class="btn-group voice-note-actions" role="group">
            <div class="record-state d-none ml-1">
                <div class="bg-transparent  text-center  text-muted d-flex mr-1  mt-25">
                    <div style="height:18px;" class="">
                        <div class="loader-animated text-center center m-auto mt-1"></div>
                    </div>
                    <div class="d-inline-flex ml-75"> Recording: <div class="ml-25 record_time_display">00:00</div></div>
                </div>
            </div>
            <div class="save-voice-btn ml-25 d-none">
                <button type="button"
                        class="save-voice-recording note-btn btn-success text-white d-inline-flex submit-ajax-form"  id="{{$id}}" data-type="{{$type}}" data-inputname="{{$inputname ?? false}}"  data-action="{{$store}}"  title=""  tabindex="-1" data-original-title="{{$text["save_record"]}}">
                                        <span class="spinner-border spinner-border-sm" style="display: none;"
                                              role="status"></span><span
                        class="button-text d-flex"><i class="bx bxs-send mr-25"></i> <span class="text">{{$text["save"]}}</span></span>
                </button>
            </div>
            <div class="cancel-voice-btn ml-25 d-none">
                <button  type="button"  class="cancel-voice-recording note-btn btn-danger d-inline-flex text-white" title=""  tabindex="-1" data-original-title="Cancel Record"><i class="bx bx-x-circle"></i></button>
            </div>
            <div class="start-voice-btn ml-25">
                <button type="button" class="start-voice-recording note-btn d-inline-flex" title="" tabindex="-1" data-original-title="Record Voice Note"><i class="bx bxs-microphone mr-25"></i> Voice Note</button>
            </div>

        </div>
    </div>
</div>
<div class="recorded-voice-notes card-box shadow rounded-lg m-25 p-50 @if(isset($materials) && $materials && $materials->where("type","voice-note")->isNotEmpty()) @else d-none @endif">
    <div class="row">
    @if(isset($materials) && $materials)
            @foreach($materials->where("type","voice-note") as $voiceNote)
                <div class="mb-25 col-sm-5 col-md-6">
                    <input type="hidden" name="voice_notes[]" value="{{$voiceNote->storage_path}}_{{$voiceNote->created_at}}">
                    <audio src="{{route("uploads.show",$voiceNote->storage_path)}}" controls="" style="width:100%"></audio>
                </div>
            @endforeach
        @endif
    </div>
</div>
