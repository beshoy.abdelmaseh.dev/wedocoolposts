<div class="show-actions-component" id="show-action-content-{{optional($actionContent)->id ?: rand(1,50)}}" >
    @php($data = optional(optional($actionContent)->data)->first())
    @if(!empty(strip_tags($data)) || !empty($materials) || !empty($voiceNotes))
    <div class="mb-1 mx-1 mt-0 p-1 shadow rounded-lg " style=" background-color: #fafafa !important;">
        {!! $data !!}
        @if(!empty($voiceNotes))
        <div class="row p-0 mb-1 justify-content-md-start voice-preview">
            @foreach($voiceNotes as $voiceNote)
                <div class="mb-25 col-sm-3 col-md-4">
                    <audio src="{{route("uploads.show",$voiceNote->storage_path)}}" controls="" style="width:100%"></audio>
                </div>
            @endforeach
        </div>
            @endif

        <div class="row p-0 px-1 justify-content-md-start material-preview">
            @foreach($materials as $material)
                @php($file = explode(".",$material->storage_path))
                @php($extension = end($file) )
                <div class="mb-50 main-review">
                    <div class="position-relative ">
                        <a href="{{route("uploads.show",$material->storage_path)}}" {{($extension == "pdf" ? 'target=_blank' :'class=image-preview' )}} >
                            <img  src="{{$extension == "pdf" ? asset("assets/images/icon/pdf-2.png") : route("uploads.show",$material->storage_path)}}" class="img img-thumbnail rounded-lg mr-1"   {{($extension == "pdf" ? 'width=60' :  'width=100' )}} />
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
     @endif
</div>
