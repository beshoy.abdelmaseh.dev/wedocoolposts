@if(!isset($type))

@if($state == "full-paid")
    <div class="badge badge-light-success badge-pill {{$class ?? ''}}">@lang("locale.invoice.state.".$state)</div>
@elseif($state == "half-paid")
    <div class="badge badge-light-primary badge-pill {{$class ?? ''}}">@lang("locale.invoice.state.".$state)</div>
@else
    <div class="badge badge-light-danger badge-pill {{$class ?? ''}}">@lang("locale.invoice.state.unpaid")</div><br>
@endif

    @elseif($type == "glow")

    @if($state == "full-paid")
        <div class="badge badge-pill badge-glow badge-success m-1 py-1 px-3 font-medium-4 {{$class ?? ''}}">@lang("locale.invoice.state.".$state)</div>
    @elseif($state == "half-paid")
        <div class="badge badge-pill badge-glow badge-primary m-1 py-1 px-3 font-medium-4 {{$class ?? ''}}">@lang("locale.invoice.state.".$state)</div>
    @else
        <div class="badge badge-pill badge-glow badge-danger m-1 py-1 px-3 font-medium-4 {{$class ?? ''}}">@lang("locale.invoice.state.unpaid")</div><br>

    @endif

@endif
