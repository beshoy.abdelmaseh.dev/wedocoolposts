<div class="modal fade remote-modal" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    <div class="d-flex justify-content-center">@lang("Loading....")</div>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body p-3 loader">
                <div class="d-flex justify-content-center">
                    <div class="spinner-border" role="status"></div>
                </div>
            </div>
            <div class="remote-content d-none">

            </div>
        </div>
    </div>
</div><!-- /.modal -->
