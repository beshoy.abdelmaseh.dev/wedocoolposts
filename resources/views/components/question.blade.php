<div class="question-component {{$question->getMeta("col_class","col-12")}}" id="question-{{$question->key}}" data-id="{{$question->id}}" data-type="{{$question->type}}">
            <x-question-form-component :question="$question" :isChild="$isChild ?? null" :groupId="$groupId ?? null" />
            @if($question->relationLoaded("children") && $question->children)
                @php($subQuestions = $question->children->where("state","active")->sortBy("sorting"))
                @foreach($subQuestions as $subQuestion)
                    <x-question-component :question="$subQuestion" isChild="true" :groupId="$groupId" />
                @endforeach
            @endif
</div>
