@if(!isset($alert_theme) || (isset($alert_theme) && $alert_theme == "default"))

    <div id="{{$alert_id ?? ""}}"  class="alert alert-{{$alert_type ?? "light"}} {{isset($dismissible) ? "alert-dismissible" : ""}} {{$custom_class ?? ""}}" role="alert" data-type="{{$alert_type ?? "light"}}">
        {{isset($dismissible) ? '<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span></button>' : "" }}
        <div class="alert_content">{!!  $alert_content ?? ""!!}</div>
    </div>

@elseif($alert_theme == "background")

    <div id="{{$alert_id ?? ""}}" class="alert alert-{{$alert_type ?? "light"}} bg-{{$alert_type ?? "light"}} text-white border-0 {{isset($dismissible) ? "alert-dismissible" : ""}} {{$custom_class ?? ""}}" role="alert" data-type="{{$alert_type ?? "light"}}">
        {{isset($dismissible) ? '<button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span></button>' : "" }}
        <div class="alert_content">{!!  $alert_content ?? "" !!}</div>
    </div>

@endif
