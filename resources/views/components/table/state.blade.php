@switch($state)
@case('Active')
<span class="badge badge-success badge-pill d-inline">{{$state}}</span>
@break

@case('Verified')
@case('Scheduled')
<span class="badge badge-secondary badge-pill d-inline">{{$state}}</span>
@break

@case('inActive')
<span class="badge badge-danger badge-pill d-inline">{{$state}}</span>
@break

@case('Disabled')
<span class="badge badge-danger badge-pill d-inline">{{$state}}</span>
@break

@case('Cancelled')
<span class="badge badge-danger badge-pill d-inline">{{$state}}</span>
@break

@case('Pending')
<span class="badge badge-blue badge-pill d-inline">{{$state}}</span>
@break

@case('Confirmed')
@case('Completed')
<span class="badge badge-success badge-pill d-inline {{isset($class) ? $class : ''}}">{{$state}}</span>
@break

@case('Reviewing')
@case('Under Review')
<span class="badge badge-info badge-pill d-inline">{{$state}}</span>
@break

@case('Hidden')
<span class="badge badge-secondary badge-pill d-inline">{{$state}}</span>
@break

@case('Deleted')
<span class="badge badge-danger badge-pill d-inline">{{$state}}</span>
@break

@default
<span class="badge badge-light badge-pill d-inline">Unknown</span>
@endswitch
