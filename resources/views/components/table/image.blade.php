@if(isset($image_popup))
    <a href="{{$src}}" class="image-popup" title="{{$title}}">
        <img src="{{$thumbnail}}" class="{{$image_class}}" height="{{$thumbnail_height}}">
    </a>
@else
    <img src="{{$src}}" class="{{$image_class}}" height="32">
@endif