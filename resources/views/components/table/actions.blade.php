@if(empty($actions))
    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="No Permissions"
       href="javascript:void(0);" class="action-icon"> <i class="fa fa-ban"></i></a>
@endif

@if(in_array("view",$actions))
    @if(isset($modal) && array_key_exists("view",$modal))
        <span data-toggle="tooltip" data-placement="top" title="" data-original-title="View" data-toggle="modal"
              data-target="{{$modal["view"]}}" data-url="{{route($route[0].".show",$route[1]  ??  null)}}" class="action-icon cursor-pointer datatable-view-row"> <i class="mdi mdi-eye"></i></span>
    @else
        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="View"
           href="{{route(str_replace("ajax.","",$route[0]).".show",$route[1]  ??  null)}}" class="action-icon "> <i class="mdi mdi-eye"></i></a>

    @endif
@endif
@if(in_array("edit",$actions))
    @if(isset($modal) && array_key_exists("edit",$modal))
        <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" data-toggle="modal"
              data-target="{{$modal["edit"]}}" data-url="{{route($route[0].".edit",$route[1]  ??  null)}}" class=" action-icon cursor-pointer datatable-edit-row mr-1"> <i class="bx bx-edit-alt"></i></span>
    @else
        <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"
           href="{{route(str_replace("ajax.","",$route[0]).".edit",$route[1]  ??  null)}}" class="action-icon"> <i class="mdi mdi-square-edit-outline"></i></a>
    @endif
@endif
@if(in_array("delete",$actions))
    <span data-name="{{$warning_name}}" data-table="{{$dataTable_id}}" data-url="{{route($route[0].".destroy",$route[1]  ??  null)}}"
          data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"
          class="action-icon sa-warning datatable-delete-row cursor-pointer "> <i class="bx bx-trash"></i></span>
@endif

