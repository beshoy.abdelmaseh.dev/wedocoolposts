<tr class="question-table-component">
                    <td ></td>
                    <td>
                        {{$question->id}} @if($question->state == "deactivated") <span class="badge badge-light-danger m-0" style="font-size: 10px; padding:2px;">DeActivated</span> @endif
                    </td>
                    <td><span class="text-wrap">{{$question->title}}</span></td>
                    <td><span class="invoice-amount">{{$question->price}}</span></td>
                    <td><small class="text-muted" style="cursor: pointer;"  data-toggle="tooltip"  data-original-title="{{$question->key}}" data-placement="top">{{Str::limit($question->key,10)}}</small></td>
                    <td><small class="text-muted">{{$question->sorting}}</small></td>
                    <td><small class="text-muted">{{$question->group_id}}</small></td>
                    <td><small class="text-muted">{{$question->type}}</small></td>
                    <td><div  style="cursor: pointer;" class="text-muted" data-toggle="tooltip"  data-original-title="{{optional($question->parent)->title}} - {{optional($question->parent)->type}}" data-placement="left">#{{$question->parent_id}}</div></td>
                    <td class="text-center ">
                        <div class="invoice-action font-small-3 d-flex justify-content-center">
                            <a href="{{route("admin.questions.edit",$question->id)}}" class=" text-info mr-1">Edit</a>
                            <a href="{{route("admin.questions.duplicate",$question->id)}}" class=" text-primary mr-1">Duplicate</a>
                            <form method="POST" action="{{route("admin.questions.destroy",$question->id)}}" class="p-0 m-0">
                                @csrf
                                @method("DELETE")
                                <input type="submit" class=" font-small-3 border-0 text-danger bg-transparent m-0 p-0" value="Delete">
                            </form>
                        </div>
                    </td>
</tr>
            @if($question->relationLoaded("children") && $question->children)
                @php($subQuestions = $question->children->sortBy("sorting"))
                @foreach($subQuestions as $subQuestion)
                    <x-question-table-component :question="$subQuestion"  />
                @endforeach
            @endif

