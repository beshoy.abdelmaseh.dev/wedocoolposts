@extends('layouts.fullLayoutMaster')
{{-- title --}}
@section('title','Login')

@section('content')
    <!-- login page start -->
    <section id="auth-login" class="row justify-content-center align-items-center mt-sm-5">
        <div class="col-xl-8 col-11 p-0 px-sm-1">
            <div class="card bg-authentication mb-0">
                <div class="row m-0">
                    <!-- left section-login -->
                    <div class="col-md-6 col-12 px-0">
                        <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                            <div class="brand-logo text-center"><a href="{{route("home")}}"><img class="logo"  src="{{asset('assets/images/logo/logo.png')}}"></a></div>
                            <div class="card-header pb-0">
                                <div class="card-title">
                                    <h3 class="text-center mb-2">@lang("auth.welcome_back")</h3>
                                </div>
                            </div>
                            <div class="card-content">
                                <div class="card-body p-0 px-sm-2">
                                    <div class="d-none flex-md-row flex-column justify-content-around">
                                        <a href="#" class="btn btn-social btn-google btn-block font-small-3 mr-md-1 mb-md-0 mb-1">
                                            <i class="bx bxl-google font-medium-3"></i>
                                            <span class="pl-50 d-block text-center">Google</span>
                                        </a>
                                        <a href="#" class="btn btn-social btn-block mt-0 btn-facebook font-small-3">
                                            <i class="bx bxl-facebook-square font-medium-3"></i>
                                            <span class="pl-50 d-block text-center">Facebook</span>
                                        </a>
                                    </div>
                                    <div class="divider d-none">
                                        <div class="divider-text text-uppercase text-muted">
                                            <small>or login with email</small>
                                        </div>
                                    </div>
                                    {{-- form  --}}
                                    <form method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="form-group mb-50">
                                            <label class="text-bold-600" for="email">{{__("Email Address")}} {{__("or")}} {{__("Phone Number")}}</label>
                                            <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{old("email")}}"  autocomplete="email" autofocus >
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label class="text-bold-600" for="password">{{__("Password")}}</label>
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="current-password">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                                            @enderror
                                        </div>
                                        <div class="form-group d-flex justify-content-between align-items-center">
                                            <div class="text-left">
                                                <div class="checkbox checkbox">
                                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="remember">
                                                        <small>@lang("auth.remember_me")</small>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="text-right">
                                                <a href="{{ route('password.request') }}" class="card-link"><small>@lang("auth.forget_password")</small></a>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary glow w-100 position-relative">@lang("auth.login")
                                            <i id="icon-arrow" class="bx bx-{{class_by_dir("right")}}-arrow-alt"></i>
                                        </button>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <small class="mr-25">@lang("auth.dont_have_account")</small>
                                        <a href="{{route('register')}}"><small>@lang("auth.register")</small></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- right section image -->
                    <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                        <div class="card-content">
                            <img class="img-fluid" src="{{asset('assets/images/pages/login.png')}}" alt="branding logo">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- login page ends -->
@endsection
