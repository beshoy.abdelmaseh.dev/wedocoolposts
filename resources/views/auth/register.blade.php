@extends('layouts.fullLayoutMaster')

{{-- page title --}}
@section('title',__("auth.register"))
{{-- page scripts --}}


@section('content')
    <!-- register section starts -->
    <section class="row justify-content-center align-items-center mt-sm-5">
        <div class="col-xl-8 col-11 p-0 px-sm-1">
            <div class="card bg-authentication mb-0">

                <div class="row m-0">
                    <!-- register section left -->
                    <div class="col-md-6 col-12 px-0">
                        <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                            <div class="brand-logo text-center"><a href="{{route("home")}}"><img class="logo"  src="{{asset('assets/images/logo/logo.png')}}"></a></div>
                            <div class="card-header pb-1">
                                <div class="card-title">
                                    <h4 class="text-center mb-0">@lang("auth.register")</h4>
                                </div>
                            </div>
                            <div class="text-center">
                                <p> <small> @lang("auth.register_hint")</small>
                                </p>
                            </div>
                            <div class="card-content">
                                <div class="card-body p-0 px-sm-2">

                                    <form method="POST" action="{{ route('register') }}">
                                        @csrf
                                        <div class="form-group mb-50">
                                            <label class="text-bold-600" for="full_name">@lang("Full Name")</label>
                                            <div class="position-relative has-icon-left">
                                            <input id="full_name" type="text" class="form-control @error('full_name') is-invalid @enderror" name="full_name" value="{{ old('full_name') }}"  autocomplete="name" autofocus placeholder="@lang("Full Name")">
                                                @error('full_name')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                             </span>
                                                @enderror
                                                <div class="form-control-position">
                                                    <i class="bx bx-user"></i>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group mb-50">
                                            <label class="text-bold-600" for="phone">@lang("Phone Number")</label>
                                            <div class="position-relative has-icon-left">
                                            <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}"  autocomplete="phone" autofocus placeholder="@lang("Phone Number")">
                                                @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                             </span>
                                                @enderror
                                            <div class="form-control-position">
                                                <i class="bx bx-mobile"></i>
                                            </div>
                                        </div>

                                        </div>
                                        <div class="form-group mb-50">
                                            <label class="text-bold-600" for="email">@lang("Email Address")</label>
                                            <div class="position-relative has-icon-left">
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" placeholder="@lang("Email Address")">
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                          </span>
                                                @enderror
                                                <div class="form-control-position">
                                                <i class="bx bx-mail-send"></i>
                                            </div>
                                        </div>

                                        </div>
                                        <div class="row">
                                        <div class="col-md-6">
                                        <div class="form-group mb-50">
                                            <label class="text-bold-600" for="password">@lang("Password")</label>
                                            <div class="position-relative has-icon-left">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password" placeholder="@lang("Password")">
                                                @error('password')
                                                <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                        </span>
                                                @enderror
                                                <div class="form-control-position">
                                                    <i class="bx bx-lock"></i>
                                                </div>
                                            </div>

                                        </div>
                                        </div>
                                        <div class="col-md-6">
                                        <div class="form-group mb-2">
                                            <label class="text-bold-600" for="password-confirm">@lang("Confirm Password")</label>
                                            <div class="position-relative has-icon-left">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password" placeholder="@lang("Confirm Password")">
                                                <div class="form-control-position">
                                                    <i class="bx bx-lock"></i>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>

                                        <button type="submit" class="btn btn-primary glow position-relative w-100">@lang("auth.sign_up")<i
                                                id="icon-arrow" class="bx bx-{{class_by_dir("right")}}-arrow-alt"></i></button>
                                    </form>
                                    <hr>
                                    <div class="text-center"><small class="mr-25">@lang("auth.already_have_account")</small>
                                        <a href="{{asset('login')}}"><small>@lang("auth.sign_in")</small> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- image section right -->
                    <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                        <img class="img-fluid" src="{{asset('assets/images/pages/register.png')}}" alt="branding logo">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- register section endss -->
@endsection
