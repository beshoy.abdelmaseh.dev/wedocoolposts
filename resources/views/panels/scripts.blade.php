
    <!-- BEGIN: Vendor JS-->
    <script>
        var assetBaseUrl = "{{ asset('') }}";
    </script>
    <script src="{{mix('assets/vendors/js/vendors.min.js')}}"></script>
    <script src="{{mix('assets/fonts/LivIconsEvo/js/liviconsevo-init.js')}}"></script>
    <script src="{{mix('assets/js/packages.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    @yield('vendor-scripts')
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    @if(config('theme.custom.mainLayoutType') == 'vertical-menu')
    <script src="{{mix('assets/js/scripts/configs/vertical-menu-light.js')}}"></script>
    @else
    <script src="{{mix('assets/js/scripts/configs/horizontal-menu.js')}}"></script>
    @endif


    <script src="{{mix('assets/js/bundles.js')}}"></script>
    <script src="{{mix('assets/js/inits.js')}}"></script>
    <script src="{{mix('assets/custom/js/scripts.js')}}"></script>
    <script src="{{mix('assets/js/app.js')}}"></script>
    <script src="{{mix('assets/vendors/js/voice-recorder-polyfill.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    @yield('page-scripts')
    <!-- END: Page JS-->

    @stack('scripts')
