<section class="kb-search">
    <div class="row">
        <div class="col-12">
            <div class="card bg-transparent shadow-none mb-1">
                <div class="card-content">
                    <div class="card-body text-center pt-0">
                        <div class="brand-logo "><a href="{{route("home")}}"><img class="logo"  src="{{asset('assets/images/logo/logo.png')}}"></a></div>
                        <h1 class=" mb-1 kb-title d-none">first question , How can we help you today?</h1>
                        <p class=" mb-0 font-medium-2 mt-2">
                            {!! settings("desc") !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
