{{-- style blade file --}}
    <link href="{{mix('assets/css/fonts.css')}}" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{mix('assets/vendors/css'.(theme_dir_assets()).'vendors.min.css')}}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{mix('assets/css'.(theme_dir_assets()).'bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{mix('assets/css/bundles.css')}}">
    <link rel="stylesheet" type="text/css" href="{{mix('assets/css/packages.css')}}">

    @yield('vendor-styles')

    @if(config('theme.custom.direction') === 'rtl')
    <link rel="stylesheet" type="text/css" href="{{mix('assets/css-rtl/custom.css')}}">
    @endif
    <!-- END: Theme CSS-->

    <!-- BEGIN: Page CSS-->
    @if( config('theme.custom.mainLayoutType') == 'horizontal-menu')
    <link rel="stylesheet" type="text/css" href="{{mix('assets/css/core/menu/menu-types/horizontal-menu.css')}}">
    @else
    <link rel="stylesheet" type="text/css" href="{{mix('assets/css/core/menu/menu-types/vertical-menu.css')}}">
    @endif
    @yield('page-styles')
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{mix('assets/custom/css'.(theme_dir_assets("path")).'style.css')}}">
    <!-- END: Custom CSS-->

    <link rel="stylesheet" type="text/css" href="{{mix('assets/css/app.css')}}">

    @stack('styles')

