{{-- navabar  --}}

<nav
    class="header-navbar {{config('theme.custom.fulllayout') ? '' : 'main-header-navbar'}}  navbar-expand-lg navbar navbar-with-menu border-0 {{ config('theme.custom.navbarType') }}"
    data-bgcolor="@if(isset($configData['navbarBgColor'])){{$configData['navbarBgColor']}}@endif">
    <div class="navbar-wrapper">
        <div class="navbar-container content">
            <div class="navbar-collapse" id="navbar-mobile">
                <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                    <ul class="nav navbar-nav d-none">
                        <li class="nav-item mobile-menu d-xl-none mr-auto"><a
                                class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
                                    class="ficon bx bx-menu"></i></a></li>
                    </ul>
                    <ul class="nav navbar-nav d-none">
                        <li class="nav-item nav-dropdown-user">
                            <div class=" nav-link p-1 d-flex align-items-center shadow rounded-lg"  >
                                <span> <i class="bx bx-phone-call primary font-large-1 text-muted"></i></span>
                                <div class="user-nav ">
                                    <span class="text-left font-weight-bold ml-1 font-medium-1">+20 128 756 2568</span><br>
                                    <span class="user-status text-muted  ml-1">we are always happy to help!</span>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item nav-dropdown-user ml-1 d-none d-md-block">
                            <div class=" nav-link p-1 d-flex align-items-center shadow rounded-lg"  >
                                <span> <i class="bx bx-mail-send primary font-large-1 text-muted"></i></span>
                                <div class="user-nav ">
                                    <span class="text-left font-weight-bold ml-1 font-medium-1">hello@help.com</span><br>
                                    <span class="user-status text-muted  ml-1">Best way to get answer faster!</span>
                                </div>
                            </div>
                        </li>
                    </ul>

                </div>
                <ul class="nav navbar-nav float-right">
                    <li class="dropdown dropdown-language nav-item">
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        @if($localeCode == LaravelLocalization::getCurrentLocale())
                                <a class="dropdown-toggle nav-link" id="dropdown-flag" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="flag-icon flag-icon-{{$properties['flag']}}"></i><span class="selected-language-disabled"> {{ ucwords($properties['native']) }}</span>
                                </a>
                            @else
                                <div class="dropdown-menu" aria-labelledby="dropdown-flag">
                                    <a class="dropdown-item" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" data-language="{{ $localeCode }}">
                                        <i class="flag-icon flag-icon-{{$properties['flag']}} mr-50"></i> <span  @if($localeCode == "ar") class="font-medium-1 " @endif style="text-transform: none !important;">{{ ucwords($properties['native']) }}</span>
                                    </a>
                                </div>
                            @endif
                    @endforeach
                    </li>
                    <li class="dropdown dropdown-notification nav-item d-none"><a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i
                                class="ficon bx bx-bell bx-tada bx-flip-horizontal"></i><span
                                class="badge badge-pill badge-danger badge-up">5</span></a>
                        <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                            <li class="dropdown-menu-header">
                                <div class="dropdown-header px-1 py-75 d-flex justify-content-between"><span
                                        class="notification-title">7 new Notification</span><span
                                        class="text-bold-400 cursor-pointer">Mark all as read</span></div>
                            </li>
                            <li class="scrollable-container media-list"><a class="d-flex justify-content-between"
                                                                           href="javascript:void(0)">
                                    <div class="media d-flex align-items-center">
                                        <div class="media-left pr-0">
                                            <div class="avatar mr-1 m-0"><img
                                                    src="{{asset('assets/images/portrait/small/avatar-s-11.jpg')}}"
                                                    alt="avatar" height="39" width="39"></div>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading"><span class="text-bold-500">Congratulate Socrates Itumay</span>
                                                for work anniversaries</h6><small class="notification-text">Mar 15
                                                12:32pm</small>
                                        </div>
                                    </div>
                                </a>
                                <div class="d-flex justify-content-between read-notification cursor-pointer">
                                    <div class="media d-flex align-items-center">
                                        <div class="media-left pr-0">
                                            <div class="avatar mr-1 m-0"><img
                                                    src="{{asset('assets/images/portrait/small/avatar-s-16.jpg')}}"
                                                    alt="avatar" height="39" width="39"></div>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading"><span class="text-bold-500">New Message</span>
                                                received</h6><small class="notification-text">You have 18 unread
                                                messages</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between cursor-pointer">
                                    <div class="media d-flex align-items-center py-0">
                                        <div class="media-left pr-0"><img class="mr-1"
                                                                          src="{{asset('assets/images/icon/sketch-mac-icon.png')}}"
                                                                          alt="avatar" height="39" width="39"></div>
                                        <div class="media-body">
                                            <h6 class="media-heading"><span
                                                    class="text-bold-500">Updates Available</span></h6><small
                                                class="notification-text">Sketch 50.2 is currently newly added</small>
                                        </div>
                                        <div class="media-right pl-0">
                                            <div class="row border-left text-center">
                                                <div class="col-12 px-50 py-75 border-bottom">
                                                    <h6 class="media-heading text-bold-500 mb-0">Update</h6>
                                                </div>
                                                <div class="col-12 px-50 py-75">
                                                    <h6 class="media-heading mb-0">Close</h6>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between cursor-pointer">
                                    <div class="media d-flex align-items-center">
                                        <div class="media-left pr-0">
                                            <div class="avatar bg-primary bg-lighten-5 mr-1 m-0 p-25"><span
                                                    class="avatar-content text-primary font-medium-2">LD</span></div>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading"><span class="text-bold-500">New customer</span> is
                                                registered</h6><small class="notification-text">1 hrs ago</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="cursor-pointer">
                                    <div class="media d-flex align-items-center justify-content-between">
                                        <div class="media-left pr-0">
                                            <div class="media-body">
                                                <h6 class="media-heading">New Offers</h6>
                                            </div>
                                        </div>
                                        <div class="media-right">
                                            <div class="custom-control custom-switch">
                                                <input class="custom-control-input" type="checkbox" checked
                                                       id="notificationSwtich">
                                                <label class="custom-control-label" for="notificationSwtich"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between cursor-pointer">
                                    <div class="media d-flex align-items-center">
                                        <div class="media-left pr-0">
                                            <div class="avatar bg-danger bg-lighten-5 mr-1 m-0 p-25"><span
                                                    class="avatar-content"><i
                                                        class="bx bxs-heart text-danger"></i></span></div>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading"><span class="text-bold-500">Application</span> has
                                                been approved</h6><small class="notification-text">6 hrs ago</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between read-notification cursor-pointer">
                                    <div class="media d-flex align-items-center">
                                        <div class="media-left pr-0">
                                            <div class="avatar mr-1 m-0"><img
                                                    src="{{asset('assets/images/portrait/small/avatar-s-4.jpg')}}" alt="avatar"
                                                    height="39" width="39"></div>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading"><span class="text-bold-500">New file</span> has
                                                been uploaded</h6><small class="notification-text">4 hrs ago</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between cursor-pointer">
                                    <div class="media d-flex align-items-center">
                                        <div class="media-left pr-0">
                                            <div class="avatar bg-rgba-danger m-0 mr-1 p-25">
                                                <div class="avatar-content"><i class="bx bx-detail text-danger"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading"><span class="text-bold-500">Finance report</span>
                                                has been generated</h6><small class="notification-text">25 hrs
                                                ago</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between cursor-pointer">
                                    <div class="media d-flex align-items-center border-0">
                                        <div class="media-left pr-0">
                                            <div class="avatar mr-1 m-0"><img
                                                    src="{{asset('assets/images/portrait/small/avatar-s-16.jpg')}}"
                                                    alt="avatar" height="39" width="39"></div>
                                        </div>
                                        <div class="media-body">
                                            <h6 class="media-heading"><span class="text-bold-500">New customer</span>
                                                comment recieved</h6><small class="notification-text">2 days ago</small>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown-menu-footer"><a
                                    class="dropdown-item p-50 text-primary justify-content-center"
                                    href="javascript:void(0)">Read all notifications</a></li>
                        </ul>
                    </li>

                    @guest
                        @if(!config('theme.custom.fulllayout'))
                        <li class="nav-item "><a class="nav-link nav-link-search" href="{{route('login')}}"><i
                                    class="ficon bx bx-log-in"></i> @lang("auth.sign_in")</a></li>
                            @else
                            <li class="nav-item "><span class="p-2"></span></li>
                        @endif
                    @else
                        <li class="nav-item "><a class="nav-link nav-link-search" href="{{route("tracking.search")}}"><i
                                    class="ficon bx bx-target-lock"></i> @lang("Tracking") @lang("Order")</a></li>
                        <li class="dropdown dropdown-user nav-item">
                            <a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav  ">
                                    <div class="user-nam "> <div class="d-none d-sm-block">{{current_user()->full_name}}</div></div>
                                    <span class="user-status text-muted d-none">Available</span>
                                </div>
                                <span><img class="round" src="{{asset('assets/images/portrait/small/no-avatar.jpg')}}"
                                           alt="avatar" height="40" width="40"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right pb-0">
                                <a class="dropdown-item" href="{{route('dashboard.profile.edit')}}">
                                    <i class="bx bx-user-circle mr-50"></i> @lang("My Profile")
                                </a>
                                <a class="dropdown-item" href="{{route('dashboard.index')}}">
                                    <i class="bx bx-list-check mr-50"></i> @lang("My Orders")
                                </a>
                                <div class="dropdown-divider mb-0 d-none"></div>
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <button class="dropdown-item" type="submit"><i
                                            class="bx bx-power-off mr-50"></i>@lang("auth.logout")</button>
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </div>
</nav>
