@extends('layouts.contentLayoutMaster')

{{-- title --}}
@section('title','Edit Question')
{{-- venodr style --}}
@section('vendor-styles')

@endsection

{{-- page style --}}
@section('page-styles')

@endsection

@section('content')
    <section id="basic-input" class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header pb-0">
                        <h4 class="card-title">Edit Question</h4>
                        <hr>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form class="ajax-form" data-alert="#edit-question-alert" enctype="multipart/form-data"
                                  data-scrolltoalert="true"
                                  data-button="#edit-question-button" id="edit-question-form" method="POST"
                                  action="{{route("admin.questions.update",$question->id)}}">
                                @method("PUT")
                                <div class="row mt-1">
                                    <div class="col-md-12">
                                        @component('components.alert')
                                            @slot('alert_theme')  background  @endslot
                                            @slot('alert_type')  {{session('alert_message')["type"]}} @endslot
                                            @slot('alert_content')  {!! session('alert_message')["content"] !!}  @endslot
                                            @slot('custom_class') mb-1 {{session('alert_message') ? "" : "d-none"}} @endslot
                                            @slot('alert_id')  edit-question-alert  @endslot
                                        @endcomponent
                                    </div>
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label for="question-title">Question Title</label>
                                            <input type="text" class="form-control" id="question-title" name="title" data-additional="generate-slug" data-field="#question-create-key" value="{{$question->title}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="question-title-ar">Question Title (باللغة العربية)</label>
                                            <input type="text" class="form-control" id="question-title-ar" name="translatable[title:ar]" dir="rtl" value="{{optional($question->translate('ar'))->title}}">
                                        </div>

                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label for="question-create-key">Question Key</label>
                                            <input type="text" class="form-control" name="key" id="question-create-key" data-empty_only="true" value="{{$question->key}}">
                                            <small class="text-danger">Must be unique.</small>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <div class="form-group">
                                                    <div class="form-inline justify-content-between">
                                                        <div>
                                                            <div  class="custom-control custom-switch custom-switch-dark custom-control-inline mb-1">
                                                                <input type="checkbox" class="custom-control-input" name="state" id="deactivate-question"  {{$question->state == "deactivated" ? "checked" : ""}}>
                                                                <label class="custom-control-label mr-1" for="deactivate-question">
                                                                </label>
                                                                <span>DeActivate</span>
                                                            </div>
                                                            <div  class="custom-control custom-switch custom-control-inline mb-1 ml-3">
                                                                <input type="checkbox" class="custom-control-input" name="main" id="main-question" {{$question->main ? "checked" : ""}}>
                                                                <label class="custom-control-label mr-1" for="main-question">
                                                                </label>
                                                                <span>Is Main Question</span>
                                                            </div>
                                                        </div>
                                                        <ul class="list-unstyled mb-0">
                                                            <li class="d-inline-block mr-2 mb-1">
                                                                <fieldset>
                                                                    <div class="radio radio-primary radio-glow">
                                                                        <input type="radio" id="radioGlow12" name="meta[form]" value="" {{empty($question->getMeta("form")) ? "checked" : ""}}>
                                                                        <label for="radioGlow12">None</label>
                                                                    </div>
                                                                </fieldset>
                                                            </li>
                                                            <li class="d-inline-block mr-2 mb-1">
                                                                <fieldset>
                                                                    <div class="radio radio-primary radio-glow">
                                                                        <input type="radio" id="radioGlow1" name="meta[form]" value="wizard" {{$question->getMeta("form") == "wizard" ? "checked" : ""}}>
                                                                        <label for="radioGlow1">Wizard Form (multi steps)</label>
                                                                    </div>
                                                                </fieldset>
                                                            </li>
                                                            <li class="d-inline-block mr-2 mb-1">
                                                                <fieldset>
                                                                    <div class="radio radio-primary radio-glow">
                                                                        <input type="radio" id="radioGlow2" name="meta[form]" value="default" {{$question->getMeta("form") == "default" ? "checked" : ""}}>
                                                                        <label for="radioGlow2">Default Form</label>
                                                                    </div>
                                                                </fieldset>
                                                            </li>
                                                        </ul></div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="question-parent">Select Parent Question</label>
                                                    <select class="form-control select2" id="question-parent" name="parent_id">
                                                        <option value="">None</option>
                                                        @foreach($questions as $pQuestion)
                                                        <option value="{{$pQuestion->id}}" {{$pQuestion->id == $question->parent_id ? "selected" : ""}}>{{$pQuestion->id}} - {{$pQuestion->title}} - [{{$pQuestion->key}}]</option>
                                                         @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                    <div class="col-md-4">
                                        <fieldset class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control" name="desc" rows="3">{{$question->desc}}</textarea>
                                        </fieldset>
                                        <div class="form-group">
                                            <label>Description (باللغة العربية)</label>
                                            <textarea class="form-control" rows="3"  name="translatable[desc:ar]" dir="rtl">{{optional($question->translate('ar'))->desc}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <label for="question-price">Price (EGP)</label>
                                                    <input type="text" class="form-control" id="question-price"
                                                           name="price" value="{{$question->getRawOriginal("price")}}">
                                                </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <label for="question-sorting">Sorting No.</label>
                                                    <input type="text" class="form-control" id="question-sorting"
                                                           name="sorting"  value="{{$question->sorting}}" >
                                                    <small>Sorting is the number to sort questions</small>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-4">
                                                <fieldset class="form-group">
                                                    <label for="question-group_id">Group ID</label>
                                                    <input type="text" class="form-control" id="question-group_id"
                                                           name="group_id"  value="{{$question->group_id}}">
                                                    <small>Group id is a number for for wizard form , step number.</small>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <hr>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="question-type">Select Question Type</label>
                                            <select class="form-control" id="question-type" name="type">
                                                <option value="" {{empty($question->type) ? "selected" : ""}}>None</option>
                                                <option value="textfield" {{$question->type == "textfield" ? "selected" : ""}}>Default Input , TextField</option>
                                                <option value="radio" {{$question->type == "radio" ? "selected" : ""}}>Radio</option>
                                                <option value="checkbox" {{$question->type == "checkbox" ? "selected" : ""}}>Checkbox</option>
                                                <option value="switch" {{$question->type == "switch" ? "selected" : ""}}>Switch</option>
                                                <option value="textarea" {{$question->type == "textarea" ? "selected" : ""}}>Textarea</option>
                                                <option value="texteditor" {{$question->type == "texteditor" ? "selected" : ""}}>TextEditor</option>
                                                <option value="file" {{$question->type == "file" ? "selected" : ""}}>File</option>
                                                <option value="voice-note" {{$question->type == "voice-note" ? "selected" : ""}}>Voice Note Recorder</option>
                                                <option value="sub_questions" {{$question->type == "sub_questions" ? "selected" : ""}}>Sub Questions</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="question-sub-type">Select Question Sub Type</label>
                                            <select class="form-control select2" id="question-sub-type" name="sub_type[]" multiple="multiple">
                                                <option value="parent_key" {{ Str::contains($question->sub_type , "parent_key") ? "selected" : "" }}>Parent_Key</option>
                                                <option value="parent_id"  {{ Str::contains($question->sub_type , "parent_id") ? "selected" : "" }}>Parent_ID</option>
                                                <option value="parent_type" {{ Str::contains($question->sub_type , "parent_type") ? "selected" : "" }}>Parent_Type</option>
                                            </select>
                                            <small>Sub type is additional data to initialize form</small><br>
                                            <small>The Default name of inputs using question key and question id</small><br>
                                            <small><code>Parent_Key</code>use the parent question key</small><br>
                                            <small><code>Parent_ID</code>use the parent question id</small>
                                            <small><code>Parent_Type</code>use the parent question type</small>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="question-label">Question Label</label>
                                            <input type="text" class="form-control" id="question-label" name="label"  value="{{$question->label}}">
                                            <small>Can be used as step title for wizard form or instead of title</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="question-label-ar">Question Label (باللغة العربية)</label>
                                            <input type="text" class="form-control" id="question-label-ar" name="translatable[label:ar]" dir="rtl" value="{{optional($question->translate('ar'))->label}}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <hr>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="question-title-in-order">Title in invoice</label>
                                                    <input type="text" class="form-control" id="question-title-in-order" name="meta[title_in_order]"  value="{{$question->getMeta("title_in_order")}}">
                                                    <small>This text will be shown in invoice page with the price.</small>
                                                    <small><code>(answer_value)</code> will be replaced with the user answer/input.</small>
                                                </div>
                                                <div class="form-group">
                                                    <label for="question-title-in-order-ar">Title in invoice (باللغة العربية)</label>
                                                    <input type="text" class="form-control" id="question-title-in-order-ar" name="translatable[meta][title_in_order][ar]" dir="rtl" value="{{$question->getMetaTranslate("ar","title_in_order")}}">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <ul class="list-unstyled mb-0 mt-2">
                                                    <li class="d-inline-block  mb-1">
                                                        <fieldset>
                                                            <div class="checkbox checkbox-primary">
                                                                <input type="checkbox" id="colorCheckbox1" name="meta[hide_in_order]" {{$question->getMeta("hide_in_order",false) ? "checked" : ""}}>
                                                                <label for="colorCheckbox1">Hide from order/invoice page</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="question-cloning">Cloning other input / question</label>
                                                    <select class="form-control select2" id="question-cloning" name="meta[clone_question]">
                                                        <option value="">null</option>
                                                        @foreach($questions as $cQuestion)
                                                            <option value="{{$cQuestion->id}}"  {{$cQuestion->id == $question->getMeta("clone_question") ? "selected" : ""}}>{{$cQuestion->id}} - {{$cQuestion->title}} - [{{$cQuestion->key}}]</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="question-cloning-amount">Clone xx times on change event</label>
                                                    <input type="text" class="form-control" id="question-cloning-amount" name="meta[clone_question_amount]" value="{{$question->getMeta("clone_question_amount")}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 mb-1">
                                        <div class="row">
                                            <div class="col-md-6 mt-2">
                                                <ul class="list-unstyled mb-0 ">

                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="checkbox checkbox-primary">
                                                                <input type="checkbox" id="colorCheckbox11" name="meta[multi]" {{$question->getMeta("multi",false) ? "checked" : ""}}>
                                                                <label for="colorCheckbox11">Is Multiple Input ?</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="checkbox checkbox-primary">
                                                                <input type="checkbox" id="colorCheckbox2" name="meta[increment]" {{$question->getMeta("increment",false) ? "checked" : ""}}>
                                                                <label for="colorCheckbox2">Is Cloneable/Increment Input ?</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="question-default-value">Default value</label>
                                                    <input type="text" class="form-control" id="question-default-value" name="meta[default_value]" value="{{$question->getMeta("default_value")}}">
                                                    <small>set default value for input / question.</small>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="list-unstyled mb-0 ">

                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="checkbox checkbox-primary">
                                                        <input type="checkbox" id="colorCheckbox111" name="meta[hide_wizard_pagination]" {{$question->getMeta("hide_wizard_pagination",false) ? "checked" : ""}}>
                                                        <label for="colorCheckbox111">Hide Wizard Pagination ?</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="divider divider-dashed">
                                        <div class="divider-text">Meta - Additional Options / Settings</div>
                                    </div>
                                </div>

                                <div class="row mt-2">
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Icon</span>
                                        </div>
                                        <input type="text" class="form-control"  name="meta[icon]" value="{{$question->getMeta("icon")}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Input-Type</span>
                                        </div>
                                        <input type="text" class="form-control"  name="meta[input_type]" value="{{$question->getMeta("input_type")}}">
                                    </div>
                                </div>
                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Label Classes</span>
                                            </div>
                                            <input type="text" class="form-control"  name="meta[label_class]" value="{{$question->getMeta("label_class")}}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group mt-1">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Form Group Classes</span>
                                            </div>
                                            <input type="text" class="form-control"  name="meta[form_group_classes]" value="{{$question->getMeta("form_group_classes")}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Column/width class in tracking page</span>
                                            </div>
                                            <input type="text" class="form-control"  name="meta[tracking_class]" value="{{$question->getMeta("tracking_class")}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mt-1">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Top Html Code</span>
                                            </div>
                                            <textarea class="form-control" name="meta[top_html]" rows="4">{{$question->getMeta("top_html")}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Bottom Html Code</span>
                                            </div>
                                            <textarea class="form-control" name="meta[bottom_html]" rows="4">{{$question->getMeta("bottom_html")}}</textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label> <span class="input-group-text">Parent Classes Before from-group</span></label>
                                            <input type="text" class="form-control"  name="meta[parent_class]" value="{{$question->getMeta("parent_class")}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Element / Input Classes</span>
                                            </div>
                                            <input type="text" class="form-control"  name="meta[element_class]" value="{{$question->getMeta("element_class")}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Element / Input Attributes</span>
                                            </div>
                                            <input type="text" class="form-control"  name="meta[element_attr]" value="{{$question->getMeta("element_attr")}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Component Classes</span>
                                            </div>
                                            <input type="text" class="form-control"  name="meta[col_class]" value="{{$question->getMeta("col_class")}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Placeholder</span>
                                            </div>
                                            <input type="text" class="form-control"  name="meta[placeholder]" value="{{$question->getMeta("placeholder")}}">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="list-unstyled mb-0 ">

                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="checkbox checkbox-primary">
                                                        <input type="checkbox" id="colorCheckbox3" name="meta[hide_label]" {{$question->getMeta("hide_label",false) ? "checked" : ""}}>
                                                        <label for="colorCheckbox3">Hide Label</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="checkbox checkbox-primary">
                                                        <input type="checkbox" id="colorCheckbox38" name="meta[hide_price]" {{$question->getMeta("hide_price",false) ? "checked" : ""}}>
                                                        <label for="colorCheckbox38">Hide Price</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> <span class="input-group-text">Html Code</span></label>
                                            <textarea class="form-control" name="meta[html]" rows="4">{{$question->getMeta("html")}}</textarea>
                                            <small>use <code>{name}</code> for default input name and <code>{question_id}</code> to get question id</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="divider divider-dashed">
                                        <div class="divider-text">Form Validation</div>
                                    </div>
                                </div>
                                <div class="row mt-1">
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Custom Key</span>
                                                </div>
                                                <input type="text" class="form-control"  name="validation[key]" value="{{optional(optional($question->validation)->keys())->first() ?: ""}}">
                                            </div>
                                            <small><code>(key)</code>question key</small><br>
                                            <small><code>(type)</code>question type</small><br>
                                            <small><code>(id)</code>question id</small><br>
                                            <small><code>(parent_key)</code>parent question key</small><br>
                                            <small><code>(parent_type)</code>parent question type</small><br>
                                            <small><code>(parent_id)</code>parent question id</small>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Rules</span>
                                                </div>
                                                <input type="text" class="form-control"  name="validation[rule]" value="{{optional($question->validation)->first() ?: ""}}">
                                            </div>
                                        </div>
                                </div>
                                <div class="row mt-1 d-none">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label> <span class="input-group-text">Rules Messages</span></label>
                                            <textarea class="form-control"  rows="8"></textarea>
                                            <small>Only For Main Question</small>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit"
                                        class="btn  btn-primary waves-effect waves-light  submit-ajax-form mt-4"
                                        id="edit-question-button" data-form="#edit-question-form">
                                        <span class="spinner-border spinner-border-sm" style="display: none;"
                                              role="status"></span><span
                                        class="button-text">Update</span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

{{-- vendor scripts --}}
@section('vendor-scripts')

@endsection

@section('page-scripts')
    <script src="{{asset('assets/js/scripts/forms/select/form-select2.js')}}"></script>

@endsection
