@extends('layouts.contentLayoutMaster')
{{-- page title --}}
@section('title','Questions')
{{-- vendor style --}}

{{-- page style --}}
@section('page-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/app-invoice.css')}}">
@endsection


@section('content')
    <!-- invoice list -->
    <section class="invoice-list-wrapper ">

        <div class="invoice-create-btn mb-1">
            <a href="{{route('admin.questions.create')}}" class="btn btn-primary glow invoice-create" role="button" aria-pressed="true" >Create New Question</a >
        </div>

        @foreach($questions->where("main",1) as $mainQuestion)

            <div class="h4 d-flex mt-3">{{$mainQuestion->title}} <div class="text-left ml-2">
                    <div class="font-small-3 d-flex ">
                        @if($mainQuestion->state == "deactivated") <span class="badge badge-light-danger m-0 mr-2 " style="font-size: 10px; padding:2px;">DeActivated</span> @endif
                        <a href="{{route("admin.questions.edit",$mainQuestion->id)}}" class=" text-info mr-1">Edit</a>
                        <a href="{{route("admin.questions.duplicate",$mainQuestion->id)}}" class=" text-primary mr-1">Duplicate</a>
                        <form method="POST" action="{{route("admin.questions.destroy",$mainQuestion->id)}}" class="p-0 m-0">
                            @csrf
                            @method("DELETE")
                            <input type="submit" class=" font-small-3 border-0 text-danger bg-transparent m-0 p-0" value="Delete">
                        </form>
                    </div>
                </div>
                </div>
            <div class="table-responsive">
                <table class="table  invoice-data-table questions-data-table dt-responsive nowrap"  id="questions-{{$mainQuestion->key}}" style="width:100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th>
                            <span class="align-middle">#ID</span>
                        </th>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Key</th>
                        <th><small>S</small></th>
                        <th><small>G</small></th>
                        <th>Type</th>
                        <th><small>Parent</small></th>
                        <th class="text-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($mainQuestion->children->sortBy("group_id") as $question)
                        <x-question-table-component :question="$question"  />
                    @endforeach

                    </tbody>
                </table>
            </div>
            <hr>
            @endforeach
    </section>
@endsection


{{-- page style --}}
@push('styles')

@endpush

{{-- page scripts --}}
@push('scripts')
    <script>
        if ($(".questions-data-table").length) {
            $( ".questions-data-table" ).each(function( index ) {
            var dataListView = $( this ).DataTable({
                "iDisplayLength": 50,
                "bSort" : false,
                columnDefs: [
                    {

                    },
                ],
                //order: [2, 'asc'],
                dom:
                    '<"top d-flex flex-wrap"<"action-filters flex-grow-1"f><"actions action-btns d-flex align-items-center">><"clear">rt<"bottom"p>',
                language: {
                    search: "",
                    searchPlaceholder: "Search Question"
                },

                responsive: {
                   /* details: {
                        type: "column",
                        target: 0
                    }*/
                }
            });
            });
        }
    </script>
@endpush
