@extends('layouts.contentLayoutMaster')

{{-- title --}}
@section('title','Settings')
{{-- venodr style --}}
@section('vendor-styles')

@endsection

{{-- page style --}}
@section('page-styles')

@endsection

@section('content')
    <section id="basic-input" class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header pb-0">
                        <h4 class="card-title">General Settings</h4>
                        <hr>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <form class="ajax-form" data-alert="#settings-update-alert" enctype="multipart/form-data"
                                  data-scrolltoalert="true"
                                  data-button="#settings-update-button" id="settings-update-form" method="POST"
                                  action="{{route("admin.settings.update")}}">
                                @method("PUT")
                            <div class="row mt-1">
                                <div class="col-md-12">
                                    @component('components.alert')
                                        @slot('alert_theme')  background  @endslot
                                        @slot('alert_type')  {{session('alert_message')["type"]}} @endslot
                                        @slot('alert_content')  {!! session('alert_message')["content"] !!}  @endslot
                                        @slot('custom_class')  mb-1 {{session('alert_message') ? "" : "d-none"}} @endslot
                                        @slot('alert_id')  settings-update-alert  @endslot
                                    @endcomponent
                                </div>

                                <div class="col-md-12">
                                    <fieldset class="form-group">
                                        <label>Description <span class="text-danger">*</span></label>
                                        <textarea class="form-control texteditor-summernote" name="desc"  placeholder="website description , under logo" rows="6">{{settings("desc")}}</textarea>
                                    </fieldset>
                                </div>

                                <div class="col-md-12 mb-2 mt-2">
                                    <fieldset class="form-group text-right " style="direction: rtl !important;">
                                        <label  class="control-label font-medium-1"><span class="text-danger">*</span> الوصف باللغة العربية </label>
                                        <textarea class="form-control texteditor-summernote" name="desc_ar"  placeholder="وصف الموقع باللغة العربية" rows="6" dir="rtl">{{settings("desc_ar")}}</textarea>
                                    </fieldset>
                                </div>
                                <div class="col-md-12 ">
                                    <div class="form-group">
                                        <label class="control-label">Admin Email <span
                                                class="text-danger">*</span></label>
                                        <input type="text" class="form-control" maxlength="120" name="admin_email"
                                               value="{{settings("admin_email")}}">
                                        <small class="text-muted">Receiving admin notifications/mails on it.</small>
                                    </div>
                                </div>

                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label class="control-label">General Password <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" class="form-control" maxlength="120" name="auth_general_password" autocomplete="off"
                                                   value="{{$setting->get("auth_general_password")}}">
                                            <small class="text-muted">A general password to login to any user.</small>
                                        </div>
                                    </div>

                                <div class="col-md-12">
                                    <hr>
                                    <div  class="custom-control custom-switch custom-switch-dark custom-control-inline mb-2">
                                        <input type="checkbox" class="custom-control-input" name="test_mode_weaccept" id="test-mode-weaccept"  {{settings("test_mode_weaccept") ? "checked" : ""}}>
                                        <label class="custom-control-label mr-1" for="test-mode-weaccept">
                                        </label>
                                        <span>Test Mode (weAccept)</span>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <fieldset class="form-group">
                                        <label>Accept (paymobsolutions) Api Key</label>
                                        <textarea class="form-control" name="paymobsolutions_api_key"  rows="4">{{settings("paymobsolutions_api_key")}}</textarea>
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <fieldset class="form-group">
                                        <label>Accept (paymobsolutions) Integration ID</label>
                                        <input type="text" class="form-control" name="paymobsolutions_integration_id" value="{{settings("paymobsolutions_integration_id")}}">
                                    </fieldset>
                                </div>
                                <div class="col-md-12">
                                    <fieldset class="form-group">
                                        <label>Accept (paymobsolutions) Iframe Link</label>
                                        <textarea class="form-control" name="paymobsolutions_iframe_link"  rows="4">{{settings("paymobsolutions_iframe_link")}}</textarea>
                                    </fieldset>
                                </div>

                            </div>
                                <button type="submit"
                                        class="btn  btn-primary waves-effect waves-light  submit-ajax-form"
                                        id="settings-update-button" data-form="#settings-update-form">
                                        <span class="spinner-border spinner-border-sm" style="display: none;"
                                              role="status"></span><span
                                        class="button-text">Update Settings</span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

