@section('modal_title')
    {{$modal_title ?? "Create New Coupon" }}
@stop
@section('modal_content')
    <div class="modal-body p-1">
        <form class="ajax-form" data-alert="#coupon-create-alert" data-scrolltoalert="true"
              data-button="#coupon-create-button" id="coupon-create-form" method="POST"
              action="{{route("admin.coupons.store")}}">
            @csrf
            <div class="row">
                <div class="col-12">
                    @component('components.alert')
                        @slot('alert_theme')  background  @endslot
                        @slot('custom_class')  d-none  @endslot
                        @slot('alert_id')  coupon-create-alert  @endslot
                    @endcomponent
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Title <span
                                    class="text-danger">*</span></label>
                        <input type="text" class="form-control" maxlength="191" name="title" >
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                    <div  class="custom-control custom-switch custom-control-inline mt-2 pt-50">
                        <input type="checkbox" class="custom-control-input" name="state" id="active-coupon" checked>
                        <label class="custom-control-label mr-1" for="active-coupon">
                        </label>
                        <span>Active</span>
                    </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Coupon Code <span
                                class="text-danger">*</span></label>
                        <input type="text" class="form-control" maxlength="10" name="code" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Start Date <span
                                class="text-danger">*</span></label>
                        <input type="text" class="form-control flatpicker" name="start_date"  id="coupon-create-start-date">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-1" class="control-label">End Date <span
                                class="text-danger">*</span></label>
                        <input type="text" class="form-control flatpicker" name="end_date"  id="coupon-create-end-date">
                    </div>
                </div>
            </div>
            <div class="row mt-1 mb-1">
                <div class="col-md-7">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Discount Type</label>
                        <ul class="list-unstyled mb-0 d-inline-flex mt-1">
                            <li class="d-inline-block mr-2 mb-1">
                                <fieldset>
                                    <div class="radio radio-primary radio-glow">
                                        <input type="radio" id="radioGlow12" name="type" value="percentage">
                                        <label for="radioGlow12" class="text-muted">Percentage</label>
                                    </div>
                                </fieldset>
                            </li>
                            <li class="d-inline-block mr-2 mb-1">
                                <fieldset>
                                    <div class="radio radio-primary radio-glow">
                                        <input type="radio" id="radioGlow1" name="type" value="fixed" >
                                        <label for="radioGlow1" class="text-muted">Fixed Amount</label>
                                    </div>
                                </fieldset>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="field-3" class="control-label">Discount Amount</label>
                        <input type="text" class="form-control" maxlength="40" name="amount"  >
                        <small>amount of discount depend on type.</small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-3" class="control-label">Minimum Amount</label>
                        <input type="text" class="form-control" maxlength="40" name="minimum_amount"  >
                        <small>The minimum amount of total order to use the coupon.</small>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-3" class="control-label">Max Usage</label>
                        <input type="text" class="form-control" maxlength="40" name="max_usage"  >
                        <small>Maximum of usage for this coupon.</small>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-3" class="control-label">Select Users</label>
                        <select class="form-control select2" name="user_id[]" data-search="true" multiple="multiple"
                                id="user-dropdown" >
                            @foreach(App\Models\User\User::all() as $user)
                                <option value="{{$user->id}}">{{$user->full_name}} - {{$user->email}}</option>
                            @endforeach
                        </select>
                        <small>Allow specific users to use this discount.</small>
                    </div>
                </div>
            </div>

        </form>
    </div>
    <div class="modal-footer">
            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info waves-effect waves-light submit-ajax-form"
                    id="coupon-create-button" data-form="#coupon-create-form">
                <span class="spinner-border spinner-border-sm" style="display: none;" role="status"></span><span
                        class="button-text">Create</span>
            </button>
    </div>
        <script>
            $( document ).ready(function() {
                if ($('.select2').length) {
                    $("#user-dropdown").select2({
                        dropdownAutoWidth: true,
                        width: '100%'
                    });
                }
            });
        </script>

@stop
