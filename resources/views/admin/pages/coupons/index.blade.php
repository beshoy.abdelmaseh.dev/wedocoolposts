@extends('layouts.contentLayoutMaster')

@section('title',"Coupons")


@section('content')
    @include("components.remote-modal-content")
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if (session('alert_message'))
                        @component('components.alert')
                            @slot('alert_theme')  background  @endslot
                            @slot('alert_content')  {{session('alert_message')["content"]}}  @endslot
                            @slot('alert_type')  {{session('alert_message')["type"]}}  @endslot
                        @endcomponent
                    @endif
                    <button class="btn btn-primary waves-effect waves-light toggle-remote-modal"
                          data-url="{{route("admin.coupons.create")}}" data-target="#coupon-create-modal"><i
                                class="mdi mdi-plus-circle mr-1"></i>Create New Coupon</button>

                    <h3 class="mt-3">Coupons</h3>
                    <hr>
                    <table id="coupons-table" data-csrf_token="{{ csrf_token() }}"
                           class="dataTable table table-striped dt-responsive table-centered  nowrap"
                           data-url="{{ route('admin.coupons.data-table') }}">
                        <thead>
                        <tr>
                            <th class="text-center pr-2">Code</th>
                            <th class="text-left pr-2">Title</th>
                            <th class="text-center pr-2">State</th>
                            <th class="text-center">Discount Amount</th>
                            <th class="text-center">Start - End Dates</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>

                        <tbody></tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

@endsection

@push('scripts')
    <script>
        $("#coupons-table").data("columns",@json($dataTable_columns));
        $(document).ready(function () {
            initializeTable();
        });
        $( document ).ready(function() {
        });
    </script>
@endpush
