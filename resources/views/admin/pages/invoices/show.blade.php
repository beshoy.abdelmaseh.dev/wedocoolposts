@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title','Invoice')
{{-- page scripts --}}
@section('page-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/app-invoice.css')}}">
@endsection

@section('content')
    <!-- login page start -->
    <section class="invoice-view-wrapper page-content"   >

        <div class="row ">
            <div class="col-12">
                <div class="row">
                    <div class="col-12 mb-1 d-flex justify-content-between">
                        <div>
                            <h3 class="text-muted">Invoice: #{{$invoice->slug}}</h3>
                        </div>
                        <div>
                       <a href="{{route("admin.orders.manage",$invoice->slug)}}" class="btn btn-primary px-3">Manage Order</a>
                        </div>
                    </div>
            <!-- invoice view page -->
            <div class="col-xl-12 col-md-12 col-12">

                <div class="card invoice-print-area">
                    <div class="card-content">
                        <div class="card-body pb-0 mx-25">
                            <!-- header section -->
                            <div class="row">
                                <div class="col-xl-4 col-md-12 font-medium-2">
                                    <span class="invoice-number ">Order#</span>
                                    <span class="font-weight-bold">{{$invoice->slug}}</span>
                                </div>
                                <div class="col-xl-8 col-md-12 ">
                                    <div class="d-flex align-items-center justify-content-xl-end flex-wrap">
                                        <div class="mr-0">
                                            <span class="text-muted">Date Issue:</span>
                                            <span class="font-weight-bold">{{$invoice->created_at->format("d F Y")}}.</span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- invoice address and contact -->

                            <hr>
                            <div class="row invoice-info mb-2">
                                <div class="col-md-6 mt-1 ">
                                    <div class="shadow rounded py-1 px-1 mb-0">
                                    <h6 class="invoice-from">User Info</h6>
                                    <div class="mb-1">
                                        <span>Full Name : {{optional($order->user)->full_name}}</span>
                                    </div>
                                    <div class="mb-1">
                                        <span>Phone : {{optional($order->user)->phone}}</span>
                                    </div>
                                    <div class="mb-1">
                                        <span>Email Address : {{optional($order->user)->email}}</span>
                                    </div>
                                        <div class="">
                                            <span>Business Type: {{optional(optional($order->user)->businessType())->name}}</span>
                                        </div>
                                    </div>
                                </div>
                                @if($invoice->transaction)
                                <div class="col-md-6  mt-1 ">
                                    <div class="shadow rounded py-1 px-1 mb-0">
                                    <h6 class="invoice-to">Last Transaction Details</h6>
                                        <div class="mb-1">
                                            <span>Amount : {{(optional($invoice->transaction)->amount)}} EGP</span>
                                        </div>
                                    <div class="mb-1">
                                        <span>Transaction ID : {{optional($invoice->transaction)->getMeta("trans_id")}}</span> ,    <span>Order ID : {{optional($invoice->transaction)->getMeta("order_id")}}</span>
                                    </div>
                                    <div class="mb-1">
                                        <span>Status : {{ucfirst(optional($invoice->transaction)->state)}} - {{ucfirst(optional($invoice->transaction)->payment_status)}}</span>
                                    </div>
                                    <div class="">
                                        <span>Success : {{optional($invoice->transaction)->getMeta("success") ? "true" : "false"}}</span> ,   <span>Response : {{optional($invoice->transaction)->getMeta("process_data_message")}}</span>
                                    </div>
                                    </div>
                                </div>
                                @endif
                            </div>

                            <hr>
                            <!-- logo and title -->
                            <div class="row my-1">
                                <div class="col-6  d-none">
                                    <h4 class="text-primary">Order Details</h4>
                                </div>
                                <div class="col-12 d-flex justify-content-between">
                                    <div class="mt-1 ml-2 font-medium-4"> <i class="livicon-evo" data-options="name: {{$order->details->firstWhere("answer_type","main_question")->question->getMeta("icon")}}; size: 50px; strokeColorAlt: #FDAC41; strokeColor: #ff6b09; style: lines-alt; eventOn: .kb-hover-1;"></i><span class="ml-1 font-weight-bold">{{$order->details->firstWhere("answer_type","main_question")->question->title}}</span></div>
                                   <div class="text-right">
                                       @if($invoice->state == "full-paid")
                                           <div class="badge badge-pill badge-glow badge-success m-1 py-1 px-3 font-medium-4">FULL PAID</div>
                                       @elseif($invoice->state == "half-paid")
                                           <div class="badge badge-pill badge-glow badge-primary m-1 py-1 px-3 font-medium-4">50% PAID</div>
                                       @else
                                           <div class="badge badge-pill badge-glow badge-danger m-1 py-1 px-3 font-medium-4">UNPAID</div><br>

                                       @endif
                                   </div>
                                </div>
                            </div>

                            <h4 class="text-primary">Order Details</h4>

                            <hr>
                        </div>
                        <!-- product details table-->
                        <div class="invoice-product-details  mx-md-25">
                            <table class="table table-borderless mb-0">
                                <thead>
                                <tr class="border-0">
                                    <th scope="col" class="col-md-12  text-nowrap w-100"></th>
                                    <th scope="col"></th>
                                    <th scope="col" class="text-right">Price</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                            @php($loadedQuestions = $order->loadQuestions(optional($order->details)->pluck("answer_value")))
                            @foreach($order->details as $details)
                                @if(!($details->question->getMeta("hide_in_order",false)))
                                    @php($title = $details->title_in_order)
                                    <div class="row px-1 mb-1">
                                        <div class="col-sm-12 col-md-5">
                                            @if($title)
                                                {!! $title !!}
                                            @else
                                                {{$details->question->title}}
                                            @endif
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            @if(empty($title))
                                                {!! $details->getAnswerValue($loadedQuestions) !!}
                                            @endif
                                        </div>
                                        <div class="col-sm-12 col-md-2 text-right text-muted  text-nowrap">{{$details->price }}</div>
                                        <div class="col-sm-12 d-md-none "><hr></div>
                                    </div>
                                @endif

                            @endforeach

                        </div>

                        <!-- invoice subtotal -->
                        <div class="card-body pt-0 mx-25">
                            <hr>
                            <div class="row">
                                <div class=" col-sm-12 col-md-6 mt-75">
                                    <p></p>
                                </div>
                                <div class="col-8 col-sm-6 d-flex justify-content-end mt-75 mr-0 pr-0">
                                    <div class="invoice-subtotal">
                                        @if($invoice->discount)
                                            <div class="invoice-calc d-flex justify-content-between mb-50">
                                                <span class="invoice-title ">Sub Total:</span>
                                                <span class="invoice-value  text-primary">{{$invoice->getRawOriginal("amount")}} EGP</span>
                                            </div>
                                            <div class="invoice-calc d-flex justify-content-between">
                                                <span class="invoice-title">Discount:</span>
                                                <span class="invoice-value  text-primary">- {{$invoice->discount}} EGP</span>

                                            </div>
                                            @if($invoice->coupon)
                                                <div class="font-small-2 pt-50 px-50 m-0"><div><span>A Coupon [ <mark class="font-small-2" style="background: rgba(239,239,239,0.82);">{{$invoice->getMeta("coupon_code","NULL")}}</mark> ] has been applied.</span></div>
                                                </div>
                                            @endif
                                            <hr>
                                        @endif
                                        <div class="invoice-calc d-flex justify-content-between">
                                            <span class="invoice-title font-weight-bold text-uppercase">Amount Due:</span>
                                            <span class="invoice-value text-primary font-weight-bold font-medium-1">{{$invoice->amount}} EGP</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                </div>
        </div>
        </div>
    </section>
    <!-- login page ends -->
@endsection
@section('vendor-scripts')
    <script src="{{asset('assets/vendors/js/extensions/jquery.steps.min.js')}}"></script>
    <script src="{{asset('assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
@endsection
@section('page-scripts')
    <script src="{{asset('assets/js/scripts/forms/wizard-steps.js')}}"></script>
@endsection
