@extends('layouts.contentLayoutMaster')

{{-- title --}}
@section('title','Statistics & Analytics')


@section('vendor-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/charts/apexcharts.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/vendors/css/extensions/swiper.min.css')}}">
@endsection
@section('page-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/dashboard-ecommerce.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/app-invoice.css')}}">
@endsection
@section('content')
    @include("components.remote-modal-content")
    <!-- Dashboard Analytics Start -->
    <section id="dashboard-analytics" class="invoice-list-wrapper ">
        <div class="row">
            <div class="col-sm-2  ">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body py-1">
                            <div class="badge-circle badge-circle-lg badge-circle-light-danger mx-auto mb-50">
                                <i class="bx bx-user font-medium-5"></i>
                            </div>
                            <div class="text-muted line-ellipsis">Total Users</div>
                            <h3 class="mb-0">{{$statistics["total_users"]}}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 ">
                <div class="card text-center">
                    <div class="card-content">
                        <div class="card-body py-1">
                            <div class="badge-circle badge-circle-lg badge-circle-light-primary mx-auto mb-50">
                                <i class="bx bx-shopping-bag font-medium-5"></i>
                            </div>
                            <div class="text-muted line-ellipsis">Total Orders</div>
                            <h3 class="mb-0">{{$statistics["total_orders"]}}</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8  ">
                <div class="card">
                    <div class="card-body py-1">
                <div class="pt-1">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 mb-2">
                            <div class="d-flex flex-column">
                                <span>UnPaid - Orders</span>
                                <div class="d-flex align-items-center">
                                    <div class="badge-circle badge-circle-lg badge-circle-light-secondary flex-column mr-1">
                                        <i class='bx bx-x-circle text-danger font-medium-4'></i>
                                    </div>
                                    <h4 class="mb-0">{{$statistics["unpaid_orders"]}}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6  mb-2">
                            <div class="d-flex flex-column">
                                <span>Pending Orders</span>
                                <div class="d-flex align-items-center">
                                    <div class="badge-circle badge-circle-lg badge-circle-light-secondary flex-column mr-1">
                                        <i class='bx bx-time-five text-info font-medium-4'></i>
                                    </div>
                                    <h4 class="mb-0">{{$statistics["pending_orders"]}}</h4>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 mb-2">
                            <div class="d-flex flex-column">
                                <span>Full Paid - Orders</span>
                                <div class="d-flex align-items-center">
                                    <div class="badge-circle badge-circle-lg badge-circle-light-secondary flex-column mr-1">
                                        <i class='bx bx-dollar-circle text-success font-medium-4'></i>
                                    </div>
                                    <h4 class="mb-0">{{$statistics["full_paid_orders"]}}</h4>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3 col-sm-6  mb-2">
                            <div class="d-flex flex-column">
                                <span>Approved - Orders</span>
                                <div class="d-flex align-items-center">
                                    <div class="badge-circle badge-circle-lg badge-circle-light-secondary flex-column mr-1">
                                        <i class='bx bx-check-circle text-success font-medium-4'></i>
                                    </div>
                                    <h4 class="mb-0">{{$statistics["approved_orders"]}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>


            <div>

            </div>

        </div>
        <div class="h4">New Orders </div>
        <div class="table-responsive">
            <table class="table invoice-data-table dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                    <th>
                        <span class="align-middle">Order#</span>
                    </th>
                    <th>Type</th>
                    <th>User</th>
                    <th>ORDER STATUS</th>
                    <th class="font-small-2 text-center">PAYMENT STATUS</th>
                    <th class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>
                            <a href="{{route('admin.invoices.show',$order->invoice->id)}}">{{$order->invoice->slug}}</a>
                        </td>
                        <td><span class="invoice-amount font-weight-bold">{{$order->main_question}}</span></td>
                        <td><span class="invoice-amount">{{optional($order->user)->full_name}}</span></td>
                        <td><div class="{{optional($order->action->status)->class}} font-small-2">{{ucwords(optional($order->action->status)->name)}}</div></td>
                        <td class="text-center">
                            @if($order->invoice->state == "full-paid")
                                <div class="badge badge-light-success badge-pill">Full PAID</div>
                            @elseif($order->invoice->state == "half-paid")
                                <div class="badge badge-light-primary badge-pill">50% PAID</div>
                            @else
                                <div class="badge badge-light-danger badge-pill">UNPAID</div><br>
                            @endif
                        </td>
                        <td class="text-center">
                            <div class="invoice-action">
                                <a href="{{route('admin.orders.manage',$order->invoice->slug)}}" class="btn btn-primary text-white py-50">
                                    <i class="bx bx-target-lock"></i> <span class="align-center ml-25">Manage</span>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

        <hr>
        <div class="h4">Last Transactions </div>
        <div class="table-responsive">
            <table class="table invoice-data-table dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>

                    <th>
                        <span class="align-middle">Order#</span>
                    </th>
                    <th>Trans ID</th>
                    <th>Ref ID</th>
                    <th>Amount</th>
                    <th>Status - Response</th>
                    <th class="font-small-1 text-center">Success</th>
                    <th class="text-center"></th>
                </tr>
                </thead>
                <tbody>
                @foreach($transactions as $transaction)
                    <tr>
                        <td>
                            <a href="{{$transaction->invoice->order ? route('admin.invoices.show',$transaction->invoice->id) : 'javascript:void(0);'}}" @if(is_null($transaction->invoice->order))  class="text-light-secondary" style="text-decoration:line-through !important;cursor: default;" @endif>{{$transaction->invoice->slug}}</a>
                        </td>
                        <td><span class="">{{$transaction->getMeta("trans_id")}}</span></td>
                        <td><span class="">{{$transaction->getMeta("order_id")}}</span></td>
                        <td><span class="invoice-amount">{{$transaction->amount}} EGP</span></td>
                        <td class="text-muted"> {{ucfirst($transaction->state)}} - {{ucfirst($transaction->payment_status)}} - {{$transaction->getMeta("process_data_message")}}</td>
                        <td class="text-center">{!!  $transaction->getMeta("success") ? "<i class='bx bx-check-circle text-success'></i>" : "<i class='bx bx-x-circle text-danger'></i>"!!}</td>
                        <td class="text-center">
                            <div class="invoice-action">
                                <button  class="border-0 bg-transparent text-white toggle-remote-modal text-info" data-modal_add_class="modal-lg "  data-toggle="tooltip"  data-original-title="More Details" data-placement="top"
                                         data-url="{{route("admin.transactions.details",$transaction->id)}}" data-target="#transaction-details-modal">
                                    <i class="bx bx-show-alt font-medium-3"></i>
                                </button>
                            </div>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

    </section>
    <!-- Dashboard Analytics end -->
@endsection


{{-- page scripts --}}
@push('scripts')
    <script>

        var dataListView = $( ".invoice-data-table" ).DataTable({
            "iDisplayLength": 10,
            "bSort" : false,
            columnDefs: [
                {

                },
            ],
            //order: [2, 'asc'],
            dom:
                '<"top d-flex flex-wrap"<"action-filters flex-grow-1"f><"actions action-btns d-flex align-items-center">><"clear">rt<"bottom"p>',
            language: {
                search: "",
                searchPlaceholder: "Search Transaction"
            },

            responsive: {
                /* details: {
                     type: "column",
                     target: 0
                 }*/
            }
        });
    </script>
@endpush
