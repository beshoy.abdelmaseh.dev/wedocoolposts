@section('modal_title')
    {{$modal_title ?? "More Details" }}
@stop
@section('modal_content')
    <div class="modal-body p-1">
            <div class="row">
                <div class="col-md-12">
                    <label>processCallback</label>
                    @php(dump($transaction->getMeta("processCallback",null,true)))
                </div>
                <div class="col-md-12">
                    <label>responseCallback</label>
                    @php(dump($transaction->getMeta("responseCallback",null,true)))
                </div>
                <div class="col-md-12">
                    <label>all data</label>
                   @php(dump($transaction->toArray()))
                </div>
            </div>
    </div>
    <div class="modal-footer">
            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
    </div>
    <script>
        var compacted = document.querySelectorAll('.sf-dump-compact');

        for (var i = 0; i < compacted.length; i++) {
            compacted[i].className = 'sf-dump-expanded';
        }
    </script>
@stop
