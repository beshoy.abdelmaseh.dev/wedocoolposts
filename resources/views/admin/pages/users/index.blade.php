@extends('layouts.contentLayoutMaster')
{{-- page title --}}
@section('title','Users')
{{-- vendor style --}}

{{-- page style --}}
@section('page-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/app-invoice.css')}}">
@endsection

@section('content')
    <!-- invoice list -->
    <section class="invoice-list-wrapper ">
        <div class="table-responsive">
            <table class="table  invoice-data-table users-data-table dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                    <th></th>
                    <th>
                        <span class="align-middle">#ID</span>
                    </th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Business Type</th>
                    <th>Total Orders</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr>
                    <td></td>
                    <td>{{$user->id}}</td>
                    <td>{{$user->full_name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>
                    <td class="text-center">{{optional($user->businessType())->name}}</td>
                    <td class="text-center">{{$user->orders_count}}</td>
                    <td class="text-center">
                        <div class="d-inline-flex">
                            <div>
                        <a href="{{route("admin.users.edit",$user->id)}}" data-toggle="tooltip"  data-original-title="Edit" data-placement="top"><i class="bx bx-edit-alt mr-1"></i></a>
                            </div>
                            <div>
                        <form method="POST" action="{{route("admin.users.destroy",$user->id)}}" class="p-0 m-0">
                            @csrf
                            @method("DELETE")
                            <button type="submit" class=" font-small-3 border-0 text-danger bg-transparent m-0 p-0"  data-toggle="tooltip"  data-original-title="Delete" data-placement="top"><i class="bx bx-trash-alt"></i></button>
                        </form>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </section>
@endsection


{{-- page scripts --}}
@push('scripts')
    <script>
        if ($(".users-data-table").length) {
                var dataListView = $(".users-data-table").DataTable({
                    "iDisplayLength": 50,

                    columnDefs: [
                        {

                        },
                    ],
                    //order: [2, 'asc'],
                    dom:
                        '<"top d-flex flex-wrap"<"action-filters flex-grow-1"f><"actions action-btns d-flex align-items-center">><"clear">rt<"bottom"p>',
                    language: {
                        search: "",
                        searchPlaceholder: "Search User"
                    },

                    responsive: {
                        /* details: {
                             type: "column",
                             target: 0
                         }*/
                    }
                });
            }

    </script>
@endpush
