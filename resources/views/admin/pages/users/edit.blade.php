@extends('layouts.contentLayoutMaster')

{{-- title --}}
@section('title','Edit User')
{{-- venodr style --}}
@section('vendor-styles')

@endsection

{{-- page style --}}
@section('page-styles')

@endsection

@section('content')
    <section id="basic-input" class="page-content">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header pb-0">
                        <h4 class="card-title">Edit User : {{$user->full_name}}</h4>
                        <hr>
                    </div>
                    <div class="card-body">
                        <form class="ajax-form" data-alert="#user-update-alert" enctype="multipart/form-data"
                              data-scrolltoalert="true"
                              data-button="#user-update-button" id="user-update-form" method="POST"
                              action="{{route("admin.users.update",$user->id)}}">
                            @method("PUT")
                            <div class="row">
                                <div class="col-md-12">
                                    @component('components.alert')
                                        @slot('alert_theme')  background  @endslot
                                        @slot('alert_type')  {{session('alert_message')["type"]}} @endslot
                                        @slot('alert_content')  {!! session('alert_message')["content"] !!}  @endslot
                                        @slot('custom_class')
                                            mb-1 {{session('alert_message') ? "" : "d-none"}} @endslot
                                        @slot('alert_id')  user-update-alert  @endslot
                                    @endcomponent
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <label>Full Name</label>
                                            <input type="text" class="form-control" name="full_name"
                                                   value="{{$user->full_name}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <label>Phone</label>
                                            <input type="text" class="form-control" value="{{$user->phone}}" name="phone">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <label>E-mail</label>
                                            <input type="text" class="form-control" value="{{$user->email}}" name="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 d-none">
                                    <div class="alert bg-rgba-warning alert-dismissible mb-2"
                                         role="alert">
                                        <button type="button" class="close" data-dismiss="alert"
                                                aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <p class="mb-0">
                                            Your email is not confirmed. Please check your inbox.
                                        </p>
                                        <a href="javascript: void(0);">Resend confirmation</a>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Business Type</label>
                                        @include("partials.types.dropdown",["type" => "Business-Types" , "type_id" => [$user->getMeta("type_id")]])
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="row mt-1">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Facebook Page <small>(Optional)</small></label>
                                        <input type="text" class="form-control"  name="links[facebook]" placeholder="Add link" value="{{$user->getMeta("facebook")}}">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Instagram <small>(Optional)</small></label>
                                        <input type="text" class="form-control" name="links[instagram]" placeholder="Add link" value="{{$user->getMeta("instagram")}}">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row mt-1">
                                <div class="col-12">
                                    <div class="form-group">
                                        <div class="controls">
                                            <label>Update Password <small>(Optional)</small></label>
                                            <input type="password" class="form-control" value="" placeholder="Set Password" name="password">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <button type="submit"
                                    class="btn  btn-primary waves-effect waves-light  submit-ajax-form mt-1"
                                    id="user-update-button" data-form="#user-update-form">
                                        <span class="spinner-border spinner-border-sm" style="display: none;"
                                              role="status"></span><span
                                    class="button-text">Update</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

