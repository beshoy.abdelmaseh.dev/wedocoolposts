@extends('layouts.contentLayoutMaster')

@section('title',"Order Status")


@section('content')
    @include("components.remote-modal-content")
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if (session('alert_message'))
                        @component('components.alert')
                            @slot('alert_theme')  background  @endslot
                            @slot('alert_content')  {{session('alert_message')["content"]}}  @endslot
                            @slot('alert_type')  {{session('alert_message')["type"]}}  @endslot
                        @endcomponent
                    @endif
                    <button class="btn btn-primary waves-effect waves-light toggle-remote-modal"
                          data-url="{{route("admin.types.create",["page_type" => "order-status"])}}" data-target="#type-create-modal"><i
                                class="mdi mdi-plus-circle mr-1"></i>Create New Order Status</button>

                    <h3 class="mt-3">Order Status</h3>
                    <hr>
                    <table id="order-status-types-table" data-csrf_token="{{ csrf_token() }}"
                           class="dataTable table table-striped dt-responsive table-centered  nowrap"
                           data-url="{{ route('admin.types.data-table',"order-status") }}">
                        <thead>
                        <tr>
                            <th class="text-center pr-2">ID</th>
                            <th class="text-center pr-2">Name</th>
                            <th class="text-center pr-2">Key</th>
                            <th class="text-center">Description</th>
                            <th class="text-center">Actions</th>
                        </tr>
                        </thead>

                        <tbody></tbody>
                    </table>

                </div> <!-- end card body-->
            </div> <!-- end card -->
        </div><!-- end col-->
    </div>
    <!-- end row-->

@endsection

@push('scripts')
    <script>
        $("#order-status-types-table").data("columns",@json($dataTable_columns));
        $(document).ready(function () {
            initializeTable();
        });
    </script>
@endpush
