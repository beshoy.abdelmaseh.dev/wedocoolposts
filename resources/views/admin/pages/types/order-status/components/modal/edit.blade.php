@section('modal_title')
    {{$modal_title ?? "Edit Order Status" }}
@stop
@section('modal_content')
    <div class="modal-body p-3">
        <form class="ajax-form" data-alert="#type-edit-alert" data-scrolltoalert="true"
              data-button="#type-edit-button" id="type-edit-form" method="POST"
              action="{{route("admin.types.update",$type->id)}}">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-12">
                    @component('components.alert')
                        @slot('alert_theme')  background  @endslot
                        @slot('custom_class')  d-none  @endslot
                        @slot('alert_id')  type-edit-alert  @endslot
                    @endcomponent
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Name <span
                                    class="text-danger">*</span></label>
                        <input type="text" class="form-control" maxlength="40" name="name" value="{{$type->name}}"
                               data-additional="generate-slug" data-field="#type-edit-key">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group text-right">
                        <label for="field-1" class="control-label"><span class="text-danger">*</span> الاسم باللغة العربية </label>
                        <input type="text" class="form-control text-right" maxlength="40" name="translatable[name]"  dir="rtl" value="{{optional($type->translatable)->name}}">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Key <span
                                class="text-danger">*</span></label>
                        <input type="text" class="form-control" maxlength="40" name="key" value="{{$type->key}}"  id="type-create-key">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Class</label>
                        <input type="text" class="form-control" maxlength="40" name="class"  value="{{$type->class}}">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label  class="control-label">Description</label>
                        <textarea type="text" class="form-control " name="desc" style="resize: none;"
                                  maxlength="250">{{$type->desc}}</textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group text-right">
                        <label  class="control-label "><span class="text-danger">*</span> الوصف باللغة العربية </label>
                        <textarea type="text" class="form-control text-right" name="translatable[desc]" style="resize: none;" dir="rtl"
                                  maxlength="250">{{optional($type->translatable)->desc}}</textarea>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-info waves-effect waves-light submit-ajax-form" id="type-edit-button"
                data-form="#type-edit-form">
            <span class="spinner-border spinner-border-sm" style="display: none;" role="status"></span><span
                    class="button-text">Save</span>
        </button>
    </div>
@stop
