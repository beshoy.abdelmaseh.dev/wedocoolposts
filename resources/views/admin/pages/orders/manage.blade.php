@extends('layouts.contentLayoutMaster')
{{-- title --}}
@section('title','Manage Order')
{{-- page scripts --}}

@section('content')
    @include("components.remote-modal-content")
    <!-- login page start -->
    <section class="invoice-view-wrapper page-content"   >
        <div class="row ">
            <div class="col-12 ">
                <div class="row">
                    <div class="col-12">
                        <div class="d-flex justify-content-between">
                        <div>
                            <h3 class="text-muted">Manage Order: #{{$invoice->slug}}</h3>
                        </div>
                        <div>
                            <a href="{{route("admin.invoices.show",$invoice->id)}}" class="btn btn-primary px-3">Invoice Details</a>
                        </div>
                        </div>
                        <hr>
                    </div>
                    <div class=" col-12 col-xl-3 col-md-6 col-lg-4 py-sm-1">
                        <div class="card shadow-none border">
                            <div class="card-body pb-0">
                                <div class="d-flex justify-content-between mb-1 ">
                                    <div class="text-uppercase font-medium-2">Order Status</div>
                                    <div class=""><div class="{{$order->action->status->class}} font-medium-1">{{ucwords($order->action->status->name)}}</div></div>
                                </div>
                                <div>
                                    <div class="d-flex justify-content-between ">
                                        <div class="col-md-8">
                                            <div class="form-group mb-0">
                                                <form class="ajax-form mb-0" data-alert="#order-action-alert" data-scrolltoalert="true"
                                                      data-button="#order-action-button" id="order-action-form" method="POST"
                                                      action="{{route("admin.actions.update_status",$order->action->id)}}">
                                                    @method("PUT")
                                                    @include("partials.types.dropdown",["type" => "Order-Status" , "type_id" => [$order->action->state_id]])
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group mb-0">
                                                <button type="button" class="btn btn-primary waves-effect waves-light submit-ajax-form mr-1"
                                                        id="order-action-button" data-form="#order-action-form">
                                                    <span class="spinner-border spinner-border-sm" style="display: none;" role="status"></span><span
                                                        class="button-text"> Update</span>

                                                </button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <hr>
                                <div class="d-flex justify-content-between mb-1">
                                    <div class="text-uppercase font-medium-2">Payment Status</div>
                                    <div class="">
                                        @if($invoice->state == "full-paid")
                                            <div class="badge badge-pill badge-success font-medium-1">FULL PAID</div>
                                        @elseif($invoice->state == "half-paid")
                                            <div class="badge badge-pill badge-primary font-medium-1">50% PAID</div>
                                        @else
                                            <div class="badge badge-pill badge-danger  font-medium-1">UNPAID</div>
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <div class="d-flex justify-content-between mb-1">
                                    <div class="text-uppercase font-medium-2">Order Date</div>
                                    <div class="">
                                      <div class=" font-medium-1">{{$order->created_at->format("d M Y, h:i A")}}</div>

                                    </div>
                                </div>
                                <hr>


                                    <div class="mb-2">
                                        @if($order->action->approval == "approved")
                                        <div class="alert border-success p-1 mb-0">
                                            <div class="d-flex align-items-center">
                                                <i class="bx bxs-check-circle"></i><span>Post Has Been Approved</span>
                                            </div>
                                            </div>
                                            @endif
                                    </div>


                            </div>
                        </div>
                        @if($order->user)
                            <div class="card shadow-none border">
                                <div class="card-body pb-0">
                                    <div class="d-flex justify-content-between mb-1 ">
                                        <div class="font-medium-1">Full Name</div>
                                        <div class="">{{$order->user->full_name}}</div>
                                    </div>
                                    <hr>
                                    <div class="d-flex justify-content-between mb-1 ">
                                        <div class="font-medium-1">Email</div>
                                        <div class="">{{$order->user->email}}</div>
                                    </div>
                                    <hr>
                                    <div class="d-flex justify-content-between mb-1 ">
                                        <div class="font-medium-1">Phone</div>
                                        <div class="">{{$order->user->phone}}</div>
                                    </div>
                                    <hr>
                                    <div class="d-flex justify-content-between mb-1 ">
                                        <div class="font-medium-1">Business Type</div>
                                        <div class="">{{optional($order->user->businessType())->name}}</div>
                                    </div>
                                </div>
                            </div>
                            @endif
                    </div>


            <div class="col-12 col-xl-9 col-md-12 col-lg-8 p-0 py-sm-1 ">
                @if($order->action->approval == "declined")
                <div class="alert alert-danger  mb-1 py-1" role="alert">
                    <div class="d-flex align-items-center">
                        <i class="bx bxs-x-circle"></i>
                        <p class="m-0 p-0">
                        <span class="font-weight-bold">Poopy</span><br>
                             <span class="font-small-3">{{$order->action->data->first()}}</span>
                        </p>
                    </div>
                </div>
                @endif

                            @component('components.alert')
                                @slot('alert_theme')  background  @endslot
                                @slot('alert_type')  {{session('alert_message')["type"]}} @endslot
                                @slot('alert_content')  {!! session('alert_message')["content"] !!}  @endslot
                                @slot('custom_class')  mb-1 {{session('alert_message') ? "" : "d-none"}} @endslot
                                @slot('alert_id')  order-action-alert  @endslot
                            @endcomponent

                    <div class="card shadow rounded-lg mb-1">
                        <div class="card-body py-1">
                            <div class="col-12  text-left">
                                <div class="font-medium-3"> <i class="livicon-evo" data-options="name: {{$order->details->firstWhere("answer_type","main_question")->question->getMeta("icon")}}; size: 40px; strokeColorAlt: #FDAC41; strokeColor: #ff6b09; style: lines-alt; eventOn: .kb-hover-1;"></i><span class="ml-1 font-weight-bold">{{$order->details->firstWhere("answer_type","main_question")->question->title}}</span></div>
                            </div>
                        </div>
                    </div>

                    <div class="row ">
                      @php($loadedQuestions = $order->loadQuestions(optional($order->details)->pluck("answer_value")))
                    @foreach($order->details as $details)
                        @if(!($details->question->main) && !in_array($details->answer_type,["file","voice-note"]))
                            @if(optional($details->answer_value)->count() >= 1)
                                 @for($i = 0; $i < $details->answer_value->count(); $i++)
                                        @php($title = $details->question->getMeta("title_in_order"))
                                        <div class="{{$details->question->getMeta("tracking_class","col-sm-4")}} m-0 ">
                                            <div class="card shadow rounded-lg my-25">
                                                <div class="card-content">
                                                    <div class="card-body p-1 ">
                                                        <div class="d-flex justify-content-between">
                                                        <div class="font-weight-bold">
                                                                {!! $title ?: $details->question->title !!}  @if($i > 0) - {{($i+1)}} @endif
                                                        </div>
                                                            <button  class="btn btn-sm btn-primary toggle-remote-modal"  id="manage-box"  data-modal_add_class="modal-lg" data-url="{{route("admin.actions.manage",[$details->id,$i])}}" data-target="#manage-box-modal">
                                                                <span  class="button-text">Manage</span>
                                                            </button>
                                                        </div>
                                                        <hr>
                                                            <div class="@if(!in_array($details->answer_type,["texteditor"])) text-message @endif">{!! nl2br(str_contains($details->answer_value[$i] ?: "-","question_id") ? $details->getAnswerValue($loadedQuestions) : $details->answer_value[$i]  ?: "-") !!}
                                                            </div>
                                                        @if($voiceNotes = optional(optional($order->details->where("answer_key",$details->answer_key)->where("answer_type","voice-note")->first())->answer_value)->get($i))
                                                            <div class="divider divider-center">
                                                                <div class="divider-text">Voice Notes</div>
                                                            </div>
                                                            <div class="row p-0 px-1 justify-content-md-start voice-preview">
                                                                @foreach($voiceNotes as $voiceNote)
                                                                    <div class="mb-25 col-sm-3 col-md-4">
                                                                        <audio src="{{route("uploads.show",$voiceNote)}}" controls="" style="width:100%"></audio>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        @endif
                                                       @if($materials = optional(optional($order->details->where("answer_key",$details->answer_key)->where("answer_type","file")->first())->answer_value)->get($i))
                                                        <div class="divider divider-center">
                                                            <div class="divider-text">Materials</div>
                                                        </div>
                                                            <div class="row p-0 px-1 justify-content-md-start material-preview">
                                                                @foreach($materials as $material)
                                                                    @php($file = explode(".",$material))
                                                                    @php($extension = end($file) )
                                                                    <div class="mb-50 main-review">
                                                                          <div class="position-relative ">
                                                                            <a href="{{route("uploads.show",$material)}}" {{($extension == "pdf" ? 'target=_blank' :'class=image-preview' )}} >
                                                                         <img  src="{{$extension == "pdf" ? asset("assets/images/icon/pdf-2.png") : route("uploads.show",$material)}}" class="img img-thumbnail rounded-lg mr-1"   {{($extension == "pdf" ? 'width=60' :  'width=100' )}} />
                                                                               </a>
                                                                                </div>
                                                                              </div>
                                                                @endforeach
                                                            </div>
                                                           @endif
                                                    </div>
                                                    @if($actions = $details->actions)
                                                        <x-show-actions-component :order="$order" :details="$details" :actions="$actions" :offset="$i"  />
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                  @endfor

                                @endif
                        @endif
                    @endforeach
                    </div>


                <div class="card shadow-lg rounded-lg mt-1">
                    <div class="card-content p-2">
                    <div class="card-title border-bottom">
                        <h4 class="mb-0">Comments</h4>
                        <small class="text-muted text-lowercase">Write notes and comments</small>
                    </div>
                    <div class="card-body p-0">
                        @comments(['model' => $order])
                    </div>
                    </div>
                </div>

            </div>

                </div>
        </div>
        </div>
    </section>

@endsection
@section('page-scripts')
    <script src="{{asset('assets/js/scripts/forms/select/form-select2.js')}}"></script>
@endsection

@push("styles")
    <style>
      .mfp-wrap, .mfp-bg { z-index: 2000 }
    </style>
@endpush

@push("scripts")
    <script>
        $( document ).ready(function() {
            $('.image-preview').magnificPopup({
                type: 'image'
                // other options
            });
        });
    </script>
 @endpush
