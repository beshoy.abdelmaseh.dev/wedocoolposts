@section('modal_title')
    {{$modal_title ?? "Manage Question" }}
@stop
@section('modal_content')
    <div class="modal-body p-1">
        <form class="ajax-form" data-alert="#manage-question-alert" data-scrolltoalert="true"
              data-button="#manage-question-button" id="manage-question-form" method="POST"
              action="{{route("admin.actions.manage",[$details->id,$offset])}}">
            @csrf
            @method('PUT')
            <div class="row">
                <div class="col-12">
                    @component('components.alert')
                        @slot('alert_theme')  background  @endslot
                        @slot('custom_class')  d-none  @endslot
                        @slot('alert_id')  manage-question-alert  @endslot
                    @endcomponent
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 question-component" id="manage-order-component">
                    <div class="form-group question-form-component" id="manage-order-form-component">
                        <label  class="control-label">Data</label>
                        <textarea type="text" class="form-control summernote" id="manage-order-data" name="data[]" style="resize: none;"
                                  maxlength="250">{!! optional(optional($action)->data)->first() !!}</textarea>

                        @include("components.voice-note",["text" => ["save" => "Save" , "save_record" => "Save Record"],"materials" => optional($action)->materials, "id" => "manage-order-voice-note-recorder", "class"=> "summernote-additional-tools d-none","store" => route("uploads.voice_note"), "inputname"=> "voice_notes[]" , "type" => "texteditor-addon-manage-order"])
                    </div>
                </div>
                <div class="col-md-12">
                    <ul class="list-unstyled mb-0 mt-2">
                        <li class="d-inline-block  mb-1">
                            <fieldset>
                                <div class="checkbox checkbox-primary">
                                    <input type="checkbox" id="colorCheckbox1" name="meta[watermark]" {{optional($action)->getMeta("watermark",false) ? "checked" : ""}}>
                                    <label for="colorCheckbox1">Watermark on images</label>
                                </div>
                            </fieldset>
                        </li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="form-group mt-1 material-upload mb-0">
                        <label>Upload Materials</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input upload-file" data-store="{{route("uploads.store")}}" data-inputname="materials[]" id="materials-{{$details->id}}">
                            <label class="custom-file-label" for="materials-{{$details->id}}">Choose file</label>
                        </div>
                        <small>Allowed extensions : images( jpg,jpeg,png,bmp ) and pdf only | Maximum size : 8MB.</small>
                        <div class="mt-2 ml-1">
                            <div class="progress progress-bar-primary mb-2 progress-input d-none">
                                <div class="progress-bar progress-label" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100" style="width:1%"></div>
                            </div>
                        </div>
                        <div >
                            <div class="row p-0 px-1 justify-content-md-start material-preview">
                                @if($materials = optional($action)->materials)
                                    @foreach($materials->whereNull("type") as $material)
                                        @php($file = explode(".",$material->storage_path))
                                        @php($extension = end($file) )
                                        <div class="mb-50 main-review">
                                           <input type="hidden" name="materials[]" value="{{$material->storage_path}}_{{$material->created_at}}">
                                            <div class="position-relative ">
                                                <a href="{{route("uploads.show",$material->storage_path)}}" {{($extension == "pdf" ? 'target=_blank' :'class=image-preview' )}} >
                                                    <img  src="{{$extension == "pdf" ? asset("assets/images/icon/pdf-2.png") : route("uploads.show",$material->storage_path)}}" class="img img-thumbnail rounded-lg mr-1"   {{($extension == "pdf" ? 'width=60' :  'width=100' )}} />
                                                </a>
                                                <div class="position-absolute remove-material" data-toggle="tooltip"  data-original-title="Remove" data-placement="top"  style="top: 0%;right:15%;cursor: pointer;"><i class="bg-danger text-white  bx bx-x rounded"></i></div>
                                            </div>
                                        </div>
                                    @endforeach
                                    @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-info waves-effect waves-light submit-ajax-form" id="manage-question-button"
                data-form="#manage-question-form">
            <span class="spinner-border spinner-border-sm" style="display: none;" role="status"></span><span
                    class="button-text">Save</span>
        </button>
    </div>
    <script>
        function inFormcallbeforeAjax(formEl){
            if (formEl.data("voice_recorder") === true) {
                var getToolObj = $(formEl.data("button")).closest(".note-voice-record");

                getToolObj.find(".record-state").addClass("d-none");
                getToolObj.find(".save-voice-btn button").tooltip('hide');
                getToolObj.find(".cancel-voice-btn").addClass("d-none");
            }
        }

        function AjaxHandleErrorMessage(formEl,response) {
            if (formEl.data("voice_recorder") === true) {
                alert("Failed to upload voice note.");
                var getToolObj = $(formEl.data("button")).closest(".note-voice-record");
                getToolObj.find(".save-voice-btn").addClass("d-none");
                getToolObj.find(".save-voice-btn button").tooltip('hide');
                getToolObj.find(".start-voice-btn").removeClass("d-none");
            }
        }

        function ajaxUnHandeledError(formEl,alertMessage,formData) {
            if (formEl.data("voice_recorder") === true) {
                alert("failed to deliver voice note , check your internet connection please!");
            }
        }
        </script>
@stop
