@extends('layouts.contentLayoutMaster')
{{-- page title --}}
@section('title','Orders')
{{-- vendor style --}}

{{-- page style --}}
@section('page-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/pages/app-invoice.css')}}">
@endsection

@section('content')
    <!-- invoice list -->
    <section class="invoice-list-wrapper ">
        <div class="table-responsive">
            <table class="table invoice-data-table dt-responsive nowrap" style="width:100%">
                <thead>
                <tr>
                    <th></th>
                    <th>
                        <span class="align-middle">Order#</span>
                    </th>
                    <th>Type</th>
                    <th>User</th>
                    <th>Amount Due</th>
                    <th>Order Status</th>
                    <th>Payment Status</th>
                    <th class="text-center">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                <tr id="orders-{{$order->id}}">

                    <td></td>
                    <td>
                        <a href="{{route('admin.invoices.show',$order->invoice->id)}}">{{$order->invoice->slug}}</a>
                    </td>
                    <td><span class="invoice-amount font-weight-bold">{{$order->main_question}}</span></td>
                    <td><span class="invoice-amount">{{optional($order->user)->full_name}}</span></td>
                    <td><span class="invoice-amount">{{$order->invoice->amount}} EGP</span></td>
                    <td><div class="{{optional($order->action->status)->class}} font-small-2">{{ucwords(optional($order->action->status)->name)}}</div></td>
                    <td>

                        @if($order->invoice->state == "full-paid")
                            <div class="badge badge-light-success badge-pill">Full PAID</div>
                        @elseif($order->invoice->state == "half-paid")
                            <div class="badge badge-light-primary badge-pill">50% PAID</div>
                        @else
                            <div class="badge badge-light-danger badge-pill">UNPAID</div><br>
                        @endif

                    </td>
                    <td class="text-center">
                        <div class="invoice-action d-inline-flex">
                            <div>
                            <a href="{{route('admin.invoices.show',$order->invoice->id)}}" class="text-info"  data-toggle="tooltip"  data-original-title="Invoice" data-placement="top">
                                <i class="bx bx-show-alt mr-1 font-medium-4"></i>
                            </a>
                            <a href="{{route('admin.orders.manage',$order->invoice->slug)}}" class="text-primary "  data-toggle="tooltip"  data-original-title="Manage" data-placement="top">
                                <i class="bx bx-target-lock font-medium-4 mr-1"></i>
                            </a>
                            </div>
                            <div>
                                 <span class="cursor-pointer text-danger action-delete-item" style="cursor: pointer;" data-toggle="tooltip"  data-original-title="Delete" data-placement="top"
                                       data-remove_element="#orders-{{$order->id}}"
                                       data-name="order"
                                       data-url="{{route("admin.orders.destroy",$order->id)}}">
                                               <i class="bx bx-trash-alt font-medium-4"></i>
                                            </span>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </section>
@endsection

{{-- page scripts --}}
@push('scripts')
    <script>

                var dataListView = $( ".invoice-data-table" ).DataTable({
                    "iDisplayLength": 50,
                    "bSort" : false,
                    columnDefs: [
                        {

                        },
                    ],
                    //order: [2, 'asc'],
                    dom:
                        '<"top d-flex flex-wrap"<"action-filters flex-grow-1"f><"actions action-btns d-flex align-items-center">><"clear">rt<"bottom"p>',
                    language: {
                        search: "",
                        searchPlaceholder: "Search Order"
                    },

                    responsive: {
                        /* details: {
                             type: "column",
                             target: 0
                         }*/
                    }
                });
    </script>
@endpush
