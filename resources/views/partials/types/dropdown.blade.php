<select class="form-control {{ !isset($reInit) ? "select2" : "" }}"   {{ isset($multiple) ? 'multiple="multiple" name=type_id[]' : 'name=type_id' }} data-search="false"  @if(isset($changeableContent))  data-show_changeable_content="true" @endif @if(isset($reInit)) data-plugin="re-init-libs"  data-class="select2" @endif
        id="{{$type}}-types-dropdown" {{ isset($disabled) ? $disabled ? "disabled" : null : null }}
        {{ isset($readonly) ? $readonly ? "readonly" : null : null }}>
    <option value="">@lang("Select") {{ucfirst(__($type))}}</option>
    @foreach($types as $type)
        @if (isset($excepts) && (in_array($type->id,$excepts)))
            @continue
        @endif
        <option value="{{$type->id}}" {{isset($type_id) ? ((in_array($type->id,$type_id)) ? "selected" : null) : null}} >{{$type->name}}</option>
    @endforeach
</select>
