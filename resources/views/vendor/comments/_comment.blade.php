@inject('markdown', 'Parsedown')
@php($markdown->setSafeMode(true))

@if(isset($reply) && $reply === true)
  <div id="comment-{{ $comment->getKey() }}" class="media">
@else
  <li id="comment-{{ $comment->getKey() }}" class="media">
@endif
    <img class="mr-3 d-none" src="https://www.gravatar.com/avatar/{{ md5($comment->commenter->email ?? $comment->guest_email) }}.jpg?s=64" alt="{{ $comment->commenter->name ?? $comment->guest_name }} Avatar">
    <div class="media-body shadow rounded-lg mb-1 px-1 pt-1" @if($comment->child_id) style=" background-color: #f8f8f8 !important;" @endif>
        <div class="mt-0 mb-1 font-weight-bold border-bottom">{{ $comment->commenter->full_name ?? $comment->guest_name }} <small class="text-muted">- {{ $comment->created_at->diffForHumans() }}</small></div>
        @if(str_contains($comment->comment,"voice-notes"))
            <div class="col-sm-12 col-md-12 col-lg-6">
                <audio src="{{route("uploads.show",$comment->comment)}}" controls="" style="width:100%"></audio>
            </div>
        @else
            <div style="white-space: pre-wrap;">{!! $markdown->line($comment->comment) !!}</div>
        @endif
        <div>
            @can('reply-to-comment', $comment)
                <button data-toggle="modal" data-target="#reply-modal-{{ $comment->getKey() }}" class="btn btn-sm btn-link text-uppercase">@lang("Reply")</button>
            @endcan
            @can('edit-comment', $comment)
                @if(!str_contains($comment->comment,"voice-notes"))
                <button data-toggle="modal" data-target="#comment-modal-{{ $comment->getKey() }}" class="btn btn-sm btn-link text-uppercase">@lang("Edit")</button>
                 @endif
            @endcan
            @can('delete-comment', $comment)
                <a href="{{ route('comments.destroy', $comment->getKey()) }}" onclick="event.preventDefault();document.getElementById('comment-delete-form-{{ $comment->getKey() }}').submit();" class="btn btn-sm btn-link text-danger text-uppercase">@lang("Delete")</a>
                <form id="comment-delete-form-{{ $comment->getKey() }}" action="{{ route('comments.destroy', $comment->getKey()) }}" method="POST" style="display: none;">
                    @method('DELETE')
                    @csrf
                </form>
            @endcan
        </div>

        @can('edit-comment', $comment)
            <div class="modal fade" id="comment-modal-{{ $comment->getKey() }}" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <form method="POST" action="{{ route('comments.update', $comment->getKey()) }}">
                            @method('PUT')
                            @csrf
                            <div class="modal-header">
                                <h5 class="modal-title">@lang("Edit Comment")</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                <span>&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="message">@lang("Update your message here"):</label>
                                    <textarea required class="form-control" name="message" rows="3">{{ $comment->comment }}</textarea>
                                    <small class="form-text text-muted d-none"><a target="_blank" href="https://help.github.com/articles/basic-writing-and-formatting-syntax">Markdown</a> cheatsheet.</small>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-sm btn-secondary text-uppercase" data-dismiss="modal">@lang("Cancel")</button>
                                <button type="submit" class="btn btn-sm btn-success text-uppercase">@lang("Update")</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan

        @can('reply-to-comment', $comment)
            <div class="modal fade" id="reply-modal-{{ $comment->getKey() }}" tabindex="-1" role="dialog">
                <div class="modal-dialog question-component" role="document">
                    <div class="modal-content question-form-component" id="comment-reply-box-{{$comment->getKey()}}">
                        <form method="POST" action="{{ route('comments.reply', $comment->getKey()) }}">
                            @csrf
                            <div class="modal-header">
                                <h5 class="modal-title">@lang("Reply to Comment")</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                <span>&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="inputs d-none">
                                    <input type="hidden" name="comment_reply" value="{{ $comment->getKey() }}" />
                                    <input type="hidden" name="current_url" value="{{ url()->full() }}" />
                                </div>
                                <div class="form-group">
                                    <label for="message">@lang("Enter your message here"):</label>
                                    <textarea required class="form-control" name="message" rows="3"></textarea>
                                    <small class="form-text text-muted d-none"><a target="_blank" href="https://help.github.com/articles/basic-writing-and-formatting-syntax">Markdown</a> cheatsheet.</small>
                                </div>
                            </div>
                            <div class="modal-footer comment-box p-0  py-1">
                                <button type="button" class="btn btn-sm btn-secondary text-uppercase" data-dismiss="modal">@lang("Cancel")</button>
                                <button type="submit" class="btn btn-sm btn-success text-uppercase">@lang("Reply")</button>
                                @include("components.voice-note",["text" => ["save" => "Send" , "save_record" => "Send Record"],"class" => "" , "materials" => null,"id" => "comment-reply-form-voice-note-recorder-".$comment->getKey(),"store" => route("uploads.voice_note") , "type" => "comment-box"])
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endcan

        <br />{{-- Margin bottom --}}

        {{-- Recursion for children --}}
        @if($grouped_comments->has($comment->getKey()))
            @foreach($grouped_comments[$comment->getKey()] as $child)
                @include('comments::_comment', [
                    'comment' => $child,
                    'reply' => true,
                    'grouped_comments' => $grouped_comments
                ])
            @endforeach
        @endif

    </div>
@if(isset($reply) && $reply === true)
  </div>
@else
  </li>
@endif
