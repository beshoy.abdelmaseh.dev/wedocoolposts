<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_metas', function (Blueprint $table) {
            $table->primary(["transaction_id","key"]);
            $table->foreignId('transaction_id')->constrained('transactions')->onDelete('cascade');
            $table->string("label")->nullable();
            $table->string("key");
            $table->longText("value")->nullable();
            $table->longText("data")->nullable();
            $table->boolean("default")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_metas');
    }
}
