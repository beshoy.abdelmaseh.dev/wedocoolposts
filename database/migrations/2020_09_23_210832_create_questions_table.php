<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->boolean('main')->default(false);
            //$table->boolean('changeable')->default(false);
            $table->string('title');
            $table->string('label')->nullable();
            $table->string('type')->nullable();
            $table->string('sub_type')->nullable();
            $table->text("desc")->nullable();
            $table->string('key');
            $table->decimal('price',14,2)->default(0.00);
            $table->longText("validation")->nullable();
            $table->longText("data")->nullable();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedTinyInteger('state')->default(0);
            $table->unsignedTinyInteger('sorting')->default(0);
            $table->unsignedTinyInteger('group_id')->default(0);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('created_by')->references('id')->on('users');
        });
        \DB::statement("ALTER TABLE ".\DB::getTablePrefix()."questions ADD CONSTRAINT questions_parent_id_foreign FOREIGN KEY (parent_id) REFERENCES ".\DB::getTablePrefix()."questions(id)");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
