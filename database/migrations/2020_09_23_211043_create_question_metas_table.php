<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_metas', function (Blueprint $table) {
            $table->primary(["question_id","key"]);
            $table->foreignId('question_id')->constrained('questions')->onDelete('cascade');
            $table->string("label")->nullable();
            $table->string("key");
            $table->longText("value")->nullable();
            $table->longText("data")->nullable();
            $table->boolean("default")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_metas');
    }
}
