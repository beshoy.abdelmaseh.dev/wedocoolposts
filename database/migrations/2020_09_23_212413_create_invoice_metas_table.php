<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_metas', function (Blueprint $table) {
            $table->primary(["invoice_id","key"]);
            $table->foreignId('invoice_id')->constrained('invoices')->onDelete('cascade');
            $table->string("label")->nullable();
            $table->string("key");
            $table->longText("value")->nullable();
            $table->longText("data")->nullable();
            $table->boolean("default")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_metas');
    }
}
