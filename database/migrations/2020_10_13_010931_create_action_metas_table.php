<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_metas', function (Blueprint $table) {
            $table->primary(["action_id","key"]);
            $table->foreignId('action_id')->constrained('actions')->onDelete('cascade');
            $table->string("label")->nullable();
            $table->string("key");
            $table->longText("value")->nullable();
            $table->longText("data")->nullable();
            $table->boolean("default")->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_metas');
    }
}
