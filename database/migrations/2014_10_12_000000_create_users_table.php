<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('full_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('phone',20)->unique();
            $table->timestamp('phone_verified_at')->nullable();
            $table->string('password');
            $table->tinyInteger('state')->default(0)->unsigned();
            $table->timestamp('approved_at')->nullable();
            $table->timestamp("last_active_at")->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
        \DB::statement("ALTER TABLE ".\DB::getTablePrefix()."users ADD CONSTRAINT created_by_foreign FOREIGN KEY (created_by) REFERENCES ".\DB::getTablePrefix()."users(id)");
        \DB::statement("ALTER TABLE ".\DB::getTablePrefix()."users ADD  FULLTEXT full_name_fulltext(full_name)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
