<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('title');
            $table->string('desc')->nullable();
            $table->string('type')->nullable();
            $table->text('user_id')->nullable();
            $table->unsignedTinyInteger("state")->default(0);
            $table->decimal('amount',14,2)->default(0.00);
            $table->decimal('minimum_amount',14,2)->default(0.00);
            $table->integer("max_usage")->default(0);
            $table->timestamp("start_date")->nullable();
            $table->timestamp("end_date")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
