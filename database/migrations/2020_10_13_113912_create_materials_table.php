<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->morphs("model");
            $table->string('slug')->nullable();
            $table->string("name")->nullable();
            $table->string("label")->nullable();
            $table->string("type")->nullable();
            $table->string("remote_url")->nullable();
            $table->string("storage_domain")->nullable();
            $table->string("storage_path")->nullable();
            $table->string("size")->nullable();
            $table->string("extension")->nullable();
            $table->text("desc")->nullable();
            $table->tinyInteger('state')->unsigned()->default(0);
            $table->unsignedBigInteger('created_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
    }
}
