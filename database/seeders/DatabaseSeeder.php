<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{

		$this->call([
			MainTableSeeder::class,
			RolesTableSeeder::class,
			UsersTableSeeder::class,
           // QuestionsTableSeeder::class,
			]);
	}
}
