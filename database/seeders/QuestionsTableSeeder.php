<?php
namespace Database\Seeders;

use App\Models\Question\Question;
use App\Models\User\User;
use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Question::insert([
            [
                "title" => "Creative",
                "main" => true,
                "desc" => "create creative post",
                "key" => "creative",
                "price" => 10,
            ], [
                "title" => "Template",
                "main" => true,
                "desc" => "create new template",
                "key" => "template",
                "price" => 15.5,
            ],
            [
                "title" => "Educational",
                "main" => true,
                "desc" => "create new Educational",
                "key" => "educational",
                "price" => 30,
            ]
            ,[
                "title" => "Album",
                "main" => true,
                "desc" => "create new album",
                "key" => "album",
                "price" => 10,
            ]
        ]);

        $question = Question::where("key","creative")->first();

        $question->syncMetas([
            "icon" => "magic.svg"
        ]);

        $question->children()->insert([
            [
                "group_id" => 0,
                "title" => "Radio Label 1",
                "type" => "radio",
                "key" => "radio_label_1",
                "price" => 1,
                "sorting" => 0,
                "parent_id" => $question->id,
            ],
            [
                "group_id" => 0,
                "title" => "Radio Label 2",
                "type" => "radio",
                "key" => "radio_label_2",
                "price" => 2,
                "sorting" => 1,
                "parent_id" => $question->id,
            ],
            [
                "group_id" => 0,
                "title" => "Radio Label 3",
                "type" => "radio",
                "key" => "radio_label_3",
                "price" => 3,
                "sorting" => 2,
                "parent_id" => $question->id,
            ],
            [
                "group_id" => 1,
                "title" => "Breif",
                "type" => "texteditor",
                "key" => "breif",
                "price" => 0,
                "sorting" => 0,
                "parent_id" => $question->id,
            ],
            [
                "group_id" => 2,
                "title" => "When do you need it?",
                "type" => "sub_questions",
                "key" => "when_do_you_need_it",
                "price" => 0,
                "sorting" => 0,
                "parent_id" => $question->id,
            ],
            [
                "group_id" => 2,
                "title" => "How Many Revisions do you need?",
                "type" => "sub_questions",
                "key" => "how_many_revisions_do_you_need",
                "price" => 0,
                "sorting" => 1,
                "parent_id" => $question->id,
            ],
            [
                "group_id" => 2,
                "title" => "Do You need a Caption?",
                "type" => "sub_questions",
                "key" => "do_you_need_captions",
                "price" => 0,
                "sorting" => 2,
                "parent_id" => $question->id,
            ],
        ]);


        $whenDoYouNeedIt = Question::where("key","when_do_you_need_it")->first();

        $whenDoYouNeedIt->syncMetas([
         "label-class" => "font-medium-3 font-weight-bold"
         ]);

        $whenDoYouNeedIt->children()->insert([
            [
                "title" => "Radio Label 1",
                "type" => "radio",
                "key" => "when_do_you_need_it_radio_label_1",
                "price" => 5,
                "sorting" => 0,
                "parent_id" => $whenDoYouNeedIt->id,
            ],
            [
                "title" => "Radio Label 2",
                "type" => "radio",
                "key" => "when_do_you_need_it_radio_label_2",
                "price" => 6,
                "sorting" => 1,
                "parent_id" => $whenDoYouNeedIt->id,
            ],
            [
                "title" => "Radio Label 3",
                "type" => "radio",
                "key" => "when_do_you_need_it_radio_label_3",
                "price" => 7,
                "sorting" => 2,
                "parent_id" => $whenDoYouNeedIt->id,
            ],
        ]);

        $howManyRevisions = Question::where("key","how_many_revisions_do_you_need")->first();
        $howManyRevisions->syncMetas([
            "label-class" => "font-medium-3 font-weight-bold",
            "top-html" => "<hr>"
        ]);
        $howManyRevisions->children()->insert([
            [
                "title" => "Radio Label 1",
                "type" => "radio",
                "key" => "how_many_revisions_do_you_need_radio_label_1",
                "price" => 3,
                "sorting" => 0,
                "parent_id" => $howManyRevisions->id,
            ],
            [
                "title" => "Radio Label 2",
                "type" => "radio",
                "key" => "how_many_revisions_do_you_need_radio_label_2",
                "price" => 3,
                "sorting" => 1,
                "parent_id" => $howManyRevisions->id,
            ],
            [
                "title" => "Radio Label 3",
                "type" => "radio",
                "key" => "how_many_revisions_do_you_need_radio_label_3",
                "price" => 5,
                "sorting" => 2,
                "parent_id" => $howManyRevisions->id,
            ],
        ]);


        $doYouNeedCaptions = Question::where("key","do_you_need_captions")->first();
        $doYouNeedCaptions->syncMetas([
            "label-class" => "font-medium-3 font-weight-bold",
            "top-html" => "<hr>"
        ]);
        $doYouNeedCaptions->children()->insert([
            [
                "title" => "Radio Label 1",
                "type" => "radio",
                "key" => "do_you_need_captions_radio_label_1",
                "price" => 7,
                "sorting" => 0,
                "parent_id" => $doYouNeedCaptions->id,
            ],
            [
                "title" => "Radio Label 2",
                "type" => "radio",
                "key" => "do_you_need_captions_radio_label_2",
                "price" => 9,
                "sorting" => 1,
                "parent_id" => $doYouNeedCaptions->id,
            ],
            [
                "title" => "Radio Label 3",
                "type" => "radio",
                "key" => "do_you_need_captions_radio_label_3",
                "price" => 10,
                "sorting" => 2,
                "parent_id" => $doYouNeedCaptions->id,
            ],
        ]);
    }
}
