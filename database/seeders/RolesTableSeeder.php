<?php
namespace Database\Seeders;

use App\Models\User\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    Role::insert([
		    [
			    "name" => "developer",
			    "label" => "Developer",
			    "guard_name" => "web",
			    "desc" => "full access on the dashboard and web app",
		    ],
		    [
			    "name" => "admin",
			    "label" => "Administrator",
			    "guard_name" => "web",
			    "desc" => "full access on the dashboard and web app",
		    ]
	    ]);
    }
}
