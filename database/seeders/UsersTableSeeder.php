<?php
namespace Database\Seeders;

use App\Models\User\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $user = User::create(
		    [
			    "full_name" => "Beshoy Mounir",
			    "email" => "beshoy.mounir.fd@gmail.com",
			    "phone" => "01288675980",
			    "password" => bcrypt("secret"),
			    'email_verified_at' => now(),
		    ]
	    );
		$user->roles()->sync([1]);

        $userAdmin = User::create(
            [
                "full_name" => "Admin Account",
                "email" => "admin@wedocoolposts.com",
                "phone" => "0122222222",
                "password" => bcrypt("secret"),
                'email_verified_at' => now(),
            ]
        );
        $userAdmin->roles()->sync([1]);

    }
}
