<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('home.index');


Route::get("questions/{question}/duplicate",[App\Http\Controllers\Admin\QuestionController::class,"duplicate"])->name("questions.duplicate");
Route::resource("questions",App\Http\Controllers\Admin\QuestionController::class);

Route::resource("orders",App\Http\Controllers\Admin\OrderController::class);
Route::resource("invoices",App\Http\Controllers\Admin\InvoiceController::class);
Route::resource("users",App\Http\Controllers\Admin\UserController::class);
Route::get("coupons/data-table", [App\Http\Controllers\Admin\CouponController::class,"dataTable"])->name("coupons.data-table");
Route::resource("coupons",App\Http\Controllers\Admin\CouponController::class);


Route::get("settings", [App\Http\Controllers\Admin\SettingController::class,"edit"])->name("settings.edit");
Route::put("settings", [App\Http\Controllers\Admin\SettingController::class,"update"])->name("settings.update");

Route::get("types/data-table/{type}", [App\Http\Controllers\Admin\TypeController::class,"dataTable"])->name("types.data-table");
Route::get("types/order-status", [App\Http\Controllers\Admin\TypeController::class,"orderStatus"])->name("types.order-status");
Route::get("types/business-types", [App\Http\Controllers\Admin\TypeController::class,"businessTypes"])->name("types.business-types");
Route::resource("types",App\Http\Controllers\Admin\TypeController::class)->except("index");

Route::match(["get","put"],'actions/{action}/status', [App\Http\Controllers\Admin\ActionController::class, 'updateStatus'])->name('actions.update_status');
Route::match(["get","put"],'actions/{details}/manage/{offset?}', [App\Http\Controllers\Admin\ActionController::class, 'manage'])->name('actions.manage');
Route::get('orders/{invoice:slug}/manage', [App\Http\Controllers\Admin\TrackingController::class, 'manage'])->name('orders.manage');

Route::get('transactions/{transaction}/details', [App\Http\Controllers\Admin\TransactionController::class, 'details'])->name('transactions.details');
