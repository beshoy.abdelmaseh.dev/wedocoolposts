<?php

use App\Models\Order\Order;
use App\Models\Question\Question;
use App\Models\User\User;
use App\Notifications\NewOrder;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix(LaravelLocalization::setLocale())->middleware(['localeSessionRedirect', 'localizationRedirect'])->group(function () {
    Route::get('/', [App\Http\Controllers\Web\HomeController::class, 'index'])->name('home');

    Route::get('orders', [App\Http\Controllers\Web\OrderController::class, 'index'])->name('orders.index');
    Route::get('orders/{question:key}/create', [App\Http\Controllers\Web\OrderController::class, 'create'])->name('orders.create');
    Route::post('orders/{question}/store', [App\Http\Controllers\Web\OrderController::class, 'store'])->name('orders.store');
    Route::get('invoices/{invoice:slug}/show', [App\Http\Controllers\Web\InvoiceController::class, 'show'])->name('invoices.show');
    Route::put('invoices/{invoice:slug}/apply-coupon', [App\Http\Controllers\Web\InvoiceController::class, 'applyCoupon'])->name('invoices.apply_coupon');
    Route::get('invoices/{invoice:slug}/remove-coupon', [App\Http\Controllers\Web\InvoiceController::class, 'removeCoupon'])->name('invoices.remove_coupon');

    Route::middleware(["auth"])->group(function () {
        Route::get('profile/{page?}', [App\Http\Controllers\Web\ProfileController::class, 'edit'])->name('dashboard.profile.edit');
        Route::put('profile/{page?}', [App\Http\Controllers\Web\ProfileController::class, 'update'])->name('dashboard.profile.update');
        Route::post('invoices/{invoice:slug}/checkout', [App\Http\Controllers\Web\InvoiceController::class, 'checkout'])->name('invoices.checkout');
        Route::get('dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard.index');

        Route::post('tracking/find', [App\Http\Controllers\Web\TrackingController::class, 'find'])->name('tracking.find');
        Route::get('tracking/search', [App\Http\Controllers\Web\TrackingController::class, 'search'])->name('tracking.search');
        Route::get('tracking/{invoice:slug}/show', [App\Http\Controllers\Web\TrackingController::class, 'show'])->name('tracking.show');
    });

    Route::match(["get", "put"], 'actions/{action}/status', [App\Http\Controllers\Web\ActionController::class, 'updateStatus'])->name('actions.update_status');

});


Route::post('transaction/integrates/{method}/{gateway}/process-callback', [App\Http\Controllers\Web\TransactionController::class, 'processCallback']);
Route::get('transaction/integrates/{method}/{gateway}/response-callback', [App\Http\Controllers\Web\TransactionController::class, 'responseCallback']);

Route::resource("uploads", App\Http\Controllers\UploadFileController::class)->except(["show"]);
Route::post("uploads/voice-note", [App\Http\Controllers\UploadFileController::class,"voiceNote"])->name("uploads.voice_note");
Route::get("uploads/show/{storage_code}", [App\Http\Controllers\UploadFileController::class, "show"])->where('storage_code', '(.*)')->name("uploads.show");

