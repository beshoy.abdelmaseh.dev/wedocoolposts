<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ShowActionsComponent extends Component
{
    public $order;
    public $details;
    public $actions;
    public $offset;
    public $voiceNotes = [];
    public $actionContent;
    public $materials = [];

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($order,$details,$actions,$offset)
    {
        //
        $this->order = $order;
        $this->details = $details;
        $this->actions = $actions;
        $this->offset = $offset;
        $this->actionContent = optional($this->actions)->firstWhere("offset",$offset);
        if($this->actionContent) {
            if($materials = optional($this->actionContent)->materials) {
                $this->voiceNotes = $materials->where("type", "voice-note")->all();
                if($this->actionContent->getMeta("watermark",false)){
                    $this->materials = $materials->where("type", "watermark")->all();
                }else{
                    $this->materials = $materials->whereNull("type")->all();
                }
            }
         }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.show-actions');
    }
}
