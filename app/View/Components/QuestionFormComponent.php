<?php

namespace App\View\Components;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\Component;

class QuestionFormComponent extends Component
{
    public $question;
    public $isChild;
    public $mainQuestion;
    public $name;
    public $questionTitle;
    /**
     * @var null
     */
    public $groupId;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Request $request,$question,$isChild = null,$groupId = null)
    {
        //
        $this->question = $question;
        $this->isChild = $isChild;
        $this->groupId = $groupId;
        $this->mainQuestion = optional($request->route()->parameters)["question"];
        $this->name = "question[{$question->key}]";


        $labelHint = "";
        if($question->type != "sub_questions" && !empty($question->validation) && !is_array($question->validation)){
            if(Str::contains($question->validation,"required")){
                $labelHint .= (" <small class='text-danger'>(".__("required").")</small>");
            }
            if(Str::contains($question->validation,"nullable")){
                $labelHint .= (" <small class='text-muted'>(".__("optional").")</small>");
            }
        }

        $this->questionTitle = ($question->label ?: $question->title).$labelHint;

        if(Str::contains($question->sub_type , "parent_key")){
            $this->name = "question[{$question->parent->key}]";
        }

        if(Str::contains($question->sub_type , "parent_type")){
            $this->name .= "[".($question->parent->type ?: "none")."]";
        }else {
            $this->name .= "[{$question->type}]";
        }

       // if(!Str::contains($question->sub_type, "name_without_question_id")){
            if(Str::contains($question->sub_type , "parent_id")){
                $this->name .= "[{$question->parent->id}]";
            }else {
                $this->name .= "[{$question->id}]";
            }
        //}
        if($question->getMeta("increment",false)){
            $this->name .= "[0]";
        }
        if($question->getMeta("multi",false)){
            $this->name .= "[]";
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.question-form');
    }
}
