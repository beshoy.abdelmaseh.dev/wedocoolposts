<?php

namespace App\View\Components;

use Illuminate\View\Component;

class QuestionComponent extends Component
{
    public $question;
    public $isChild;
    /**
     * @var null
     */
    public $groupId;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($question,$isChild  = null,$groupId = null)
    {
        //
        $this->question = $question;
        $this->isChild = $isChild;
        $this->groupId = $groupId;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.question');
    }
}
