<?php


namespace App\View\Composers;


use App\Models\Question\Question;
use Illuminate\View\View;

class MainQuestionsComposer
{
	public function compose(View $view)
	{
        $mainQuestions = Question::query()->isMain()->isActive()->get();

		return $view->with("mainQuestions",$mainQuestions);
	}

}
