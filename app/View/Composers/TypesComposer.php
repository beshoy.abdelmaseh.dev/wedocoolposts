<?php

namespace App\View\Composers;


use App\Models\Type;
use Illuminate\View\View;

class TypesComposer
{
	public function compose(View $view)
	{

		return $view->with("types",Type::where("type",$view->offsetGet("type"))->get());
	}

}
