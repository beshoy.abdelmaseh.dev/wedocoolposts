<?php

namespace App\Rules;

use App\Services\CouponService;
use Illuminate\Contracts\Validation\Rule;

class Coupon implements Rule
{
    private $invoice;
    private $user;
    private $message;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($invoice,$user)
    {
        //
        $this->invoice = $invoice;
        $this->user = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($errorMessage = CouponService::validate($this->invoice,$this->user ,$value)){
            $this->message = $errorMessage;
            return false;
        }

       return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
