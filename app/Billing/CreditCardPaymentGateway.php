<?php


namespace App\Billing;


use App\Billing\Integrates\PaymobSolutions;

class CreditCardPaymentGateway
{

    /**
     * @var PaymobSolutions
     */
    private $gateway;
    private $options;

    public function __construct($options)
    {
        $this->gateway = new PaymobSolutions($options);
        $this->options = $options;
    }

    public function provider()
    {
        return $this->gateway::PROVIDER;
    }

    public function charge($amount)
    {
        $this->gateway->setChargeAmount($amount);

    }

    public function setBillingData($data)
    {
        return  $this->gateway->setBillingData($data);
    }

    public function setItems($data)
    {
        return  $this->gateway->setItems($data);
    }

    public function initializePaymentUrl()
    {
        $this->gateway->initializePaymentUrl();
    }

    public function redirect()
    {
        return $this->gateway->redirectToPayPage();
    }

    public function urlGeneratedSuccessfully()
    {
        return $this->gateway->urlGeneratedSuccessfully();
    }

    public function storeTransactionData($data)
    {
        return $this->gateway->storeTransactionData($data);
    }

    public function referenceId()
    {
        return $this->gateway->referenceId();
    }

    public function validatePayment($request,$transaction)
    {
        return  $this->gateway->validatePayment($request,$transaction);
    }
}
