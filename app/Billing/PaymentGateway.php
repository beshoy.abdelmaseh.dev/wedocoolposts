<?php


namespace App\Billing;

class PaymentGateway
{

    public $method = null;
    protected $paymentGateway = null;
    public $paymentProvider = null;
    private $options;

    public function charge($amount){

        if(!$this->paymentGateway)
            throw new \Exception("Missing Payment Method for charge.");

       return  $this->paymentGateway->charge($amount);
    }
    public function options($options){

        if(!$this->paymentGateway)
            throw new \Exception("Missing Payment Method for options.");

        $this->options = $options;

        return  $this;
    }
    public function initializePaymentUrl()
    {
        $this->paymentGateway->initializePaymentUrl();
    }
    public function redirect(){

        if(!$this->paymentGateway)
            throw new \Exception("Missing Payment Method for preparePaymentUrl.");

        return  $this->paymentGateway->redirect();
    }

    public function setBillingData($data)
    {
        if(!$this->paymentGateway)
            throw new \Exception("Missing Payment Method for setBillingData.");

        return  $this->paymentGateway->setBillingData($data);
    }

    public function setItems($data)
    {
        if(!$this->paymentGateway)
            throw new \Exception("Missing Payment Method for setItems.");

        return  $this->paymentGateway->setItems($data);
    }

    public function urlGeneratedSuccessfully()
    {
        if(!$this->paymentGateway)
            throw new \Exception("Missing Payment Method for setItems.");

        return  $this->paymentGateway->urlGeneratedSuccessfully();
    }
    public function setPaymentMethod($method  = 'credit-card')
    {
        $this->method = $method;

        switch($this->method){
            case 'credit-card':
                $this->paymentGateway = new CreditCardPaymentGateway($this->options);
                $this->paymentProvider = $this->paymentGateway->provider();
            break;
            default:
                throw new \Exception("Invalid Payment Method For Payment Gateway.");
        }

    }

    public function referenceId()
    {
        return $this->paymentGateway->referenceId();
    }

    public function storeTransactionData($data)
    {
        if(!$this->paymentGateway)
            throw new \Exception("Missing Payment Method for storeTransactionData.");

        return  $this->paymentGateway->storeTransactionData($data);
    }


    public function validatePayment($request,$transaction)
    {
        return  $this->paymentGateway->validatePayment($request,$transaction);
    }

}
