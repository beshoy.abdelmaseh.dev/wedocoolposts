<?php


namespace App\Billing\Integrates;


use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

class PaymobSolutions
{

    const PROVIDER = "paymobsolutions";
    private $api_key = null;
    private $integration_id = null;
    private $authToken = null;
    private $merchentId = null;
    private $chargeAmount = null;
    private $currency = "EGP";
    private $registeredOrder = null;
    private $redirectUrl = null;
    private $billingData = [];
    private $paymentKeyToken = null;
    private $iframe_link;
    private $items = [];
    private $options;

    public function __construct($options)
    {

       $this->api_key = trim(settings("paymobsolutions_api_key"));

       $this->integration_id = trim(settings("paymobsolutions_integration_id"));

       $this->iframe_link = trim(settings("paymobsolutions_iframe_link"));

        if(settings("test_mode_weaccept")){
            $this->api_key = trim("ZXlKaGJHY2lPaUpJVXpVeE1pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SndjbTltYVd4bFgzQnJJam95TmpRNU1Dd2libUZ0WlNJNkltbHVhWFJwWVd3aUxDSmpiR0Z6Y3lJNklrMWxjbU5vWVc1MEluMC5sZGZDZndyb09ub19IUGdnLWFjRUczTDBhb0xKN3h4OG1uMWM0c0dXaU9ueWZDbDhYV212cWlkNHQyRklUdGVJYXpSRXlhZFRKUmNnYWtSeW9QcWhaQQ==");

            $this->integration_id = trim("58844");

            $this->iframe_link = trim("https://accept.paymobsolutions.com/api/acceptance/iframes/77988?payment_token={payment_key_obtained_previously}");
        }

        $this->options = $options;
    }

    public function getAuthToken()
    {
        try {
            $response = Http::withHeaders([
                'Content-Type' => 'application/json'
            ])->post('https://accept.paymobsolutions.com/api/auth/tokens', [
                'api_key' => $this->api_key,
            ]);

            if (!empty($response->body()) && $response->json("token")) {
                $this->authToken = $response->json("token");
                $this->merchentId = $response->json("profile.id");
            }else{
                Log::error("getAuthToken:".$response->body());
            }
        } catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
    }

    public function setChargeAmount($amount)
    {
        $this->chargeAmount = $amount;
    }

    private function registerOrder()
    {

        if ($this->authToken && $this->merchentId && $this->chargeAmount && $this->currency) {
            try {
                $response = Http::withHeaders([
                    'Content-Type' => 'application/json'
                ])->post('https://accept.paymobsolutions.com/api/ecommerce/orders', [
                    "auth_token" => $this->authToken,
                    "delivery_needed" => "false",
                    "merchant_id" => $this->merchentId,
                    "amount_cents" => $this->chargeAmount,
                    "currency" => $this->currency,
                    "items" => [$this->items],
                ]);

                if (!empty($response->body()) && $response->json("id")) {
                    $this->registeredOrder = collect($response->json());
                }else{
                    Log::error("registerOrder:".$response->body());
                }
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
            }
        }

    }
    public function setBillingData($data)
    {
        $acceptableData = [
            "apartment", "email", "floor", "first_name" , "street" , "building" , "phone_number" ,  "shipping_method","postal_code", "city", "country","last_name" ,"state"
        ];

        $this->billingData = Arr::only($data,$acceptableData);

        foreach($acceptableData as $key){
            if(!array_key_exists($key,$this->billingData)){
                $this->billingData[$key] = "NA";
            }
        }

    }
    public function setItems($data)
    {
        return  $this->items = $data;
    }

    public function cardPaymentKey()
    {
        if ($this->authToken&& $this->chargeAmount && $this->currency) {
            try {
                $response = Http::withHeaders([
                    'Content-Type' => 'application/json'
                ])->post('https://accept.paymobsolutions.com/api/acceptance/payment_keys', [
                    "auth_token" => $this->authToken,
                    "expiration" => 3600,
                    "order_id" => optional($this->registeredOrder)->get("id"),
                    "amount_cents" => $this->chargeAmount,
                    "currency" => $this->currency,
                    "integration_id" => $this->integration_id,
                    "lock_order_when_paid" => false,
                    "billing_data" => $this->billingData
                ]);
                if (!empty($response->body()) && $response->json("token")) {
                    $this->paymentKeyToken = $response->json("token");
                }else{
                    Log::error("cardPaymentKey:".$response->body());
                }
            } catch (\Exception $exception) {
                Log::error($exception->getMessage());
            }
        }
    }

    public function referenceId()
    {
        return optional($this->registeredOrder)->get("id");
    }

    public function initializePaymentUrl()
    {
        $this->getAuthToken();

        $this->registerOrder();

        $this->cardPaymentKey();

        if($this->paymentKeyToken){
            $this->redirectUrl = str_replace("{payment_key_obtained_previously}",$this->paymentKeyToken,$this->iframe_link);
        }
    }

    public function urlGeneratedSuccessfully()
    {

        return is_null($this->redirectUrl) ? false : true;
    }

    public function redirectToPayPage()
    {
        if (is_null($this->redirectUrl))
            throw new \Exception(get_class(self), " failed to get redirect url.");

        return $this->redirectUrl;
    }

    public function storeTransactionData(Request $request)
    {
        $data = [];
        if($request->isJson()){
            $data = $request->json()->all();
        }
        return $data;
    }

    public function validatePayment($request,$transaction)
    {

        if($request->get("order") == $transaction->payment_reference_id && $request->get("success") == "true" && $transaction->getMeta("success") == 1 && $request->get("amount_cents") == ($transaction->amount * 100)){
            if($request->get("pending") == "false" && $transaction->getMeta("pending") == 0 && $this->currency == $request->get("currency") && $request->get("amount_cents") == $transaction->getMeta("amount_cents")){
                if($request->get("id")== $transaction->getMeta("trans_id")){
                    if($transaction->getMeta("integration_id") == $this->integration_id && $request->get("integration_id") == $this->integration_id  && $transaction->getMeta("order_id") == $request->get("order")) {
                        return true;
                    }
                }
            }
        }
        return  false;
    }
}
