<?php
/**
 * Created by PhpStorm.
 * User: Beshoy
 * Date: 16/3/2020 , 016
 * Time: 2:36 PM
 */

namespace App\Filters;


use Illuminate\Database\Eloquent\Builder;

interface Filter
{
	public function apply(Builder $builder, $value);
}