<?php

namespace App\Filters;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class BaseFilter
{

	/**
	 * @var Request
	 */
	private $request;

	function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function apply(Builder $builder, array $filters)
	{

		foreach ($this->getFilters($filters) as $key => $filter) {
			if (!$filter instanceof Filter) {
				continue;
			}
			$filter->apply($builder,$this->request->get($key));
		}
		return $builder;
	}

	public function getFilters(array $filters)
	{

		return Arr::only($filters,array_keys($this->request->all()));
	}
}