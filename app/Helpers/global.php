<?php
if (!function_exists('number_format_short')) {
    function number_format_short($n, $precision = 1)
    {
        if ($n < 990) {
            // 0 - 900
            $n_format = number_format($n, $precision);
            $suffix = '';
        } else if ($n < 990000) {
            // 0.9k-850k
            $n_format = number_format($n / 1000, $precision);
            $suffix = 'K';
        } else if ($n < 990000000) {
            // 0.9m-850m
            $n_format = number_format($n / 1000000, $precision);
            $suffix = 'M';
        } else if ($n < 990000000000) {
            // 0.9b-850b
            $n_format = number_format($n / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($n / 1000000000000, $precision);
            $suffix = 'T';
        }

        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
        // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ($precision > 0) {
            $dotzero = '.' . str_repeat('0', $precision);
            $n_format = str_replace($dotzero, '', $n_format);
        }

        return $n_format . $suffix;
    }
}
if (!function_exists('get_timestamps')) {
    function get_timestamps($dateTime = null)
    {
        return \Carbon\Carbon::now();
    }
}
if (!function_exists('settings')) {
    function settings($key = null)
    {
        $settings = resolve("SettingRepository");
        if($key){
                if($key == "desc" && \LaravelLocalization::getCurrentLocale() == "ar"){
                    return strip_tags($settings->get("desc_ar")) ? $settings->get("desc_ar") : $settings->get($key);
                }
            return $settings->get($key);
        }
        return $settings->readSettings();
    }
}
if (!function_exists('strtoarray')) {
    function strtoarray($a, $t = '')
    {
        $arr = [];
        $a = ltrim($a, '[');
        $a = ltrim($a, 'array(');
        $a = rtrim($a, ']');
        $a = rtrim($a, ')');
        $tmpArr = explode(",", $a);
        foreach ($tmpArr as $v) {
            if ($t == 'keys') {
                $tmp = explode("=>", $v);
                $k = $tmp[0];
                $nv = $tmp[1];
                $k = trim(trim($k), "'");
                $k = trim(trim($k), '"');
                $nv = trim(trim($nv), "'");
                $nv = trim(trim($nv), '"');
                $arr[$k] = $nv;
            } else {
                $v = trim(trim($v), "'");
                $v = trim(trim($v), '"');
                $arr[] = $v;
            }
        }
        return $arr;
    }
}
