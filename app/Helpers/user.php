<?php
if (!function_exists('current_user')) {
    function current_user()
    {
        if (\Auth::check()) {
            return \Auth::user();
        }
        return null;
    }
}
