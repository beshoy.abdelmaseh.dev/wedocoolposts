<?php
if (!function_exists('currency_locale')) {
    function currency_locale($locale = null)
    {
        if (LaravelLocalization::getCurrentLocale() == "ar") {
            return "جنيه";
        }
        return "EGP";
    }
}
if (!function_exists('free_locale')) {

    function free_locale($locale = null)
    {
        if (LaravelLocalization::getCurrentLocale() == "ar") {
            return "مجانا";
        }
        return "FREE";
    }
}
