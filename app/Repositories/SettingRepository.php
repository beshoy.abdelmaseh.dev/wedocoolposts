<?php

namespace App\Repositories;


use App\Models\Setting;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;

class SettingRepository
{

    public $setting;


    function __construct()
    {
        $this->setting = Cache::rememberForever('settings', function ()  {
            return Schema::hasTable((new Setting)->getTable()) ? Setting::all() : [];
        });
    }

    public function get($key, $array = null)
    {
        $get = $this->setting->firstWhere('key', $key);
        if (!$get || empty($get)) return false;
        if (!isset($get["value"]) || empty($get["value"]))
            if ($array)
                return json_decode($get["value"], true);
        return $get["value"];
    }

    public function getSetting($key)
    {
        $value = Setting::where('key', $key)->first();
        if ($value)
            return $value->value;
        return false;
    }

    public function shareSettings()
    {
        return [
            "setting" => $this
        ];
    }
    public function readSettings()
    {
        return $this->setting;
    }

    public function getCachedSettings()
    {
        return $this;
    }
}
