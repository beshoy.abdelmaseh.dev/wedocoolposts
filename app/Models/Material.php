<?php

namespace App\Models;

use App\Traits\Model\HasCreatedBy;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Material extends Model
{
    use SoftDeletes,HasCreatedBy;

    protected $guarded = [];

    public $incrementing = false;

    public static function boot() {
        parent::boot();

        static::creating(function (Material $material) {
            $material->id = Str::uuid()."-".Str::random(6);
        });

    }

    public function model()
    {
        return $this->morphTo();
    }

}
