<?php

namespace App\Models\Invoice;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $guarded = [];

    protected $table = "invoice_metas";


    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function sync(Invoice $invoice, $data)
    {

        $data = $this->prepareData($data);

        $attributes = [];
        foreach($data as $key => $value){
            $attributes[] = ["invoice_id" => $invoice->id,"key" => $key , "value" => $value];
        }

        if (empty($attributes))
            return $this;

        self::where("invoice_id", $invoice->id)->whereIn("key", array_keys($data))->delete();

        return $this->insert($attributes);

    }


    private function prepareData($data){

        return $data;
    }
}
