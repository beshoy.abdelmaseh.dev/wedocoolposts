<?php

namespace App\Models\Invoice\Transaction;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Meta extends Model
{
    use Cachable;


    protected $guarded = [];

    protected $table = "transaction_metas";

    public function invoice()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function sync(Transaction $transaction, $data)
    {

        $data = $this->prepareData($data);

        self::where("transaction_id",$transaction->id)->delete();

        $attributes = [];
        foreach($data as $key => $value){
            $attributes[] = ["transaction_id" => $transaction->id,"key" => $key , "value" => $value];
        }

        return $this->insert($attributes);
    }


    private function prepareData($data){

        $data = Arr::add($data,"trans_id",Arr::get($data,"id"));
        $data = Arr::add($data,"order_id",optional(Arr::get($data,"order"))["id"]);
        $data = Arr::add($data,"process_data_message",optional(Arr::get($data,"data"))["message"]);

        return Arr::only($data,[
            "trans_id",
            "pending",
            "amount_cents",
            "success",
            "currency",
            "integration_id",
            "order_id",
            "main_type",
            "process_data_message"
        ]);
    }
}
