<?php

namespace App\Models\Invoice\Transaction;

use App\Casts\State;
use App\Models\Invoice\Invoice;
use App\Models\User\User;
use App\Traits\Model\HasMetas;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes,HasMetas;

    protected $with = ["metas"];

    protected $meta = Meta::class;

    protected $guarded = [];

    protected $dates = [
        'approved_at',
    ];

    const STATES = [0 => "pending", 1 => "processing" , 2 => "paid" , 3 => "cancelled" , 4 => "refunded" , 5 => "expired", 6 => "error"];

    protected $casts = [
        "state" => State::class.':'.self::class
    ];

    public function approvedBy()
    {
        return $this->belongsTo(User::class,"approved_by");
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }
}
