<?php

namespace App\Models\Invoice;


use App\Casts\State;
use App\Models\Coupon;
use App\Models\Invoice\Transaction\Transaction;
use App\Models\Order\Order;
use App\Models\User\User;
use App\Traits\Model\HasCreatedBy;
use App\Traits\Model\HasMetas;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes,HasCreatedBy,HasMetas;

    protected $with = ["metas"];

    protected $meta = Meta::class;

    protected $guarded = [];

    const STATES = [0 => "pending" , 1 => "full-paid" , 2 => "cancelled" , 3 => "refunded" , 4 => "expired", 5 => "half-paid"];

    protected $casts = [
        "state" => State::class.':'.self::class
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class)->latest();
    }

    public function getHalfAmountAttribute()
    {
        if($this->amount > 0){
            return round(($this->amount/2),2);
        }
        return 0;
    }

    public function getAmountAttribute($value)
    {
        $amount = $value - $this->discount;
        if($amount > 0)
            return $amount;

        return 0;
    }

    public function getDiscountAttribute()
    {
        if($this->coupon && $discount = $this->getMeta("discounted_amount",0)) {
            return $discount;
        }
        return 0;
    }

    public function getAmountToPayAttribute()
    {
         if(in_array($this->state ,["pending","cancelled"])){
            return $this->half_amount;
        }
        if(in_array($this->state ,["half-paid"])){
            return $this->half_amount;
        }
        return 0;
    }
}
