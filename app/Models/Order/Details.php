<?php

namespace App\Models\Order;

use App\Models\Order\Action\Action;
use App\Models\Question\Question;
use App\Services\QuestionService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Details extends Model
{
    use SoftDeletes;

    protected $table = "order_details";

    protected $guarded = [];


    protected $casts = [
        'answer_value' => 'collection',
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class)->withTrashed();
    }

    public function getAnswerValue($loadedQuestions)
    {
        //new method , better performance

        if(is_null($loadedQuestions))
            return null;

        $value = $this->answer_value;

       $answerValue = $value;

        if($value && $value->isNotEmpty()){
            $getVal = $value->count() > 1 ? $value->toJson() : $value->first();
            $answerValue = $getVal;
            if($questions = QuestionService::getQuestionbyId($getVal,true,$loadedQuestions)){
                $replaceAnswerValue = $answerValue;
                for($i = 0; $i< count($questions); $i++) {
                    if($questions[$i]["model"])
                        $replaceAnswerValue = (str_replace($questions[$i]["value"], $questions[$i]["model"]->getMeta("title_in_order", false) ?: $questions[$i]["model"]->title, $replaceAnswerValue));
                }
                $answerValue = $replaceAnswerValue;
            }

        }
        return $answerValue;
    }

    public function getValueAttribute_Disabled()
    {
        //old method
        $value = $this->answer_value;

        $answerValue = $value;
        if($value && $value->isNotEmpty()){
            $getVal = $value->count() > 1 ? $value->toJson() : $value->first();
            $answerValue = $getVal;
            if($questions = QuestionService::getQuestionbyId($getVal,true)){
                $replaceAnswerValue = $answerValue;
                for($i = 0; $i< count($questions); $i++) {
                    if($questions[$i]["model"])
                    $replaceAnswerValue = (str_replace($questions[$i]["value"], $questions[$i]["model"]->getMeta("title_in_order", false) ?: $questions[$i]["model"]->title, $replaceAnswerValue));
                }
                $answerValue = $replaceAnswerValue;
            }

        }

        return $answerValue;
    }


    public function getPriceAttribute($value)
    {

        if($value > 0){
            return $value." ".currency_locale();
        }
        return free_locale();
    }

    public function getTitleInOrderAttribute()
    {
         $title = $this->question->getMeta("title_in_order");

        return str_replace("(answer_value)",$this->value,$title);
    }

    public function actions()
    {
        return $this->morphMany(Action::class,"model");
    }
}
