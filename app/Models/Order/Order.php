<?php

namespace App\Models\Order;

use App\Casts\State;
use App\Models\Invoice\Invoice;
use App\Models\Order\Action\Action;
use App\Models\Question\Question;
use App\Models\User\User;
use App\Traits\Model\HasCreatedBy;
use App\Traits\Model\HasMetas;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravelista\Comments\Commentable;

class Order extends Model
{
    use SoftDeletes,HasCreatedBy,HasMetas, Commentable;

    protected $guarded = [];

    protected $with = ["metas","details.question"];

    protected $meta = Meta::class;

    const STATES = [0 => "pending" , 1 => "active"];

    protected $casts = [
        "state" => State::class.':'.self::class
    ];

    public function details()
    {
        return $this->hasMany(Details::class);
    }

    public function getTotalPriceAttribute()
    {
        return $this->details->sum("price");
    }
    public function getMainQuestionAttribute()
    {
        if(!$this->details)
            return null;
        return optional($this->details->where("answer_type","main_question")->first())->question->title;
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class);
    }


    public function loadQuestions($answers,$withTrashed = false)
    {
        $questions = null;
        if(!is_null($answers) && $answers && !empty($answers)){
            if($answers->isNotEmpty()){
                $questionsIds = [];
                $answers->each(function($answer) use (&$questionsIds){

                    if($answer && !empty($answer)){
                        $answer->each(function($value) use (&$questionsIds){

                            if($value && !is_array($value) && str_contains($value,"question_id_")) {

                                preg_match_all("/question_id_(\d+)/", $value, $result);
                                if (!empty($result) && isset($result[0], $result[1])) {
                                    $questionsIds[] = $result[1][0] ?? 0;
                                }

                            }

                        });
                    }

                });
               if(is_array($questionsIds) && !empty($questionsIds)){
                   //->load("translations")
                   $questions = ($withTrashed ? Question::withTrashed()->find($questionsIds) : Question::find($questionsIds));
               }
            }
        }
     return $questions;
    }

    public function action()
    {
        return $this->morphOne(Action::class,"model")->withDefault([
            "status" =>  (object)[
                "class" => 'badge badge-pill badge-light',
                "name" => "-",
                "key" => null,
            ],
        ]);
    }
}
