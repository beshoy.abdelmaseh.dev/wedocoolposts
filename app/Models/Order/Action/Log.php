<?php

namespace App\Models\Order\Action;

use App\Casts\State;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    const STATES = [];

    protected $casts = [
        "state" => State::class.':'.self::class,
        "data" => "collection"
    ];

}
