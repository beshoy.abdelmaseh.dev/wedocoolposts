<?php

namespace App\Models\Order\Action;


use App\Casts\State;
use App\Models\Material;
use App\Models\Order\Action\Meta;
use App\Models\Type;
use App\Models\User\User;
use App\Traits\Model\HasMetas;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Action extends Model
{
    use SoftDeletes, HasMetas;

    protected $guarded = [];

    protected $with = ["metas","status"];

    protected $meta = Meta::class;

    const STATES = [0 => "pending" , 1 => "approved" , 2 => "declined"];

    protected $casts = [
        "data" => "collection",
        "approval" => State::class.':'.self::class
    ];

    protected $attributes = [
        "data" => "",
    ];

    public function model()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function logs()
    {
        return $this->hasMany(Log::class)->latest();
    }

    public function status()
    {
        return $this->belongsTo(Type::class,"state_id")->withDefault([
           "class" => 'badge badge-pill badge-light',
            "name" => "-",
            "key" => null,
        ]);;
    }

    public function materials()
    {
        return $this->morphMany(Material::class,"model")->orderBy("created_at");
    }

}
