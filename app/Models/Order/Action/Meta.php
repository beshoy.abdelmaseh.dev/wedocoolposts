<?php

namespace App\Models\Order\Action;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $guarded = [];

    protected $table = "action_metas";

    public $timestamps = false;

    public function action()
    {
        return $this->belongsTo(Action::class);
    }

    public function sync(Action $action, $data)
    {

        $data = $this->prepareData($data);

        $attributes = [];
        foreach($data as $key => $value){
            $attributes[] = ["action_id" => $action->id,"key" => $key , "value" => $value];
        }

        if (empty($attributes))
            return $this;

        self::where("action_id", $action->id)->whereIn("key", array_keys($data))->delete();

        return $this->insert($attributes);
    }


    private function prepareData($data){

        return $data;
    }
}
