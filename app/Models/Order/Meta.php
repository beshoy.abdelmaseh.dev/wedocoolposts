<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
   protected $guarded = [];

    protected $table = "order_metas";


    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function sync(Order $order, $data)
    {

        $data = $this->prepareData($data);

        self::where("order_id",$order->id)->delete();

        $attributes = [];
        foreach($data as $key => $value){
            $attributes[] = ["order_id" => $order->id,"key" => $key , "value" => $value];
        }

        return $this->insert($attributes);
    }


    private function prepareData($data){

        return $data;
    }}
