<?php

namespace App\Models\User;

use App\Models\Invoice\Invoice;
use App\Models\Order\Order;
use App\Models\Type;
use App\Traits\Model\HasMetas;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

use Laravelista\Comments\Commenter;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use SoftDeletes,HasRoles, HasFactory, Notifiable,HasMetas, Commenter, Notifiable;


    protected $with = ["metas"];

    protected $meta = Meta::class;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [ ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function getFirstNameAttribute()
    {
        return Str::before($this->full_name," ");
    }
    public function getLastNameAttribute()
    {
        return Str::afterLast($this->full_name," ");
    }

    public function businessType()
    {
        return  Type::query()->where("id",$this->getMeta("type_id",0))->first();
    }
}
