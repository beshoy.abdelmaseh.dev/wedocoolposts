<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    protected $guarded = [];

    protected $table = "user_metas";

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sync(User $user, $data)
    {

        $data = $this->prepareData($data);

        $attributes = [];
        foreach($data as $key => $value){
            $attributes[] = ["user_id" => $user->id,"key" => $key , "value" => $value];
        }

        if (empty($attributes))
            return $this;

        self::where("user_id", $user->id)->whereIn("key", array_keys($data))->delete();

        return $this->insert($attributes);
    }


    private function prepareData($data){

        return $data;
    }
}
