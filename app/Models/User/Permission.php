<?php

namespace App\Models\User;


/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 */
class Permission extends \Spatie\Permission\Models\Permission
{
	protected $fillable = ["name", "label", "guard_name", "desc"];

}
