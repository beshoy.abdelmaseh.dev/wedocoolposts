<?php

namespace App\Models\Question;

use App\Casts\State;
use App\Traits\Model\HasCreatedBy;
use App\Traits\Model\HasMetas;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
class Question extends Model implements TranslatableContract
{
    use SoftDeletes,HasCreatedBy,HasMetas,Translatable;

    protected $fillable = ["main","title","key","desc","label","type","sub_type","price","data","validation","parent_id","state","sorting","group_id"];

    public $translatedAttributes = ['title', 'label' , "desc"];

    public $translationModel = Translation::class;

    protected $with = ["metas","translations"];

    protected $meta = Meta::class;

    const STATES = [0 => "active" , 1 => "deactivated"];

    protected $casts = [
        'validation' => 'collection',
        "state" => State::class.':'.self::class
    ];

    public function scopeIsMain(Builder $builder)
    {
        $builder->where('main',  1);
    }
    public function scopeIsActive(Builder $builder)
    {
        $builder->where('state',  "active");
    }
    public function parent()
    {
        return $this->belongsTo(self::class,"parent_id");
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id','id')->with(['children','parent']);
    }

    public function getPriceAttribute($value)
    {
        if($value > 0){
            return $value." ".currency_locale();
        }
        return free_locale();
    }

    public function countSteps()
    {
        return $this->where("parent_id",$this->id)->where("state",0)->selectRaw('count(*) as steps, group_id')->groupBy("group_id")->get()->count();
    }

}
