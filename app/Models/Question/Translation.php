<?php

namespace App\Models\Question;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    use  Cachable;

    protected $guarded = [];

    protected $table = "question_translations";

    public $timestamps = false;

    protected $casts = [
        "translatable" => "object"
    ];

}
