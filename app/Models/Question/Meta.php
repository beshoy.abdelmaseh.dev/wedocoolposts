<?php

namespace App\Models\Question;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
    use Cachable;

    protected $guarded = [];

    protected $table = "question_metas";


    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function sync(Question $question, $data)
    {

        $data = $this->prepareData($data);


        $attributes = [];
        foreach($data as $key => $value){
            $attributes[] = ["question_id" => $question->id,"key" => $key , "value" => $value];
        }

        if (empty($attributes))
            return $this;

        self::where("question_id", $question->id)->whereIn("key", array_keys($data))->delete();

        return $this->insert($attributes);
    }


    private function prepareData($data){

        return $data;
    }
}
