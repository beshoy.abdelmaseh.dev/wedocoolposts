<?php

namespace App\Models;

use App\Casts\State;
use App\Models\Invoice\Invoice;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    const STATES = [0 => "active" , 1 => "deactivated"];

    protected $casts = [
        "state" => State::class.':'.self::class,
        "start_date" => "datetime",
        "end_date" => "datetime",
        "user_id" => "collection",
        "translatable" => "object"
    ];

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

}
