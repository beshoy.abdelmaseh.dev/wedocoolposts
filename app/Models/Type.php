<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        "translatable" => "object"
    ];

    static public function getKeyById($id = null)
    {
        if(is_null($id))
            return null;

        return self::findOrFail($id)->key;
    }

    public function getNameAttribute($name)
    {
        if(\LaravelLocalization::getCurrentLocale() == "en")
            return $name;

        return optional($this->translatable)->name ?: $name;
    }

    public function getDescAttribute($desc)
    {
        if(\LaravelLocalization::getCurrentLocale() == "en")
            return $desc;

        return optional($this->translatable)->desc ?: $desc;
    }
    public static function getIdByKey($key)
    {
        if(is_null($key))
            return null;
        return self::where("key",$key)->firstOrFail()->id;
    }
}
