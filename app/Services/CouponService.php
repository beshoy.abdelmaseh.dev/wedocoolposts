<?php


namespace App\Services;


use App\Models\Coupon;
use App\Models\Invoice\Invoice;

class CouponService
{

    private $coupon;
    private $discounted_amount = 0;
    private $invoice;
    private $user;

    public function __construct($invoice, $user, $coupon_code)
    {
        $this->coupon = Coupon::query()->where("code", $coupon_code)->firstOrFail();
        $this->invoice = $invoice;
        $this->user = $user;

        //no errors
        if (self::validate($invoice, $user, $coupon_code) === false) {
            $this->apply();
        }

    }

    public function getDiscountAmount()
    {
        if ($this->discounted_amount) {
            return number_format($this->discounted_amount,2);
        }
        return 0;
    }

    public function getCouponCode()
    {
        return $this->coupon->code;
    }

    public function getCouponModel()
    {
        return $this->coupon;
    }

    public static function validate($invoice, $user, $code)
    {
        $coupon = Coupon::query()->where("code", $code)->first();
        if (!$coupon) {
            return trans("validation.custom.coupon.invalid");
        }
        if ($coupon->start_date > now() || $coupon->end_date < now()) {
            return trans("validation.custom.coupon.expired");
        }
        if ($coupon->minimum_amount > $invoice->getRawOriginal("amount")) {
            return trans("validation.custom.coupon.min_amount",["minimum_amount" => number_format($coupon->minimum_amount,2,".",'')]);
        }
        if ($coupon->state != "active") {
            return trans("validation.custom.coupon.not_active");
        }
        if ($coupon->max_usage && $coupon->max_usage <= Invoice::where("coupon_id",$coupon->id)->count()) {
            return trans("validation.custom.coupon.max_usage");
        }
        if (optional($coupon->user_id)->isNotEmpty()) {
            if (!$coupon->user_id->contains(optional($user)->id)) {

                return trans("validation.custom.coupon.user",["message" => ((auth()->check() ? __("not for you") : __("please sign in")).".")]);
            }
        }

        return false;
    }

    private function apply()
    {
        // $this->invoice->getRawOriginal("amount")
        // $this->coupon
        $this->calculateDiscount($this->invoice->getRawOriginal("amount"));
    }

    private function calculateDiscount($total_amount)
    {

        $discount = 0;

        switch ($this->coupon->type) {  //coupon type

            case 'percentage': //percentage type

                //percent calculation
                if ($total_amount > 0) {
                    $discount = ($total_amount / 100) * $this->coupon->amount;
                }
                break;
            case 'fixed': //fixed type

                if ($total_amount > 0) {
                    $discount = $this->coupon->amount;
                }
                break;
        }
        //set discounted amount
        $this->discounted_amount = round($discount, 2);
    }
}
