<?php


namespace App\Services;


use App\Models\Question\Question;

class QuestionService
{
    private $byNamespace;
    private $localUploadService = null;
    /**
     * @var Question
     */
    private $question;

    function __construct(Question $question)
    {

        $this->question = $question;
    }

    public static function getQuestionbyId($value,$withTrashed = false,$loadedQuestions = null)
    {
        $questions  = null;

        if($value && !is_array($value) && str_contains($value,"question_id_")) {
            preg_match_all("/question_id_(\d+)/", $value, $result);
            if (!empty($result) && isset($result[0], $result[1])) {
                if ($findQuestions = ($loadedQuestions ?: ($withTrashed ? Question::withTrashed()->find($result[1])->load("translation") : Question::find($result[1])->load("translation")))) {
                    for($i=0; $i<count($result[1]); $i++) {
                        $questions[] = ["value" => $result[0][$i] , "model" => $findQuestions->where("id",$result[1][$i])->first()];
                    }
                }
            }
        }

        return $questions;
    }
    public static function getValidateRules($rules, $question, $group_id = null)
    {

        if ($question) {
            foreach ($question as $getQuestion) {
                if (!empty($getQuestion->validation) && $getQuestion->group_id == $group_id) {
                    $validationKey = optional(optional($getQuestion->validation)->keys())->first();
                    if (empty($validationKey)) {
                        $validationKey = "(key).(type).*";
                    }
                    $key = str_replace(
                        ["(key)", "(type)", "(id)","(parent_key)", "(parent_type)", "(parent_id)"],
                        [$getQuestion->key, $getQuestion->type ?: "none", $getQuestion->id,optional($getQuestion->parent)->key, optional($getQuestion->parent)->type ?: "none", optional($getQuestion->parent)->id],
                        $validationKey);
                    $rules->put( "question." . $key, ["rules" => optional($getQuestion->validation)->first() , "title" => $getQuestion->title]);
                }
                if ($getQuestion->relationLoaded("children") && $getQuestion->children) {
                    self::getValidateRules($rules, $getQuestion->children, $group_id);
                }
            }
        }

        return $rules;
    }
}
