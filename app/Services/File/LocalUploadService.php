<?php

namespace App\Services\File;


use App\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;


class LocalUploadService
{

	const ATTACHMENT_PATH = "public/";
	const IMAGE_PATH = "images/";

	private $byNamespace, $storage_code;

	public $file = [];
	private $request, $fullPath, $defaultKey;


	public function setFile($keys)
	{
		if (!is_array($keys))
			$this->defaultKey = $keys;

		foreach ((array)$keys as $key) {
			if ($this->request->hasFile($key) && isset($this->data[$key])) {
				$this->setUploadedFile($this->request->file($key), $key);
			}
		}

		return $this;
	}

	public function setUploadedFile(UploadedFile $file, $key)
	{
		$this->file[$key] = $file;

		return $this;
	}


	public function validate($rules)
	{
		$this->request = request();

		$this->data = $this->request->validate($rules);

		return $this;
	}

	public function setByNamespace($namespace)
	{
		$this->byNamespace = $namespace;
		return $this;
	}

	public function isNamespace($namespace)
	{
		return $this->byNamespace == $namespace;
	}

	public function generateStorageCode($key)
	{
		if (!isset($this->file[$key]))
			return null;

		return $this->storage_code[$key] = $this->file[$key]->hashName();
	}

	public function getStorageCode($key = null)
	{
		if (is_null($key))
			$key = $this->defaultKey;

		return isset($this->file[$key]) ? $this->storage_code[$key] : null;
	}

	public function getFullPath($key = null)
	{
		if (is_null($key))
			$key = $this->defaultKey;

		return isset($this->file[$key]) ? $this->fullPath[$key] : null;
	}

	public function save($path, $key = null)
	{
		if (is_null($key))
			$key = $this->defaultKey;

		if (isset($this->file[$key])) {
			$this->generateStorageCode($key);

			$this->fullPath[$key] = $path . "/" . $this->storage_code[$key];

			$this->file[$key]->storeAs($path, $this->storage_code[$key]);
		}
		return $this;
	}

	public function exists($key = null)
	{
		if (is_null($key))
			$key = $this->defaultKey;

		if (isset($this->file[$key])) {
			if (!empty($this->file[$key]))
				return true;
		}

		return false;
	}

	public function store($key = null)
	{
		if (is_null($key))
			$key = $this->defaultKey;

		if (isset($this->file[$key])) {
			$file = $this->file[$key];
			return [
				"name" => $file->getClientOriginalName(),
				"slug" => uniqid(), //not coded yet
				"extension" => $file->getClientOriginalExtension(),
				"storage_path" => $this->fullPath[$key],
				"size" => $file->getSize(),
			];
		}
		return [];
	}

}
