<?php


namespace App\Services\File;


use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ImageService
{

    public static function addWatermark($filePath)
    {
        $img = Image::make($filePath);

        $watermark = Image::make(public_path("assets/images/watermark.png"));
        //$watermarkSize = $img->width() - 20; //size of the image minus 20 margins

        //  $watermarkSize = $img->width() / 2; //half of the image size

        $resizePercentage = 40;//70% less then an actual image (play with this value)
        $watermarkSize = round($img->width() * ((100 - $resizePercentage) / 100), 2); //watermark will be $resizePercentage less then the actual width of the image

        $watermark->resize($watermarkSize, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $img->insert($watermark, 'center');

        $storage_code = "materials-watermark/" . Str::random(30).time().".".$img->extension;
        $img->save(storage_path("app/") . $storage_code);

        return $storage_code;
    }
}
