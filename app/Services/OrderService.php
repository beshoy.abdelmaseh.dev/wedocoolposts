<?php

namespace App\Services;

use App\Http\Requests\Web\StoreOrderFormRequest;
use App\Models\Order\Details;
use App\Models\Order\Order;
use App\Models\Question\Question;
use App\Models\Type;
use App\Models\User\User;
use App\Notifications\NewOrder;
use Carbon\Carbon;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Database\QueryException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Yajra\DataTables\DataTables;


class OrderService
{

    const ATTACHMENT_PATH = "public/";
    const IMAGE_PATH = "images/";

    private $order;

    private $byNamespace;
    private $localUploadService = null;

    function __construct(Order $order)
    {

        $this->order = $order;
    }


    public function setByNamespace($namespace)
    {
        $this->byNamespace = $namespace;
        return $this;
    }

    public function isNamespace($namespace)
    {
        return $this->byNamespace == $namespace;
    }

    public function handleDataTable()
    {

    }

    public function getKeyAndValueOfArrayOld($answerKey, $arr, $instance)
    {
        if (is_array($arr)) {
            $tempObj = collect(["answer_key" => $answerKey]);
            foreach ($arr as $key => $value) {
                if (is_array($value)) {
                    if($tempObj->has("answer_type")){
                        $tempObj->put("question_id", $key);
                        $tempObj->put("answer_value", json_encode($value));
                        break;
                    }
                    $tempObj->put("answer_type", $key);
                    return $this->getKeyAndValueOfArray($answerKey, $value, $instance);
                } else {
                    if (in_array($type,["radio"]) /*|| !ctype_digit((string)$key)*/) {
                       // $questionKey = !ctype_digit((string)$key) ? $key : $value;
                        $tempObj->put("answer_key", $key);
                        $question = Question::select("id")->where("key", $value)->firstOrFail();
                        $key = $question->id;
                    }
                   /* if(!$instance->has("answer_key")){
                        $answerKey = Question::select(["id","key"])->where("id", $key)->firstOrFail();
                        $instance->put("answer_key", $answerKey->key);
                    }*/
                    $tempObj->put("question_id", $key);
                    $tempObj->put("answer_value", $value);
                }
                $instance->add($tempObj->toArray());
            }
        }
    }

      public function getKeyAndValueOfArray($answerKey, $arr,$tempObj = null,$details = null)
    {

        if(is_array($arr)){
            foreach($arr as $key => $value) {
                if (count($arr) > 1) {
                    if($details && $instance = $this->getKeyAndValueOfArray($answerKey,[$key =>$value])) {
                        $details->add($instance->toArray());
                    }
                } else {
                    if (is_null($tempObj)) {
                        $tempObj = collect(["answer_key" => $answerKey]);
                    }
                    if ($tempObj->has("answer_type") && ctype_digit((string)$key)) {
                        $tempObj->put("question_id", $key);
                    }
                    if ($tempObj->has("question_id")) {
                            if (is_array($value)) {
                                $tempObj->put("answer_value", $value);
                            } else {
                                $tempObj->put("answer_value", [$value]);
                            }
                        break;
                    }
                    if (!$tempObj->has("answer_type")) {
                        $tempObj->put("answer_type", $key);
                    }

                    $this->getKeyAndValueOfArray($answerKey, $value, $tempObj);
                    // $multi->add($tempObj);
                }
            }
        }
        return $tempObj;

    }

    private function handleValidatedData($validatedData)
    {
        $validatedData = collect($validatedData);
        $details = collect();
        foreach ($validatedData->get("question") as $answerKey => $arr) {

          /*  $instance = collect();

            $this->getKeyAndValueOfArray($answerKey, $arr, $instance);

            $details->add($instance->toArray());*/

            if($instance = $this->getKeyAndValueOfArray($answerKey, $arr,null,$details)) {
                $details->add($instance->toArray());
            }
        }

      /*  $finalResults = collect();
        foreach($details as $detail){
            if(is_array($detail)){
                foreach($detail as $value){
                    $finalResults->add($value);
                }
            }else{
                $finalResults->add($detail);
            }
        }

        abort(422,json_encode($finalResults));*/

        return $details;
    }

    public function handleCreate($step_number, Question $mainQuestion, array $data): array
    {

        if ($this->isNamespace("dashboard")) {

        }

        if ($step_number >= ($mainQuestion->countSteps() - 1) || $mainQuestion->getMeta("form") == "default") {

            $orderDetails = $this->handleValidatedData($data);

            if ($orderDetails->isEmpty()) {
                throw  new \Exception("Order details is empty or unexpected error.");
            }

            $questionIds = $orderDetails->pluck("question_id")->unique();

            $questions = Question::select(["id","key","price"])->whereIn("id",$questionIds)->get();

            if($questions->count() != $questionIds->count()){
                throw  new \Exception("Unexpected error in order details.");
            }

            try {

            \DB::beginTransaction();

            $order = $this->order->create([
                "user_id" => optional(auth()->user())->id,
                "state" => "pending"
            ]);

           $users  = User::role(['developer','admin'])->get();
             if($users->isNotEmpty()) {
                 Notification::send($users, new NewOrder($order));
             }


            if(auth()->check()) {
                $order->action()->create([
                    "state_id" => null
                ]);
            }

            $orderDetails = $orderDetails->map(function($item) use($questions,$order){
                $item["price"] = optional($questions->firstWhere('id', $item["question_id"]))->getRawOriginal("price") ?: 0.00;


                $value = collect($item["answer_value"]);
                $getVal = $value->count() > 1 ? $value->toJson() : $value->first();
                if($questions = QuestionService::getQuestionbyId($getVal)){
                    for($i = 0; $i< count($questions); $i++) {
                        $item["price"] += $questions[$i]["model"]->getRawOriginal("price") ?: 0.00;
                      }
                }

                /*$answerValue = optional(collect($item["answer_value"]))->first();
               if($answerValue && !is_array($answerValue) && str_contains($answerValue,"question_id_")) {
                    preg_match("/question_id_(\d+)/", $answerValue, $result);
                    if (!empty($result) && isset($result[0], $result[1])) {
                        if ($getAnswerValueQuestion = Question::find($result[1])) {
                            $item["price"] += $getAnswerValueQuestion->price ?: 0.00;
                        }
                    }
                }*/

                // $item["order_id"] = $order->id;
                $item["answer_key"] = $item["answer_key"] ?? (optional($questions->firstWhere('id', $item["question_id"]))->key ?: 0.00);
                return $item;
            });

            $orderDetails->prepend(["order_id" =>$order->id ,"question_id" => $mainQuestion->id,"price" => $mainQuestion->getRawOriginal("price"), "answer_type" => "main_question" , "answer_key" => null , "answer_value" => null]);

            $saveOrderDetails = [];
            foreach($orderDetails->toArray() as $item){
                $saveOrderDetails[] = new Details($item);
            }

            $order->details()->saveMany($saveOrderDetails);

            $invoiceSlug =  rand(333, 999)."-" .rand(222, 888);



            Cart::destroy();
            Cart::add($invoiceSlug, $mainQuestion->key, 1, $orderDetails->sum("price"), 0);

            $order->invoice()->create([
                "slug" => $invoiceSlug,
                "state" => "pending",
                "amount" => Cart::total(2, ".", false),
                "user_id" => optional(auth()->user())->id,
            ]);

            $redirectTo = route("invoices.show", $invoiceSlug);

            \DB::commit();

            return ["redirectTo" => $redirectTo];

            } catch (\Exception $e) {
                \DB::rollback();
                throw  new \Exception($e->getMessage());
            }
        }

        return ["form_wizard" => "next"];

    }

    public function handleUpdate(array $data)
    {

        $order = $this->order;


        return $order;
    }

    public function handleDelete()
    {
        $getNamespace = $this->byNamespace;
        $order = $this->order;


        $order->delete();
    }

    public static function notify($order)
    {
        if(settings("admin_email")) {
            Notification::route('mail', settings("admin_email"))
                ->notify(new NewOrder($order,["mail"]));
        }
    }

}
