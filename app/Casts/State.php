<?php


namespace App\Casts;


use Illuminate\Contracts\Database\Eloquent\CastsAttributes;
use InvalidArgumentException;

class State implements CastsAttributes
{

    protected $states = [];

    public function __construct($model)
    {
        if (defined($model . '::STATES') && is_array($model::STATES) && !empty($model::STATES)) {
            $this->states = $model::STATES;
        }
    }

    public function get($model, $key, $value, $attributes)
    {
        return $this->states[$value] ?? $value;
    }

    public function set($model, $key, $value, $attributes)
    {
        if(ctype_digit((string)$value) && array_key_exists($value, $this->states))
            return $value;

        if (!in_array($value, $this->states)) {
            throw new InvalidArgumentException('The given value is not in states array of '.get_class ($model).'::STATE');
        }

        return array_search($value, $this->states);
    }
}
