<?php

namespace App\Providers;

use App\Repositories\SettingRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);

        if(env('APP_DEBUG')) {

            DB::listen(function ($query) {
                $queryLog = new Logger('dbQurery');
                $queryLog->pushHandler(new StreamHandler(storage_path('logs/db-queries.log')), Logger::INFO);
                $queryLog->info($query->sql, $query->bindings, $query->bindings);
            });

            /*  arabic faker
            $this->app->singleton(\Faker\Generator::class, function () {
                 return \Faker\Factory::create('ar_JO');
             });
            */

        }

        $this->app->singleton('SettingRepository', function ($app) {
            return new SettingRepository;
        });

        if (app()->environment('local')) {
            $this->app->register('App\Providers\TelescopeServiceProvider');
        }

        if (app()->environment('production')) {
            $this->app->bind('path.public', function () {
                return realpath(base_path() . '/../public_html');
            });
        }
        foreach (glob(app_path() . '/Helpers/*.php') as $file) {
            require_once($file);
        }

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (app()->environment('production') && env("FORCE_SSL"))
            \URL::forceScheme('https');
    }
}
