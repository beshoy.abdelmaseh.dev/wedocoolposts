<?php

namespace App\Providers;

use App\View\Components\QuestionComponent;
use App\View\Components\QuestionFormComponent;
use App\View\Components\QuestionTableComponent;
use App\View\Components\ShowActionsComponent;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class ComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('x-question-component',QuestionComponent::class);
        Blade::component('x-question-form-component',QuestionFormComponent::class);
        Blade::component('x-question-table-component',QuestionTableComponent::class);
        Blade::component('x-show-actions-component',ShowActionsComponent::class);
    }
}
