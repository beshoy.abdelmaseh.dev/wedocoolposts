<?php

namespace App\Providers;


use App\View\Composers\TypesComposer;
use App\View\Composers\MainQuestionsComposer;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer("web.pages.orders.main-questions",MainQuestionsComposer::class);

        View::composer("partials.types.*",TypesComposer::class);
    }
}
