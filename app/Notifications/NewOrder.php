<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class NewOrder extends Notification
{
    use Queueable;

    private $order;
    /**
     * @var array
     */
    private $channels = [];

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order,$channels = [])
    {
        //
        $this->order = $order;
        $this->channels = $channels;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return array_merge(['database'],$this->channels);
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("New Order [{$this->order->main_question}]")
                    ->greeting("Hello!")
                    ->line("A new order has been created by,")
                    ->line(("Full Name: <span style='font-weight: bold'>{$this->order->user->full_name}</span>."))
                     ->line("Phone: <span style='font-weight: bold'>".$this->order->user->phone."</span>.")
                    ->line("Email Address: <span style='font-weight: bold'>{$this->order->user->email}</span>.")
                     ->action('Check Order', route("admin.orders.manage",$this->order->invoice->slug));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "order_id" => $this->order->id
        ];
    }
}
