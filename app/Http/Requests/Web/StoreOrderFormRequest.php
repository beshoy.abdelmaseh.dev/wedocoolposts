<?php

namespace App\Http\Requests\Web;

use App\Services\QuestionService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Cache;

class StoreOrderFormRequest extends FormRequest
{

    private $customAttributes = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function attributes()
    {
        return $this->customAttributes;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [];

        $form_wizard_step_number = $this->input("form_wizard_step_number",0);

        $mainQuestion = optional($this->route()->parameters)["question"];

        if($mainQuestion) {

            $mainQuestion = Cache::rememberForever("main-question-{$mainQuestion->id}", function () use($mainQuestion)  {
                return $mainQuestion->load(["children" => function($query){
                    return  $query->isActive();
                }]);
            });


            $countSteps = 1;
            if ($mainQuestion->getMeta("form") == "wizard") {
                $countSteps = $mainQuestion->countSteps();
            }

            $dbRules = [];
            $getCustomAttributes = [];
            for($i = 0; $i< $countSteps; $i++) {
                 $data = QuestionService::getValidateRules(collect(), $mainQuestion->children, $i)->filter(function($item){
                    return (!empty($item["rules"] ?? null) && !is_null($item["rules"] ?? null));
                });

               $dbRules[$i] = $data->mapWithKeys(function($item,$key) {
                    return [$key => $item["rules"] ?? []];
                })->toArray();

                 $data->each(function($item,$key) use(&$getCustomAttributes) {
                    if(isset($item["title"]) && !empty($item["title"]))
                        $getCustomAttributes[$key] =  $item["title"];
                   });
            }
            $this->customAttributes = $getCustomAttributes;

            if ($mainQuestion->getMeta("form") == "default") {
                return $dbRules[$form_wizard_step_number] ?? [];
            }

            if ($form_wizard_step_number >= (count($dbRules) - 1))
                foreach ($dbRules as $initializeRule)
                    $rules = array_merge($rules, $initializeRule);
            else
                $rules = $dbRules[$form_wizard_step_number] ?? [];

        }

        return $rules;
    }
}
