<?php

namespace App\Http\Requests\Web;

use App\Rules\CurrentPassword;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "info" => [
                "full_name" => "required|min:2|max:40",
                "type_id" => "sometimes"
            ],
            "password" => [
                "current_password" => ["required", new CurrentPassword],
                "password" => "required|confirmed|min:8|max:20",
            ],
            "links" => [
                "links.*" => "sometimes|string"
            ]
        ];
        return $rules[$this->page];
    }
}
