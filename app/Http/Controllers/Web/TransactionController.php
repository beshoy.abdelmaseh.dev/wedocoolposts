<?php

namespace App\Http\Controllers\Web;

use App\Billing\PaymentGateway;
use App\Http\Controllers\Controller;
use App\Models\Invoice\Invoice;
use App\Models\Invoice\Transaction\Transaction;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller
{

    public function processCallback(Request $request, $method, $gateway, PaymentGateway $paymentGateway)
    {
        if (!in_array($method, ["credit-card"]) || !in_array($gateway, ["paymobsolutions"])) {
            Log::error("processCallback: Invalid parameters in process callback.");
            abort(500, "Invalid parameters in process callback.");
        }

        $paymentGateway->setPaymentMethod($method);

        $data = collect($paymentGateway->storeTransactionData($request));

        if ($data->isNotEmpty()) {
            if ($obj = $data->get("obj")) {
                $referenceId = isset($obj["order"]) ? ($obj["order"]["id"] ?? null) : null;
                $transaction = Transaction::query()->whereNotNull("payment_reference_id")->where([
                    "payment_reference_id" => $referenceId,
                    "state" => array_search("pending", Transaction::STATES),
                    "payment_method" => $method,
                    "payment_provider" => $gateway
                ])->first();

                if ($transaction) {

                    $transaction->update([
                        "state" => "processing",
                        "amount" => isset($obj["amount_cents"]) ? ($obj["amount_cents"] / 100) : 0.00,
                    ]);


                    $obj["main_type"] = $data->get("type");
                    $transaction->createMetas($obj);

                    $transaction->metas()->insert([
                        "transaction_id" => $transaction->id,
                        "key" => "processCallback",
                        "value" => json_encode($data->toArray())
                    ]);
                }
            }
        }

        return response()->json(["success" => true]);
    }

    public function responseCallback(Request $request, $method, $gateway, PaymentGateway $paymentGateway)
    {

        if (!in_array($method, ["credit-card"]) || !in_array($gateway, ["paymobsolutions"])) {
            Log::error("responseCallback: Invalid parameters in response callback.");
            abort(500, "Invalid parameters in response callback.");
        }

        $data = json_encode($request->all());

        $referenceId = $request->get("order");

        $transaction = retry(5, function () use ($referenceId, $method, $gateway) {
            return Transaction::query()->with("invoice")->whereNotNull("payment_reference_id")->where([
                "payment_reference_id" => $referenceId,
                "state" => array_search("processing", Transaction::STATES),
                "payment_method" => $method,
                "payment_provider" => $gateway
            ])->firstOrFail();

        }, 1500);

        if (is_null($transaction->payment_status)) {

            $paymentGateway->setPaymentMethod($method);

            $transaction->metas()->insert([
                [
                    "transaction_id" => $transaction->id,
                    "key" => "responseCallback",
                    "value" => $data
                ],
                [
                    "transaction_id" => $transaction->id,
                    "key" => "response_data_message",
                    "value" => $request->get("data_message")
                ]
            ]);

            $alert_message = [];

            if ($paymentGateway->validatePayment($request, $transaction)) {

                $transaction->update([
                    "state" => "paid",
                    "payment_status" => "completed",
                ]);

                $state = "half-paid";
                if($transaction->invoice->state == "half-paid"){
                    $state = "full-paid";
                }
                $transaction->invoice()->update([
                    "state" => array_search($state, Invoice::STATES),
                    "user_id" => optional(auth()->user())->id
                ]);

                if($state == "half-paid") {
                    $transaction->invoice->order->action()->update(["state_id" => optional(Type::where("key", "pending")->first())->id]);
                }

               $alert_message = [
                    "type" => "success",
                    "content" => '<div class="d-flex align-items-center"><i class="bx bx-like"></i> <span> '.trans("locale.invoice.transaction.success").' </span> </div>',
                ];

            } else {
                $transaction->update([
                    "state" => "error",
                    "payment_status" => "completed",
                ]);

                $alert_message =  [
                    "type" => "danger",
                    "content" => '<div class="d-flex align-items-center"><i class="bx bx-error"></i><span>'.trans("locale.invoice.transaction.error").' </span></div>',
                ];

                Log::error("invalid payment in responseCallback, ref id : $referenceId , " . $data);
            }

            $transaction->metas()->insert([
                "transaction_id" => $transaction->id,
                "key" => "alert_message",
                "value" => json_encode($alert_message)
            ]);
        }

        return redirect()->route("tracking.show", $transaction->invoice->slug);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Invoice\Transaction\Transaction $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Invoice\Transaction\Transaction $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Invoice\Transaction\Transaction $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Invoice\Transaction\Transaction $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction)
    {
        //
    }
}
