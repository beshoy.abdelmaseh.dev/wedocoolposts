<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\StoreOrderFormRequest;
use App\Models\Order\Order;
use App\Models\Question\Question;
use App\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('web.pages.orders.index');
    }


    public function tracking(Order $order)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(Question $question)
    {

       $question = Cache::rememberForever("main-question-{$question->id}", function () use($question)  {
                return $question->load(["children" => function($query){
                    return  $query->isActive();
                }]);
          });

        return view("web.pages.orders.create", compact("question"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderFormRequest $request, Question $question)
    {

        $question->load("children");

        $form_wizard_step_number = $request->input("form_wizard_step_number");

        $response = app(OrderService::class)->setByNamespace("web")->handleCreate($form_wizard_step_number, $question, $request->validated());

        return response()->json(array_merge(["success" => "true"], $response), Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Question\Question $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Question\Question $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Question\Question $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Question\Question $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        //
    }
}
