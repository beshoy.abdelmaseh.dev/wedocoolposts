<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Invoice\Invoice;
use App\Models\Order\Tracking\Tracking;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class TrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        return view("web.pages.tracking.search");
    }

    public function find(Request $request)
    {
        $this->validate($request,[
            "order_id" => "required|exists:invoices,slug"
        ]);

        $invoice = Invoice::where("slug",$request->input("order_id"))->firstOrFail();

        return response()->json([
            "success" => "true",
            "redirectTo" => route("tracking.show",$invoice->slug)
        ]);

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order\Tracking\Tracking  $tracking
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {

        $order = $invoice->order()->where("user_id",current_user()->id)->with(["details.actions","user","action"])->whereHas("user")->whereHas("action")->firstOrFail();

        if($alert_message = optional($invoice->transaction)->getMeta("alert_message",[],true)){
            session()->flash("alert_message",$alert_message);
            $invoice->transaction->deleteMeta("alert_message");
        }

       return view("web.pages.tracking.show",compact("order","invoice"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order\Tracking\Tracking  $tracking
     * @return \Illuminate\Http\Response
     */
    public function edit(Tracking $tracking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order\Tracking\Tracking  $tracking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tracking $tracking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order\Tracking\Tracking  $tracking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tracking $tracking)
    {
        //
    }
}
