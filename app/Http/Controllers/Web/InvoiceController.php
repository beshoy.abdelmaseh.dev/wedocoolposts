<?php

namespace App\Http\Controllers\Web;

use App\Billing\PaymentGateway;
use App\Http\Controllers\Controller;
use App\Models\Invoice\Invoice;
use App\Rules\Coupon;
use App\Services\CouponService;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function checkout(Request $request,Invoice $invoice,PaymentGateway $paymentGateway)
    {

        $paymentGateway->setPaymentMethod("credit-card");

        $amount = ($invoice->amount_to_pay * 100);
        $paymentGateway->charge($amount);

        $paymentGateway->setBillingData([
            "first_name"=> current_user()->first_name,
            "last_name"=> current_user()->last_name,
            "email"=> current_user()->email,
            "phone_number"=> current_user()->phone,
        ]);

        $paymentGateway->setItems([
            "name" => $invoice->slug,
            "amount_cents" => $amount,
            "description" => "We Do Cool Posts",
            "quantity" => "1"
        ]);

        $paymentGateway->initializePaymentUrl();

        if($paymentGateway->urlGeneratedSuccessfully()){

            $invoice->transaction()->create([
                "payment_reference_id" => $paymentGateway->referenceId(),
                "payment_provider" => $paymentGateway->paymentProvider,
                "payment_method" => $paymentGateway->method,
            ]);

            $redirectTo = $paymentGateway->redirect();

            return response()->json(["success" =>"true" , "redirectTo" => $redirectTo], Response::HTTP_OK);
        }

        return response()->json(["errors" => ["checkout_error" => "Failed to generate payment url."]],Response::HTTP_UNPROCESSABLE_ENTITY);

    }

    public function applyCoupon(Request $request,Invoice $invoice)
    {
        $data = $this->validate($request,[
            "coupon" => ["nullable" , New Coupon($invoice,current_user())]
        ]);

        if($invoice->state == "pending" && !empty($data["coupon"])) {
            $couponService = new CouponService($invoice,current_user(),$data["coupon"]);
            if($coupon = $couponService->getCouponModel()) {
                if($couponService->getDiscountAmount()) {
                    $invoice->update(["coupon_id" => $coupon->id]);
                    $invoice->syncMetas([
                        "discounted_amount" => $couponService->getDiscountAmount(),
                        "coupon" => json_encode($coupon->toArray()),
                        "coupon_code" => $coupon->code,
                    ]);
                }
            }
        }

        return response()->json([
            "success" => "true",
            "redirectTo" => redirect()->back()->getTargetUrl(),
        ]);
    }

    public function removeCoupon(Request $request,Invoice $invoice)
    {

        if($invoice->coupon && $invoice->state == "pending") {
            $invoice->update(["coupon_id" => null]);
            $invoice->deleteMetas(["discounted_amount", "coupon","coupon_code"]);
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        $invoice->load(["order","transaction"]);
        $order = $invoice->order;

        if(is_null($order)){
            abort(404);
        }

        if($order->user_id && $order->user_id != optional(current_user())->id) {
            return redirect()->route("login");
        }

        if($alert_message = optional($invoice->transaction)->getMeta("alert_message",[],true)){
            session()->flash("alert_message",$alert_message);
            $invoice->transaction->deleteMeta("alert_message");
        }

        return view("web.pages.invoices.show",compact("invoice","order"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invoice\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoice\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }
}
