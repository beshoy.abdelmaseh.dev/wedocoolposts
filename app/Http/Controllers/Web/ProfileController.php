<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\UpdateProfileFormRequest;
use App\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($page = "info")
    {
        $user = current_user();
        return view("dashboard.pages.profile.edit",compact("user","page"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileFormRequest $request, $page)
    {

        $data = $request->validated();

        $user = current_user();

        if (isset($data["password"]) && !empty($data['password']))
            $data['password'] = bcrypt($data['password']);

        $user->update(Arr::only($data,["full_name","password"]));

        if(Arr::has($data,"links")) {
            $user->syncMetas(Arr::get($data, "links"));
        }

        if(Arr::has($data,"type_id")) {
            $user->syncMetas(Arr::only($data, ["type_id"]));
        }

        session()->flash("alert_message", [
            "type" => "success",
            "content" => trans("locale.profile_updated"),
        ]);

        return response()->json([
            "success" => "true",
            "redirectTo" => redirect()->back()->getTargetUrl()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
