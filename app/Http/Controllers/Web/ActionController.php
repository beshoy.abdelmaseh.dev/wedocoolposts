<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Order\Action\Action;
use App\Models\Order\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateStatus(Request $request,Action $action)
    {
        if($request->isMethod("get")){
            return view("web.pages.actions.components.modal.update-status")->with("action", $action)->renderSections();
        }

        $data = $this->validate($request,[
            "order_id" => "required_if:approval,approved,exists:orders,id",
            "approval" => "required|in:approved,declined",
            "data" => "required_if:approval,declined", //reason
        ]);

        if($data["approval"] == "approved") {

            $order = Order::find($data["order_id"]);

            if($order->user_id != current_user()->id){
                throw new AccessDeniedHttpException();
            }

            if($order->invoice->state == "half-paid"){
                return response()->json([
                    "success" => "true",
                    "redirectTo" => route("invoices.show",$order->invoice->slug)
                ]);
            }

            session()->flash("alert_message", ["type" => "success", "content" => trans("locale.post_approved")]);
        }

        Arr::forget($data,"order_id");
        $action->update($data);

        return response()->json([
            "success" => "true",
            "redirectTo" => back()->getTargetUrl()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order\Action\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function show(Action $action)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order\Action\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function edit(Action $action)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order\Action\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Action $action)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order\Action\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function destroy(Action $action)
    {
        //
    }
}
