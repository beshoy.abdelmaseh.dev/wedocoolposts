<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Laravelista\Comments\Comment;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

        $breadcrumbs = [
            ["link" => "admin/", "name" => "Home"],["name" => "Settings"]
        ];
        return view("admin.pages.settings.edit",compact("breadcrumbs"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->validate([
            "desc" => "required",
            "desc_ar" => "nullable",
            "admin_email" => "required|email",
            "auth_general_password" => "required|string|min:6|max:20",
            "test_mode_weaccept" => "sometimes",
            "paymobsolutions_api_key" => "nullable|string",
            "paymobsolutions_integration_id" => "nullable|string",
            "paymobsolutions_iframe_link" => "nullable|string"
        ]);;

        Setting::query()->delete();

        $records = [];
        foreach ($data as $key => $value) {
            if (!empty($value))
                $records[] = ['key' => $key, 'value' => $value, 'updated_at' => now()];
        }

        Setting::insert($records);

        Cache::forget("settings");

        session()->flash("alert_message",["type" => "success","content" =>"Settings has been updated successfully."]);

        return response()->json([
            "success" => "true",
            "redirectTo" => redirect()->back()->getTargetUrl(),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
