<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order\Details;
use App\Models\Question\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions =   Question::with("children")->get();

        $breadcrumbs = [
            ["link" => "admin/", "name" => "Home"],["name" => "Questions"]
        ];

        return view("admin.pages.questions.index",compact("questions","breadcrumbs"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $questions =   Question::all();
        $breadcrumbs = [
            ["link" => "admin/", "name" => "Home"],["link" => "admin/questions", "name" => "All Questions"],["name" => "Create New Question"]
        ];

        return view("admin.pages.questions.create",compact("questions","breadcrumbs"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            "title" => "required|string",
            "key" => "required|unique:questions,key,NULL,id,deleted_at,NULL",
            "price" => "required",
        ]);


        $data = $request->except("translatable");

        $translatable = Arr::get($request->only("translatable"),"translatable",[]);

        $data["main"] = Arr::has($data,"main") ? true : false;

        $data["state"] = Arr::has($data,"state") ? "deactivated" : "active";

        $data["sub_type"] = Arr::has($data,"sub_type") ? implode(Arr::get($data, "sub_type"), "-") : null;

        if(Arr::has($data,"validation")) {
            $data["validation"] = isset($data["validation"]["key"]) && !empty($data["validation"]["key"]) ? [$data["validation"]["key"] => $data["validation"]["rule"]] : [$data["validation"]["rule"]];
        }

        $question = Question::create($data);

        //start translation
        $question->update(Arr::except($translatable,["meta"]));
        Question::where("id", $question->id)->update(Arr::only($data,$question->translatedAttributes));

        if(Arr::has($translatable,"meta")) {
            foreach(Arr::get($translatable,"meta",[]) as $key => $trans) {
                $question->translations()->create([
                    "question_meta_key" => $key,
                    "translatable" => $trans,
                ]);
            }
        }
        //end translation

        $metas = Arr::get($data,"meta",[]);

        $question->createMetas($metas);

        Artisan::call('cache:clear');

        session()->flash("alert_message",["type" => "success","content" =>"Question has been Created successfully."]);

        return response()->json([
            "success" => "true",
            "redirectTo" => route("admin.questions.edit",$question->id),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Question\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }


    public function duplicate(Question $question)
    {
        $question->load("children");

        $questions =   Question::all();

        $breadcrumbs = [
            ["link" => "admin/", "name" => "Home"],["link" => "admin/questions", "name" => "All Questions"],["name" => "Duplicated: ".$question->title]
        ];

        return view("admin.pages.questions.duplicate",compact("question","breadcrumbs","questions"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Question\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        $question->load("children");

        $questions =   Question::all();

        $breadcrumbs = [
            ["link" => "admin/", "name" => "Home"],["link" => "admin/questions", "name" => "All Questions"],["name" => "Edit:".$question->title]
        ];

        return view("admin.pages.questions.edit",compact("question","breadcrumbs","questions"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Question\Question  $question
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Question $question)
    {
        $this->validate($request,[
            "title" => "required|string",
            "key" => "required|unique:questions,key,{$question->id},id,deleted_at,NULL",
            "price" => "required",
        ]);


        $data = $request->except("translatable");

        $translatable = Arr::get($request->only("translatable"),"translatable",[]);

        $data["main"] = Arr::has($data,"main") ? true : false;

        $data["state"] = Arr::has($data,"state") ? "deactivated" : "active";

        $data["sub_type"] = Arr::has($data,"sub_type") ? implode(Arr::get($data, "sub_type"), "-") : null;

        if(Arr::has($data,"validation")) {
            $data["validation"] = isset($data["validation"]["key"]) && !empty($data["validation"]["key"]) ? [$data["validation"]["key"] => $data["validation"]["rule"]] : [$data["validation"]["rule"]];
        }

      /*  if(Arr::has($data,"rule_messages")) {
            $data["rule_messages"] = strtoarray(str_replace(["\n","\r"],'',Arr::get($data, "rule_messages")));
        }*/

        $question->update($data);

        //start translation
        $question->update(Arr::except($translatable,["meta"]));
        Question::where("id", $question->id)->update(Arr::only($data,$question->translatedAttributes));

        if(Arr::has($translatable,"meta")) {
            foreach(Arr::get($translatable,"meta",[]) as $key => $trans) {
                $question->translations()->updateOrCreate(
                    ["question_meta_key" => $key],
                    ["translatable" => $trans]
                );
            }
        }
        //end translation

        $metas = Arr::get($data,"meta",[]);

        if(!Arr::has($metas,"hide_in_order"))
             $metas["hide_in_order"] = false;

        if(!Arr::has($metas,"hide_wizard_pagination"))
            $metas["hide_wizard_pagination"] = false;

        if(!Arr::has($metas,"multi"))
             $metas["multi"] = false;

        if(!Arr::has($metas,"increment"))
             $metas["increment"] = false;

        if(!Arr::has($metas,"hide_label"))
              $metas["hide_label"] = false;

        if(!Arr::has($metas,"hide_price"))
            $metas["hide_price"] = false;

        $question->syncMetas($metas);

        Artisan::call('cache:clear');

        session()->flash("alert_message",["type" => "success","content" =>"Question has been Updated successfully."]);

        return response()->json([
            "success" => "true",
            "redirectTo" => route("admin.questions.edit",$question->id),
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Question\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {

        $question->children()->delete();
        $question->delete();

        Artisan::call('cache:clear');

        return back();
    }
}
