<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Type;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderStatus()
    {
            $dataTable_columns = [
                ["data" => "id", "name" => "id", "className" => "text-center"],
                ["data" => "name", "name" => "name", "className" => "text-center"],
                ["data" => "key", "name" => "key", "className" => "text-center"],
                ["data" => "desc", "name" => "desc", "orderable" => false, "searchable" => false, "className" => "text-center"],
                ["data" => "action", "name" => "action", "orderable" => false, "searchable" => false, "className" => "text-center"],
            ];
            $breadcrumbs = [
                ["link" => "admin/", "name" => "Home"], ["name" => "Order Status"]
            ];

        return view("admin.pages.types.order-status.index", compact("dataTable_columns","breadcrumbs"));
    }
    public function businessTypes()
    {
            $dataTable_columns = [
                ["data" => "id", "name" => "id", "className" => "text-center"],
                ["data" => "name", "name" => "name", "className" => "text-center"],
                ["data" => "key", "name" => "key", "className" => "text-center"],
                ["data" => "action", "name" => "action", "orderable" => false, "searchable" => false, "className" => "text-center"],
            ];
            $breadcrumbs = [
                ["link" => "admin/", "name" => "Home"], ["name" => "Business Types"]
            ];

        return view("admin.pages.types.business-types.index", compact("dataTable_columns","breadcrumbs"));
    }

    public function dataTable($pageType)
    {


        $actions = [/*"view",*/
            "edit", "delete"];

        return Datatables::of(Type::query()->where("type",$pageType))
            ->addColumn('action', function ($row) use ($actions,$pageType) {
                $warning_name = str_replace("-"," ",$pageType); //for warning message of delete
                $route = ["admin.types", [$row->id]]; //for actions
                $id = $row->id;
                $dataTable_id = "#{$pageType}-types-table"; //to refresh table after deleting / edit and other actions
                $modal = ["edit" => "#type-edit-modal"]; // if modal not exists it will redirect to page using href
                return view('components.table.actions', compact("warning_name", "modal", "dataTable_id", "route", "id", "actions"));

            })->addColumn('desc', function ($row) {
                return Str::limit($row->desc, 30);
            })->rawColumns(['action'])->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $pageType = $request->get("page_type");
        //render modal component
        return view("admin.pages.types.{$pageType}.components.modal.create")->renderSections();
    }

    private function validation($request, $id = "NULL")
    {
        return $request->validate([
            "name" => "required|min:2|max:40",
            "key" => "required|unique:types,key,$id,id|min:2|max:250",
            "class" => "nullable|max:250",
            "desc" => "nullable|max:250",
            "translatable" => "nullable|array"
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validation($request);

        $pageType = $request->get("page_type");

        $data["type"] = $pageType;

        Type::create($data);

        if($pageType == "order-status") {
            session()->flash("alert_message", ["type" => "success", "content" => "New Order Status has been created successfully."]);
        }elseif($pageType == "business-types"){
            session()->flash("alert_message", ["type" => "success", "content" => "New Business Type has been created successfully."]);
        }

        return response()->json([
            "success" => "true",
            "redirectTo" => route("admin.types.$pageType")
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {

        return view("admin.pages.types.{$type->type}.components.modal.edit") ->with("type", $type)->renderSections();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        $data = $this->validation($request, $type->id);

        $type->update($data);

        if($type->type == "order-status") {
            session()->flash("alert_message", ["type" => "success", "content" => "Order Status has been updated successfully."]);
        }elseif($type->type == "business-types"){
            session()->flash("alert_message", ["type" => "success", "content" => "Business Type has been updated successfully."]);
        }

        return response()->json([
            "success" => "true",
            "redirectTo" => route("admin.types.{$type->type}")
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        if($type->key == "pending")
            abort(422,"cannot delete this type");

            $type->delete();

        return response(null,Response::HTTP_NO_CONTENT);
    }
}
