<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Material;
use App\Models\Order\Action\Action;
use App\Models\Order\Details;
use App\Services\File\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function manage(Request $request,Details $details,$offset = 0)
    {
        $allData = $request->all();

        $details->load(["question","actions.materials"]);

        $questionTitle = ($details->question->getMeta("title_in_order") ?: $details->question->title).(($offset > 0) ? " - ".($offset+1) : "");
        $actions = optional($details->actions)->firstWhere("offset",$offset);

        if($request->isMethod("get")){
            return view("admin.pages.orders.components.modal.manage-box")
                ->with("offset", $offset)
                ->with("details", $details)
                ->with("action", $actions)
                ->with("modal_title", "Manage : {$questionTitle}")->renderSections();
        }

        $data = $this->validate($request,[
            "data" => "nullable|array",
            "offset" => "nullable",
        ]);

        $metas = Arr::get($allData,"meta",[]);
        $materials = Arr::get($allData,"materials",[]);
        $voiceNotes = Arr::get($allData,"voice_notes",[]);

        $actionModel = null;

        if(!Arr::has($data,"offset")){
            $data["offset"] = $offset;
        }

        if($actions){
            $action = $details->actions()->where("offset",$offset);
            $action->update($data);

            if(!Arr::has($metas,"watermark"))
                $metas["watermark"] = false;

              $actionModel = $action->first();

               optional($actionModel->materials())->forceDelete();

              $actionModel->syncMetas($metas);
        }else{
            $actionModel = $details->actions()->create($data);
            $actionModel->createMetas($metas);
        }

        if($actionModel){
            $saveMaterials = [];
            $sortingMaterial = 0;
            foreach($materials as $material){
                $created_at = explode("_",$material);
                $file = count($created_at) > 1 ? $created_at[0] : $material;
                $extension = explode(".",$file);
                $saveMaterials[] = new Material(["storage_path" => $file , "extension" => end($extension) , "created_at" => $created_at[1] ?? now()->addSeconds($sortingMaterial)]);
                if(end($extension) != "pdf") {
                    $materialWithWatermark = ImageService::addWatermark(storage_path("app/" . $file),);
                    $saveMaterials[] = new Material(["type" => "watermark", "storage_path" => $materialWithWatermark, "extension" => end($extension), "created_at" => $created_at[1] ?? now()->addSeconds($sortingMaterial)]);
                }
                $sortingMaterial++;
            }
            foreach($voiceNotes as $voiceNote){
                $created_at = explode("_",$voiceNote);
                $file = count($created_at) > 1 ? $created_at[0] : $voiceNote;
                $extension = explode(".",$file);
                $saveMaterials[] = new Material(["type" => "voice-note","storage_path" => $file , "extension" => end($extension), "created_at" => $created_at[1] ?? now()->addSeconds($sortingMaterial)]);
                $sortingMaterial++;
            }
            if(!empty($saveMaterials)) {
                $actionModel->materials()->saveMany($saveMaterials);
            }
        }

        session()->flash("alert_message", ["type" => "success", "content" => "{$questionTitle} has been updated successfully."]);

        return response()->json([
            "success" => "true",
            "redirectTo" => back()->getTargetUrl()
        ]);

    }

    public function updateStatus(Request $request,Action $action)
    {

        $data = $this->validate($request,[
            "type_id" => "required|exists:types,id",
        ]);


        $action->update(["state_id" => $data["type_id"]]);

       session()->flash("alert_message", ["type" => "success", "content" => "Order Status has been updated successfully."]);

        return response()->json([
            "success" => "true",
            "redirectTo" => back()->getTargetUrl()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order\Action\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function show(Action $action)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order\Action\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function edit(Action $action)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order\Action\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Action $action)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order\Action\Action  $action
     * @return \Illuminate\Http\Response
     */
    public function destroy(Action $action)
    {
        //
    }
}
