<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataTable_columns = [
            ["data" => "code", "name" => "code", "className" => "text-center"],
            ["data" => "title", "name" => "title", "className" => "text-left"],
            ["data" => "state", "name" => "state", "className" => "text-center"],
            ["data" => "amount", "name" => "amount", "className" => "text-center"],
            ["data" => "start_date", "name" => "start_date",  "orderable" => false, "searchable" => false,"className" => "text-center"],
            ["data" => "action", "name" => "action", "orderable" => false, "searchable" => false, "className" => "text-center"],
        ];
        $breadcrumbs = [
            ["link" => "admin/", "name" => "Home"], ["name" => "Coupons"]
        ];

        return view("admin.pages.coupons.index", compact("dataTable_columns","breadcrumbs"));
    }

    public function dataTable()
    {
        $actions = [/*"view",*/
            "edit", "delete"];

        return Datatables::of(Coupon::query()->latest())
            ->addColumn('action', function ($row) use ($actions) {
                $warning_name = "coupon"; //for warning message of delete
                $route = ["admin.coupons", [$row->id]]; //for actions
                $id = $row->id;
                $dataTable_id = "#coupons-table"; //to refresh table after deleting / edit and other actions
                $modal = ["edit" => "#coupon-edit-modal"]; // if modal not exists it will redirect to page using href
                return view('components.table.actions', compact("warning_name", "modal", "dataTable_id", "route", "id", "actions"));

            })->addColumn('amount', function ($row) {
                if($row->type == "percentage"){
                    return round($row->amount,0)."% of Total";
                }else if($row->type == "fixed"){
                    return $row->amount." EGP (Fixed)";
                }
            })->addColumn('state', function ($row) {
                return $row->state == "active" ? '<div class="badge badge-pill badge-light-success m-0 p-50 font-small-1">Active</div>' : '<div class="badge badge-pill badge-light-danger m-0 p-50 font-small-1">Deactivated</div>';
            })->addColumn('start_date', function ($row) {
                return  '<div class="badge badge-light m-0 p-50 font-small-2 " style="text-transform: none !important; ">'.$row->start_date->format("d M, Y").'</div> TO '.'<div class="badge  badge-secondary m-0 p-50 font-small-2" style="text-transform: none !important; ">'.$row->end_date->format("d M, Y").'</div>';
            })->rawColumns(['state','amount','start_date','action'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //render modal component
        return view("admin.pages.coupons.components.modal.create")->renderSections();
    }
    private function validation($request, $id = "NULL")
    {
        return $request->validate([
            "title" => "required|min:5|max:40",
            "code" => "required|unique:coupons,code,$id,id|min:2|max:10",
            "type" => "required|in:percentage,fixed",
            "state" => "sometimes",
            "amount" => "required|numeric",
            "minimum_amount" => "sometimes",
            "max_usage" => "nullable|integer",
            "user_id" => "nullable|array",
            "start_date" => "required|date|date_format:Y-m-d",
            'end_date' => 'required|date|date_format:Y-m-d|after:start_date'
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validation($request);

        $data["state"] = Arr::has($data,"state") ? "active" : "deactivated";
        $data["end_date"] .= " 23:59:59";

        if(!Arr::get($data,"minimum_amount"))
            Arr::forget( $data, "minimum_amount");

        if(!Arr::get($data,"max_usage"))
            Arr::forget( $data, "max_usage");

        Coupon::create($data);

        session()->flash("alert_message", ["type" => "success", "content" => "Coupon Status has been created successfully."]);

        return response()->json([
            "success" => "true",
            "redirectTo" => route("admin.coupons.index")
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        return view("admin.pages.coupons.components.modal.edit") ->with("coupon", $coupon)->renderSections();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        $data = $this->validation($request, $coupon->id);

        $data["state"] = Arr::has($data,"state") ? "active" : "deactivated";
        $data["end_date"] .= " 23:59:59";

        if(!Arr::get($data,"minimum_amount"))
            $data["minimum_amount"] = 0.00;

        if(!Arr::get($data,"max_usage"))
            $data["max_usage"] = 0;

        if(empty($data["user_id"]))
            $data["user_id"] = null;

        $coupon->update($data);

        session()->flash("alert_message", ["type" => "success", "content" => "Coupon Status has been updated successfully."]);

        return response()->json([
            "success" => "true",
            "redirectTo" => route("admin.coupons.index")
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();

        return response(null,Response::HTTP_NO_CONTENT);
    }
}
