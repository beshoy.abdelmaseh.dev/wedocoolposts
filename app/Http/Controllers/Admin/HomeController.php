<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invoice\Transaction\Transaction;
use App\Models\Order\Order;
use App\Models\Type;
use App\Models\User\Role;
use App\Models\User\User;
use App\Notifications\NewOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            ["link" => "admin/", "name" => "Home"]
        ];
        $transactions = Transaction::query()->with(["invoice.order"])->latest()->limit(50)->get();

        $order_ids = optional(current_user()->unreadNotifications)->pluck("data.order_id") ?: [];
        $orders = Order::query()->with(["user","invoice","action"])->whereHas("user")->whereIn("id",$order_ids)->get();

        $statistics = [
            "total_users" => User::count(),
            "total_orders" => Order::query()->whereHas("user")->count(),

            "unpaid_orders" => Order::query()->whereHas("user")->whereHas("action" , function($query){
                return $query->whereNull("state_id");
            })->count(),

            "pending_orders" => Order::query()->whereHas("action" , function($query){
                return $query->where("state_id",optional(Type::where("key", "pending")->first())->id);
            })->count(),

            "full_paid_orders" => Order::query()->whereHas("invoice" , function($query){
                return $query->where("state",1);
            })->count(),

            "approved_orders" => Order::query()->whereHas("action" , function($query){
                return $query->where("approval",1);
            })->count(),
        ];



        return view("admin.pages.home.index",compact("breadcrumbs","transactions","orders","statistics"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
