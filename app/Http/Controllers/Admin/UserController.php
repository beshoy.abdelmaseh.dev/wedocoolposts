<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users =   User::withCount('orders')->get();

        $breadcrumbs = [
            ["link" => "admin/", "name" => "Home"],["name" => "Users"]
        ];

        return view("admin.pages.users.index",compact("users","breadcrumbs"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $breadcrumbs = [
            ["link" => "admin/", "name" => "Home"],["link" => "admin/users", "name" => "All Users"],["name" => "Edit User"]
        ];
        return view("admin.pages.users.edit",compact("user","breadcrumbs"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $this->validate($request,[
            "full_name" => "required|min:2|max:40",
            "email" => 'required|min:5|max:40|email|unique:users,email,'.$user->id.',id,deleted_at,NULL',
            "phone" => 'required|max:20|unique:users,phone,'.$user->id.',id,deleted_at,NULL',
            "type_id" => "sometimes",
            "password" => "sometimes",
            "links.*" => "sometimes"

        ]);

        if (isset($data["password"]) && !empty($data['password']))
            $data['password'] = bcrypt($data['password']);
        else
            Arr::forget($data,"password");


        $user->update(Arr::only($data,["full_name","email","phone","password"]));

        if(Arr::has($data,"links")) {
            $user->syncMetas(Arr::get($data, "links"));
        }

        if(Arr::has($data,"type_id")) {
            $user->syncMetas(Arr::only($data, ["type_id"]));
        }

        session()->flash("alert_message", [
            "type" => "success",
            "content" => "User has been updated successfully.",
        ]);

        return response()->json([
            "success" => "true",
            "redirectTo" => redirect()->back()->getTargetUrl()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return back();
    }
}
