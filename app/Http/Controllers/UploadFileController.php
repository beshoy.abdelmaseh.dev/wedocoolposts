<?php

namespace App\Http\Controllers;

use App\Services\File\AudioService;
use App\Services\File\LocalUploadService;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Config;
use Laravelista\Comments\Comment;
use Response;
class UploadFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function voiceNote(Request $request)
    {

        $element_id = $request->input("element_id",null);
        $input_name = $request->input("input_name",null);
        $type = $request->input("type",null);

        if ($request->has("voice_blob_recorded")) {
            $voiceNote = $request->file("voice_blob_recorded");

            if (!$voiceNote || empty($voiceNote) || !$voiceNote instanceof UploadedFile)
                abort(422, "invalid voice note, please contact us to fix it.");

            $voiceNoteHash = $voiceNote->hashName();

            $voiceNote->storeAs("voice-notes/", $voiceNoteHash);

            $storage = "voice-notes/".$voiceNoteHash;

            $voiceUrl = route("uploads.show",$storage);

            if($type == "comment-box"){
                $current_url = $request->input("current_url",null);
                $comment_reply_id = $request->input("comment_reply",null);
                $commentable_type = $request->input("commentable_type",null);
                $commentable_id = $request->input("commentable_id",null);
                if($comment_reply_id){
                    $commentClass = Config::get('comments.model');
                    $reply = new $commentClass;
                    $comment = Comment::findOrFail($comment_reply_id);
                    $reply->commenter()->associate(auth()->user());
                    $reply->commentable()->associate($comment->commentable);
                    $reply->parent()->associate($comment);
                    $reply->comment = $storage;
                    $reply->approved = !Config::get('comments.approval_required');
                    $reply->save();

                }elseif($commentable_id && $commentable_type){
                    $model = $commentable_type::findOrFail($commentable_id);

                    $commentClass = Config::get('comments.model');
                    $comment = new $commentClass;

                    $comment->commentable()->associate($model);
                    $comment->commenter()->associate(auth()->user());
                    $comment->comment = $storage;
                    $comment->approved = !Config::get('comments.approval_required');
                    $comment->save();
                }
                return response()->json([
                    "success" => "true",
                    "redirectTo" => $current_url,
                ]);
            }

            if($element_id && $input_name) {
                $audioCol = "mb-25 col-sm-2 col-md-3";
                if ($type == "texteditor-addon-manage-order") {
                    $audioCol = "mb-25 col-sm-5 col-md-6";
                }
                if ($type == "texteditor-addon" || $type == "texteditor-addon-manage-order") {
                    $eval = "$('#$element_id').find('.note-voice-record').find(\".save-voice-btn,.cancel-voice-btn,.record-state\").addClass(\"d-none\"); $('#$element_id').find('.note-voice-record').find(\".start-voice-btn\").removeClass(\"d-none\");$('#$element_id').find('.recorded-voice-notes').removeClass('d-none').find(\".row\").append('<div class=\"$audioCol\"><input type=\"hidden\" value=\"$storage\" name=\"$input_name\"> <audio src=\"$voiceUrl\" controls=\"\" style=\"width:100%\"></audio></div>');";
                }

                return response()->json([
                    "success" => "true",
                    "eval" => $eval,
                ]);
            }

        }

        return response()->json(["errors" => ["upload_voice_note" => trans("locale.upload_voice_note")]],422);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $localUploadService = app(LocalUploadService::class)->validate([
            'file' => 'sometimes|file|mimes:jpeg,png,jpg,gif,pdf,bmp|max:8096'
        ])->setByNamespace("web");

        if ($localUploadService) {
            $uploadedFile = $localUploadService->setFile("file")->save("materials");
            if ($uploadedFile->exists()) {
                $data = $uploadedFile->store();
                $storage = route("uploads.show",$data["storage_path"]);
                if($data["extension"] == "pdf"){
                    $url = asset("assets/images/icon/pdf-2.png");
                }else{
                    $url = $storage;
                }
                return response()->json([
                    "success" => "true",
                    "data" => $uploadedFile->store(),
                    "extension" => $data["extension"],
                    "url" => $url,
                    "input" => $data["storage_path"],
                    "storage" => $storage
                ]);
            }
        }

        return response()->json(["errors" => ["upload_file" => trans("locale.upload_file")]],422);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($storage_code)
    {
        $storage_path = $storage_code;

        if (!Storage::exists($storage_code))
            return abort(404);

        $allowMimes = ['image/jpg', 'image/png', 'image/gif', 'image/jpeg',"audio/x-wav" , 'audio/wave', "audio/ogg" , "audio/mpeg" , "audio/mp4" ,"video/webm"];
        $valid_exts = ['jpeg', 'jpg', 'png', 'gif',"wav" ,"mp3" ,"mp4" , "ogg","webm"]; // valid extensions
        $fileName = $storage_path;
        $tmp = explode('.', $fileName);
        $extension = end($tmp);

        if($extension == "pdf"){

            return response()->file(storage_path("app/".$storage_code));
        }
        $MimeType = Storage::mimeType($storage_path);

        if (empty($MimeType) || !in_array($MimeType, $allowMimes) || !in_array(strtolower($extension), $valid_exts))
            return abort(404);

        if(Str::contains($MimeType,"audio")) {
            $audioService = new AudioService();
            return $audioService->start($storage_path, $MimeType);
        }

        $response = Response::make(Storage::get($storage_path), 200);
        $response->header("Cache-Control", 'private, max-age=604800, pre-check=604800');
        $response->header("Pragma", 'private');
        $response->header("Expires", date(DATE_RFC822, strtotime("7 day")));
        $response->header("Content-Type", $MimeType);
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
