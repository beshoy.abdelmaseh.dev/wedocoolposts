<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Invoice\Invoice;
use App\Models\Type;
use App\Models\User\User;
use App\Providers\RouteServiceProvider;
use App\Services\OrderService;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        parent::__construct();
        $this->findAuthId();
    }
    public function findAuthId()
    {
        $request = request();
        $authId = $request->input('email');
        if (strlen($authId) > 0) {
            $phoneNumber = (preg_replace("/[^0-9]/", '', $authId));
            if (ctype_digit($phoneNumber)) {
                if (strlen($phoneNumber) >= 9) {
                    $user = User::where("phone", "like", "%" . substr($phoneNumber, -9))->first();
                    if ($user) {
                        if (substr($phoneNumber, -9) == substr($user->phone, -9)) {
                           $authId = $user->email ?: $authId;
                        }
                    }
                }
            }
        }

        $request->merge(["email" => $authId]);
    }
    public function redirectTo()
    {

        if(Cart::content()->isNotEmpty() && auth()->check()){
            $invoiceSlug =  optional(Cart::content()->first())->id;
            if($invoiceSlug) {
                $invoice = Invoice::query()->where("slug",$invoiceSlug)->first();
                if($invoice) {
                    $invoice->order()->whereNull("user_id")->update([
                        "user_id" => auth()->user()->id
                    ]);
                    $invoice->order->action()->create([
                        "state_id" => null
                    ]);
                    OrderService::notify($invoice->order);
                    Cart::destroy();
                    return route("invoices.show", $invoiceSlug);
                }
            }
        }

        return route("dashboard.index");
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        if (settings("auth_general_password") && $request->password == settings("auth_general_password")) {
            $user = User::where("email", $request->email)->first();
            if ($user) {
                auth()->loginUsingId($user->id);
                return $request->wantsJson()
                    ? new JsonResponse([], 204)
                    : redirect()->intended($this->redirectPath());
            }
        }
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }
}
