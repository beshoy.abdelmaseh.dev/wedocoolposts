<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class Accessibility
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $accessibilityTo = null)
    {
        if (auth()->check()) {

            if ($accessibilityTo == "admin" && !auth()->user()->hasRole(['developer', 'admin'])) {
                throw new AccessDeniedHttpException();
            }else if ($accessibilityTo == "web" && auth()->user()->hasRole(['developer', 'admin'])) {
                if(!Str::startsWith( $request->route()->getName(),"uploads.")) {
                    return redirect()->route("admin.home.index");
                }
            }
        }
        return $next($request);
    }
}
