<?php
namespace App\Traits\Model;

trait SetTimeStamps
{

	public function freshTimestamp()
	{
		return get_timestamps();
	}
}