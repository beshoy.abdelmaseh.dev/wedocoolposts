<?php
namespace App\Traits\Model;

use App\Models\User\User;


trait HasCreatedBy
{
	public static function bootHasCreatedBy() {

			self::creating(function ($model) {
				$created_by =  Optional(auth()->user())->id;
				$model->attributes['created_by'] = $model->attributes['created_by'] ?? $created_by;
			});
	}

	public function createdBy()
	{
		return $this->belongsTo(User::class);
	}

}