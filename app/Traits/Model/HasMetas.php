<?php

namespace App\Traits\Model;


use Illuminate\Support\Str;

trait HasMetas
{
	public function metas()
	{
		return $this->hasMany($this->meta, Str::singular($this->getTable()) . "_id");
	}

	public function createMetas(array $data)
	{
		return (new $this->meta)->sync($this, $data);
	}

	public function syncMetas(array $data)
	{
		return (new $this->meta)->sync($this, $data);
	}

	public function updateMetas(array $data)
	{
		return (new $this->meta)->sync($this, $data);
	}

	public function getMeta($key,$default = null, bool $jsonDecode = false, bool $data = false)
	{
        if($this->translations){
            if($value = $this->translations->firstWhere("question_meta_key",$key)){
                $locale = \LaravelLocalization::getCurrentLocale();
                if($translated = optional($value->translatable)->{$locale}){
                    return $translated;
                }
            }
        }

		$result = optional($this->metas->firstWhere('key', $key));

		$getData = ($data ? $result->data : $result->value) ?: $default;

		return $jsonDecode ? ($getData ? json_decode($getData, true) : []) : $getData;
	}


    public function getMetaTranslate($locale,$key,$default = null, bool $jsonDecode = false, bool $data = false)
    {
        if(is_null($this->translations)){
            return null;
        }
        if($value = $this->translations->firstWhere("question_meta_key",$key)){
            if($translated = optional($value->translatable)->{$locale}){
                return $translated;
            }
        }
        return null;
	}
    public function hasMeta($key)
    {

        $result = in_array($key,$this->metas->pluck("key")->toArray());

        return $result ? true : null;
	}

	public function getMetaAsArray($key,$delimiter = ",")
	{
		$result = $this->getMeta($key);

		if (!empty($result)) {
			return explode($delimiter,$result);
		}

		return [];
	}

	public function allMetas()
	{
		return $this->metas->toArray();
	}

    public function deleteMeta($key)
    {
        return $this->metas()->where("key",$key)->delete();
    }

    public function deleteMetas($key)
    {
        return $this->metas()->whereIn("key",$key)->delete();
    }
}
