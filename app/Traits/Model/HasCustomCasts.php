<?php
namespace App\Traits\Model;

use Illuminate\Support\Str;

trait HasCustomCasts
{

	protected function castAttribute($key, $value)
	{
		if (is_null($value)) {
			return $value;
		}

		$getCast = trim($this->getCasts()[$key]);

		if(Str::contains($getCast,"::") && is_callable($getCast)) {
			return call_user_func($getCast, $value);
		}


		return parent::castAttribute($key,$value);
	}
}