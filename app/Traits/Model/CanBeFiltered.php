<?php
namespace App\Traits\Model;



use App\Filters\BaseFilter;
use Illuminate\Database\Eloquent\Builder;

trait CanBeFiltered
{

	public function scopeFilter(Builder $builder,$filters)
	{
		return (new BaseFilter(request()))->apply($builder,$filters);
	}

}